// Customise those settings

var divid = "betslip";
var url = "web/betslip";

var divj = "betslipJ";
var urlj = "betslip/jackpot";

var divb = "betslipB";
var urlb = "betslip/bingwa";

var thediv = "matches";
var dataUrl = "matches";

////////////////////////////////
//
// Refreshing the DIV
//
////////////////////////////////

var locations = window.location.href.split("/").pop();

function refreshdiv(loc) {

// The XMLHttpRequest object

    var xmlHttp;
    try {
        xmlHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    } catch (e) {
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        } catch (e) {
            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                alert("Your browser does not support AJAX.");
                return false;
            }
        }
    }

    // Timestamp for preventing IE caching the GET request

    fetch_unix_timestamp = function ()
    {
        return parseInt(new Date().getTime().toString().substring(0, 10))
    }

    var nocacheurl = url+ "?location="+loc;

    // The code...

    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4) {
            document.getElementById(divid).innerHTML = xmlHttp.responseText;
        }
    }
    xmlHttp.open("GET", nocacheurl, true);
    xmlHttp.send(null);
}

function refreshData() {

// The XMLHttpRequest object

    var xHttp;
    try {
        xHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    } catch (e) {
        try {
            xHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        } catch (e) {
            try {
                xHttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                alert("Your browser does not support AJAX.");
                return false;
            }
        }
    }

    var nocacheDataurl = dataUrl;

    // The code...

    xHttp.onreadystatechange = function () {
        if (xHttp.readyState == 4) {
            document.getElementById(thediv).innerHTML = xHttp.responseText;
        }
    }
    xHttp.open("GET", nocacheDataurl, true);
    xHttp.send(null);
}

function refreshSlipJ() {

// The XMLHttpRequest object

    var xHttp;
    try {
        xHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    } catch (e) {
        try {
            xHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        } catch (e) {
            try {
                xHttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                alert("Your browser does not support AJAX.");
                return false;
            }
        }
    }

    var nocacheDataurl = urlj;

    // The code...

    xHttp.onreadystatechange = function () {
        if (xHttp.readyState == 4) {
            document.getElementById(divj).innerHTML = xHttp.responseText;
        }
    }
    xHttp.open("GET", nocacheDataurl, true);
    xHttp.send(null);
}

function refreshSlipB() {

// The XMLHttpRequest object

    var xHttp;
    try {
        xHttp = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
    } catch (e) {
        try {
            xHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
        } catch (e) {
            try {
                xHttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                alert("Your browser does not support AJAX.");
                return false;
            }
        }
    }

    var nocacheDataurl = urlb;

    // The code...

    xHttp.onreadystatechange = function () {
        if (xHttp.readyState == 4) {
            document.getElementById(divb).innerHTML = xHttp.responseText;
        }
    }
    xHttp.open("GET", nocacheDataurl, true);
    xHttp.send(null);
}



// Start the refreshing process

var seconds;
window.onload = function startrefresh() {
    refreshdiv(locations);
    /**if (locations == 'jackpot') {
        refreshSlipJ();
    }
    if (locations == 'bingwa4') {
        refreshSlipB();
    }**/
}
