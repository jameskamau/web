var seconds = 5,
    loader = $(".loader"),
    errorHolder = function () {
        return $(".betslip-error");
    },
    successHolder = function () {
        return $(".betslip-success");
    };

$(window).scroll(function () {
    if ($(document).scrollTop() > 50) {
        $('#slip-holder').addClass('affix');
        $('#shrink-header').addClass('shrink-header');
    } else {
        $('#slip-holder').removeClass('affix');
        $('#shrink-header').removeClass('shrink-header');
    }
});

jQuery(document).ready(function ($) {
    $(".clickable-row").click(function () {
        window.document.location = $(this).data("href");
    });
    /** Handle tabs on home page **/

    $(document).on( 'shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
        e.preventDefault();
      
        var url = $(this).attr("data-url");
        var href = this.hash;
        var pane = $(this);
        
        // ajax load from data-url
        $(href).load(url,function(result){      
            pane.tab('show');
        });
    });

    /** End handle tabs**/

});


$("#slip-button").click(function () {
    $("#slip-holder").slideDown("fast", function () {
    });
});

$("#slip-button-close").click(function () {
    $("#slip-holder").slideUp("fast", function () {
    });
});


function refreshSlip() {
    $.post("web/betslip", {}, function (data) {
        console.log("posting refresh slip");
        $("#betslip").animate({opacity: '0.8'});
        $("#betslip").html(data);
        $("#betslip").animate({opacity: '1'});
    }).done(function () {
        $('.loader').css("display", "none");
    });
}

function refreshJackSlip() {
    $.post("web/betwjslip", {}, function (data) {
        $("#betslip").animate({opacity: '0.8'});
        $("#betslip").html(data);
        $("#betslip").animate({opacity: '1'});
    }).done(function () {
        $(".loader").css("display", "none");
    });
}

function refreshBingwaFour() {
    $.post("betslip/bingwa", {}, function (data) {
        $("#betslipB").animate({opacity: '0.8'});
        $("#betslipB").html(data);
        $("#betslipB").animate({opacity: '1'});
    }).done(function () {
        $(".loader").css("display", "none");
    });
}

function addBet(value, sub_type_id, odd_key, custom, special_bet_value, bet_type, home, away, odd, oddtype, parentmatchid, pos) {
    
    if ($('[custom="' + custom + '"]').hasClass('picked')) {
        return removeMatch(value);
    }
    $(".loader").slideDown("slow");
    errorHolder().hide();
    successHolder().hide();

    console.log(odd_key)
    console.log(oddtype)
    console.log(parentmatchid)
    $.post("betslip/add", {match_id: value, sub_type_id: sub_type_id, odd_key: odd_key, custom: custom, special_bet_value: special_bet_value, bet_type: bet_type, home: home, away: away, odd: odd, oddtype: oddtype, parentmatchid: parentmatchid, pos: pos}, function (data) {
        var game_id = value;
        $("." + game_id).removeClass('picked');
        $("." + custom).addClass('picked');
        /** alert("Ta ==> " + bet_type); **/
        bet_type = ""+bet_type;
        if (bet_type == 'jackpot') {
            refreshJackSlip();
            $(".slip-counter").html(data.total);
        } else if (bet_type == 'bingwafour') {
            refreshBingwaFour();
        } else {
            $(".slip-counter").html(data.total);
            refreshSlip();
        }
    });
}

function removeMatch(value) {
    $(".loader").slideDown("slow");
    $.post("betslip/remove", {match_id: value}, function (data) {
        if(data.bet_type == 'jackpot'){
            refreshJackSlip();
        }else{
            refreshSlip();
        }
        $("." + value).removeClass('picked');
        $(".slip-counter").html(data.total);
    });
}

function clearSlip(value) {
    $(".loader").slideDown("slow");
    $.post("betslip/clearslip", {}, function (data) {
        refreshSlip();
        $(".picked").removeClass('picked');
    });
}


function winnings() {

    var value = $("#bet_amount").val();
    var odds = $("#total_odd").val();
    var total = (value * odds)/1.2;
    var stakeaftertax = value/1.2;

    if(total > 50000){
        total = 50000;
    }
    var winnings = total,
    tax = 20 * (winnings-stakeaftertax) / 100,
    net = (winnings - tax);

    $("#stake-after-tax").html(stakeaftertax.toFixed(2));
    $("#pos_win").html(winnings.toFixed(2));
    $("#tax").html(tax.toFixed(2));
    $("#net-amount").html(net.toFixed(2));
}


function winningsM() {
    var value = $("#bet_amount_m").val();
    var odds = $("#total_odd_m").val();
    var totalWin = value * odds;
    var totalWin = Math.round(totalWin);
    $("#pos_win_m").html(totalWin);
}

function showSlipError(message) {
    $('.loader').css("display", "none");
    return errorHolder().slideDown("slow").html(message);
}


function showSlipSuccess(message) {
    successHolder().slideDown("slow").html(message);   
    return  clearSlip();
}
function placeBet() {
    var user_id = $("#user_id").val();
    var bet_amount = $("#bet_amount").val();
    var total_odd = $("#total_odd").val();
    if (!user_id) {
        return showSlipError("Account not found, please login to proceed");
    } else if (bet_amount < 1) {
        return showSlipError("The minimum bet amount is KSH 1");
    } else {
        loader.slideDown("slow");
        $.post("betslip/placebet", {user_id: user_id, bet_amount: bet_amount, total_odd: total_odd}, function (data) {
            if (data.status_code == '421') {
               successHolder().html("");
               return showSlipError(data.message);                
            }

            if (data.status_code == '500') {
                successHolder().html("");
                return showSlipError(data.message);
            }

            if (data.status_code == '201') {
                errorHolder().html("");
                return showSlipSuccess(data.message);
            }
        }, 'json');
    }
    return false;
}

function jackpotBet() {
    $(".loader").slideDown("slow");
    $(".loader").css("display", "flex");
    var user_id = $("#user_id").val();
    var bet_amount = $("#bet_amount").val();
    var jackpot_type = $("#jackpot_type").val();;
    var jackpot_id = $("#jackpot_id").val();
    var total_matches = $("#total_matches").val();

    console.log(user_id);
    console.log(bet_amount);
    console.log(jackpot_type);
    console.log(jackpot_id);

    if (!user_id) {
        $(".loader").css("display", "none");
        $("#quick-login").slideDown("slow");
    } else {
        $.post("betslip/betJackpot", {user_id: user_id, jackpot_type: jackpot_type, 
            jackpot_id: jackpot_id, bet_amount: bet_amount,total_odd:1,
            total_matches:total_matches}, function (data) {

            if (data.status_code == '421') {
                $(".loader").css("display", "none");
                $(".betslip-error").html(data.message);
                $(".betslip-error").slideDown("slow");
            }

            if (data.status_code == '500') {
                $(".loader").css("display", "none");
                $(".betslip-error").html(data.message);
                $(".betslip-error").slideDown("slow");
            }

            if (data.status_code == '201') {
                $(".betslip-success").slideDown("slow");
                $(".betslip-success").html(data.message);
                refreshJackSlip();
                refreshBingwaFour()
                $(".slip-counter").html("0");
                $(".picked").removeClass('picked');
            }
        }, 'json');
    }
}
