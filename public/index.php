<?php
$host = gethostname();
//if ($host == "qb-pr-web-1") {
if ($host == "phalcon.shikabet.com") {
ini_set('session.save_handler', 'redis');
ini_set('session.save_path', 'tcp://ke-pr-memcache:6379?auth=phpredis');
}

#var_dump(ini_get("session.save_handler"));
#var_dump(ini_get("session.save_path"));
#error_reporting(0);

define('APP_PATH', realpath('..'));

try {
    
    /**
     * Read the configuration
     */
    $config = include APP_PATH . "/app/config/config.php";

    /**
     * Read auto-loader
     */
    include APP_PATH . "/app/config/loader.php";

    /**
     * Read services
     */
    include APP_PATH . "/app/config/services.php";

    /**
     * Handle the request
     */

    include APP_PATH . "/vendor/autoload.php";
    
    $application = new \Phalcon\Mvc\Application($di);

    echo $application->handle()->getContent();

} catch (\Exception $e) {
    
    
    
    
     echo $e->getMessage() . '<br>';
     echo '<pre>' . $e->getTraceAsString() . '</pre>';
    
    if ($host != "pr-web-1" ) {
        echo "<head><title>Page Not Found - shindabet</title></head><body style='background-color: #510c65;)'>
            <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'><div style='
    max-width: 600px;
    display: block;
    margin: 100px auto;
    text-align: center;

'>
<a href=''><img style='max-width: 180px;' src='' /></a>
<p><b style='font-size: 50px;
    line-height: 60px;
    display: block;'>Page not found! </b> <br/>Unfortunately, this page cannot be found. go to homepage.
<img src='' style='max-width: 200px;
    clear: both;
    display: block;
    margin: 20px auto;
    text-align: center;' /></p>
    <a href='http://shikabet.co.ke' style=''>Go Home </a>
</div>

</body>";
    }else{
        echo $e->getMessage() . '<br>';
        echo '<pre>' . $e->getTraceAsString() . '</pre>';}
}
