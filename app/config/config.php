<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

$host = gethostname();
$baseUri = '/';

$host = '127.0.0.1/';
$baseUri = '/';

$connection = array(
    'adapter' => 'mysql',
    'host' => '35.241.140.160',
    'username' => 'web',
    'password' => 'sc0r3p3s@w3bN1nj@',
    'dbname' => 'scorepesa',
    'charset' => 'utf8'
);


return new \Phalcon\Config(array(
    'database' => $connection,
    'application' => array(
        'controllersDir' => APP_PATH . '/app/controllers/',
        'servicesDir' => APP_PATH . '/app/services/',
        'modelsDir' => APP_PATH . '/app/models/',
        'utilsDir' => APP_PATH . '/app/utils/',
        'migrationsDir' => APP_PATH . '/app/migrations/',
        'viewsDir' => APP_PATH . '/app/views/',
        'pluginsDir' => APP_PATH . '/app/plugins/',
        'libraryDir' => APP_PATH . '/app/library/',
        'vendorDir' => APP_PATH . '/vendor/',
        'cacheDir' => APP_PATH . '/app/cache/',
        'baseUri' => $baseUri,
    )
        )
);
