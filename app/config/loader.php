<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
// Register some namespaces

$loader->registerDirs(
    array(
        $config->application->controllersDir,
        $config->application->servicesDir,
        $config->application->modelsDir,
        $config->application->utilsDir
    )
);

$loader->register();
