<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\View;

class JackpotController extends ControllerBase {

    public function indexAction() {
        $jackpot = $this->rawQueries("SELECT jackpot_type,jackpot_event_id,total_games FROM jackpot_event WHERE status = 'ACTIVE' ORDER BY 1 DESC LIMIT 1");

        $jackpotID = $jackpot['0']['jackpot_event_id'];
        $jackpotType = $jackpot['0']['jackpot_type'];
        $totalMatches = $jackpot['0']['total_games'];

        $games = $this->rawQueries("select j.game_order as pos, jackpot_match_id,e.sub_type_id, group_concat(concat(odd_value)) as threeway, m.game_id, m.match_id, m.start_time, m.parent_match_id, m.away_team, m.home_team, c.competition_name, c.category from jackpot_match j inner join `match` m on m.parent_match_id = j.parent_match_id INNER JOIN competition c ON m.competition_id = c.competition_id inner join event_odd e on e.parent_match_id = m.parent_match_id where j.jackpot_event_id=? and j.status='ACTIVE'  and e.sub_type_id=1 group by j.parent_match_id order by game_order", [
            $jackpotID,
        ]);

        $theBetslip[] = '';

        $betslip = $this->session->get("betslip");

        unset($theBetslip);

        foreach ($betslip as $slip) {
            if ($slip['bet_type'] == 'jackpot') {
                $theBetslip[$slip['match_id']] = $slip;
            }
        }

        $slipCountJ = sizeof($theBetslip);

        $this->tag->setTitle('Jackpot');

        $this->view->setVars(["games" => $games, 'slipCount' => $slipCountJ, 'theBetslip' => $theBetslip, 'jackpotSlip'=> 1, 'jackpotID' => $jackpotID, 'totalMatches' => $totalMatches,'jackpotType' => $jackpotType]);
    }

}
