<?php

use Phalcon\Mvc\View;

class WebController extends ControllerBase {

    public function indexAction() {
        
    }

    public function footballAction() {
        $this->view->setVars(["allLeagues" => $this->allLeagues()]);
    }

    public function rugbyAction() {
        $this->view->setVars(['rugby' => $this->rugby()]);
        $this->view->pick("mobile/football");
    }

    public function tennisAction() {
        $this->view->pick("mobile/tennis");
    }

    public function sportsAction() {
        $this->view->pick("mobile/sport");
    }

    public function betslipAction() {
        $reference_id = $this->reference();

        $jackpot = $this->request->get('jackpot');

        $jp = $_GET['location'];

        $betslip = ($this->session->get('betslip') != null)?$this->session->get('betslip'):[];

        $referenceID = $this->reference();
        $theBetslip = [];
        $jackpotSlip = 0;

        foreach ($betslip as $slip) {
            if($jp == 'jackpot'){
                $jackpotSlip = 1;
                if ($slip['bet_type'] == 'jackpot') {
                    $theBetslip[$slip['match_id']] = $slip;
                }
            }else{
                if ($slip['bet_type'] == 'prematch') {
                    $theBetslip[$slip['match_id']] = $slip;
                }
            }
        }

        $betslip = $theBetslip;
        $betslipSession = $this->session->get("betslip");
        //coreUtils::flog('INFO', "betslipSession returns ==>" . print_r($betslipSession, true), __CLASS__, __FUNCTION__, __LINE__);

        $this->view->setVars(["betslip" => $betslip, 'betslipSession' => $betslipSession, "referenceID" => $referenceID,
            'jackpotSlip'=> $jackpotSlip]);

        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $this->view->pick("partials/betslip");
    }

    public function betwjslipAction() {
        $reference_id = $this->reference();

        $jackpot = $this->request->get('jackpot');

        $betslip = $this->session->get('betslip');

        // $betslip = $this->rawQueries("select b.bet_pick, m.parent_match_id, b.sub_type_id, m.home_team, m.away_team, m.match_id, d.odd_value, b.sub_type_id, concat(t.name,' ',d.special_bet_value) as odd_name from bet_slip_temp b inner join `match` m on b.match_id=m.match_id inner join event_odd d on m.parent_match_id=d.parent_match_id inner join odd_type t on t.sub_type_id=d.sub_type_id where d.sub_type_id=b.sub_type_id and d.odd_key=b.bet_pick and d.special_bet_value=b.special_bet_value and b.reference_id='$reference_id' and bet_type in ('prematch','live')");
       // coreUtils::flog('INFO', "betslip query returns ==>" . print_r($betslip, true), __CLASS__, __FUNCTION__, __LINE__);

        $theBetslip = [];
        
        foreach ($betslip as $slip) {
            
            if ('jackpot' == $slip['bet_type']) {
                $theBetslip[$slip['match_id']] = $slip;
            }
        }

        $betslip = $theBetslip;

        $referenceID = $this->reference();

        $betslipSession = $this->session->get("betslip");

        //coreUtils::flog('INFO', "betslipSession returns ==>" . print_r($betslipSession, true), __CLASS__, __FUNCTION__, __LINE__);

        $this->view->setVars(["betslip" => $betslip, 'betslipSession' => $betslipSession, 
            "referenceID" => $referenceID, 'jackpotSlip'=>1]);

        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $this->view->pick("partials/betslip");
    }

    public function competitionAction() {
        $id = $this->request->get('country');
        $competitions = $this->rawQueries("select c.competition_id, c.competition_name, c.category from competition c inner join `match` m on m.competition_id = c.competition_id where c.sport_id = 5 and c.category='$id' and m.start_time BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY) group by c.competition_name order by c.competition_name");
        $theCompetition = $this->rawQueries("select competition_name,competition_id,category,sport.sport_name from competition left join sport on competition.sport_id=sport.sport_id where category='$id' limit 1");

        $title = $theCompetition['0']['competition_name'] . ", " . $theCompetition['0']['category'];

        $this->tag->setTitle($title);

        $this->view->setVars(["competitions" => $competitions, "theCompetition" => $theCompetition]);
    }

}
