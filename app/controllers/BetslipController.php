<?php

use Phalcon\Session\Bag as SessionBag;
use Phalcon\Http\Response;
use Phalcon\Mvc\View;

class BetslipController extends ControllerBase {

    public function indexAction() {
        $reference_id = $this->reference();

        $jackpot = $this->request->get('jackpot');

        $betslip = $this->session->get('betslip');

        $betslip = $this->rawQueries("select b.bet_pick, m.parent_match_id, b.sub_type_id, m.home_team, m.away_team, m.match_id, d.odd_value, b.sub_type_id, concat(t.name,' ',d.special_bet_value) as odd_name from bet_slip_temp b inner join `match` m on b.match_id=m.match_id inner join event_odd d on m.parent_match_id=d.parent_match_id inner join odd_type t on t.sub_type_id=d.sub_type_id where d.sub_type_id=b.sub_type_id and d.odd_key=b.bet_pick and d.special_bet_value=b.special_bet_value and b.reference_id='$reference_id' and bet_type in ('prematch','live')");

        $referenceID = $this->reference();

        $betslipSession = $this->session->get("betslip");

        $this->view->setVars(["betslip" => $betslip, 'betslipSession' => $betslipSession, "referenceID" => $referenceID]);

        //$this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $this->view->pick("partials/betslip");
    }

    public function jsonAction() {
        $reference_id = $this->reference();

        $betslip = $this->rawQueries("select b.bet_pick, m.parent_match_id, b.sub_type_id, m.home_team, m.away_team, m.match_id, d.odd_value, b.sub_type_id, concat(t.name,' ',d.special_bet_value) as odd_name from bet_slip_temp b inner join `match` m on b.match_id=m.match_id inner join event_odd d on m.parent_match_id=d.parent_match_id inner join odd_type t on t.sub_type_id=d.sub_type_id where d.sub_type_id=b.sub_type_id and d.odd_key=b.bet_pick and d.special_bet_value=b.special_bet_value and b.reference_id='$reference_id' and bet_type in ('prematch','live')");

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $response->setContent(json_encode($betslip));

        return $response;
    }

    public function jackpotAction() {
        $jackpotID = 6; //33;

        $jackpot = $this->request->get('jackpot');
        coreUtils::flog('INFO', "jackpotAction jackpot returns >>>>>> | " . json_encode($jackpot), __CLASS__, __FUNCTION__, __LINE__);
        $betslip = $this->session->get("betslip");
        coreUtils::flog('INFO', "jackpotAction betslip returns >>>>>> | " . json_encode($betslip), __CLASS__, __FUNCTION__, __LINE__);

        $betslipSession[] = '';

        unset($betslipSession);

        foreach ($betslip as $slip) {
            if ($slip['bet_type'] == 'jackpot') {
                $betslipSession[$slip['pos']] = $slip;
            }
        }

        $betslipSession = $this->array_msort($betslipSession, array('pos' => SORT_ASC));
        coreUtils::flog('INFO', "jackpotAction betslipSession returns >>>>>> | " . json_encode($betslipSession), __CLASS__, __FUNCTION__, __LINE__);
        $this->view->setVars(["betslipSession" => $betslipSession, 'jackpotID' => $jackpotID]);

        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $this->view->pick("partials/betslip-jackpot");
    }

    public function bingwaAction() {
        $jackpot = $this->request->get('jackpot');

        $betslip = $this->session->get("betslip");

        $betslipSession[] = '';

        unset($betslipSession);

        foreach ($betslip as $slip) {
            if ($slip['bet_type'] == 'bingwafour') {
                $betslipSession[$slip['pos']] = $slip;
            }
        }

        $betslipSession = $this->array_msort($betslipSession, array('pos' => SORT_ASC));

        $this->view->setVars(["betslip" => $betslip, "betslipSession" => $betslipSession]);

        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $this->view->pick("partials/betslip-bingwa");
    }

    public function pushintobetslipAction() {
        // adding some info into the session
        $ov = $this->request->getPost('over');
        $match_id = $this->request->getPost('match_id', 'int');
        $bets = $this->session->get('betslip');

        if (isset($ov)) {
            coreUtils::flog('INFO', "over/under returns =========>" . print_r($ov, true), __CLASS__, __FUNCTION__, __LINE__);
            $betslip["$match_id"]['ou'] = $ov;
            exit();
        }
        coreUtils::flog('INFO', "addBetSlip returns2 =========>" . print_r($betslip, true), __CLASS__, __FUNCTION__, __LINE__);
    }

    public function addAction() {
        $reference_id = $this->reference();
        $match_id = $this->request->getPost('match_id', 'int');
        $bet_pick = $this->request->getPost('odd_key');
        $sub_type_id = $this->request->getPost('sub_type_id', 'int');
        $special_bet_value = $this->request->getPost('special_bet_value');
        $bet_type = $this->request->getPost('bet_type') ?: 'prematch';
        $home_team = $this->request->getPost('home');
        $away_team = $this->request->getPost('away');
        $odd_value = $this->request->getPost('odd');
        $odd_type = $this->request->getPost('oddtype');
        $parent_match_id = $this->request->getPost('parentmatchid');
        $pos = $this->request->getPost('pos');

        if ($special_bet_value == '0') {
            $special_bet_value = '';
        }

        if ($bet_type == 'live') {
            $this->session->set("betslip", '');
        }

        $data = 'selection updated';

        $betslip[] = '';

        unset($betslip);

        if ($this->session->has("betslip")) {
            $betslip = $this->session->get("betslip");
        }

        $betslip["$match_id"] = ['match_id' => $match_id, 'bet_pick' => $bet_pick, 'sub_type_id' => $sub_type_id, 'special_bet_value' => $special_bet_value, 'bet_type' => $bet_type, 'home_team' => $home_team, 'away_team' => $away_team, 'odd_value' => $odd_value, 'odd_type' => $odd_type, 'parent_match_id' => $parent_match_id, 'pos' => $pos];

       // coreUtils::flog('INFO', "addBetSlip returns =========>" . print_r($betslip, true), __CLASS__, __FUNCTION__, __LINE__);

        $this->session->set("betslip", $betslip);

        $bets = $this->session->get('betslip');

        $theBetslip = [];
        
        foreach ($bets as $slip) {
            
            if ($bet_type == $slip['bet_type']) {
                $theBetslip[$slip['match_id']] = $slip;
            }
        }

        //coreUtils::flog('INFO', "addBetSlip set session =========>" . print_r($theBetslip, true), __CLASS__, __FUNCTION__, __LINE__);
        $count = sizeof($theBetslip);

        $data = ['total' => $count, 'message' => 'Selection updated'];

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $response->setContent(json_encode($data));

        return $response;
    }

    public function removeAction() {

        $reference_id = $this->reference();
        $match_id = $this->request->getPost('match_id', 'int');
        $src = $this->request->getPost('src', 'string');
        $betslip = $this->session->get("betslip");
        $bet_type = $betslip["$match_id"]['bet_type'];
        unset($betslip["$match_id"]);


        $this->session->set("betslip", $betslip);

        $theBetslip = [];
        
        foreach ($betslip as $slip) {
            
            if ($bet_type == $slip['bet_type']) {
                $theBetslip[$slip['match_id']] = $slip;
            }
        }

        $count = sizeof($theBetslip);

        $data = ['total' => $count, 'bet_type'=>$bet_type];

        if ($src == 'mobile') {
            $this->flashSession->success($this->flashSuccess('Match successfully removed'));
            $this->response->redirect('betmobile');
            // Disable the view to avoid rendering
            $this->view->disable();
        } else {
            $response = new Response();
            $response->setStatusCode(201, "OK");
            $response->setHeader("Content-Type", "application/json");

            $response->setContent(json_encode($data));

            return $response;
        }
    }

    public function clearslipAction() {

        $reference_id = $this->reference();

        //$data = $this->rawInsert("delete from bet_slip_temp where reference_id='$reference_id'");

        $this->session->remove("betslip");

        $data = '1';

        $src = $this->request->getPost('src', 'string');

        if ($src == 'mobile') {
            $this->flashSession->success($this->flashSuccess('Betslip cleared'));
            $this->response->redirect('betmobile');
            // Disable the view to avoid rendering
            $this->view->disable();
        } else {
            $response = new Response();
            $response->setStatusCode(201, "OK");
            $response->setHeader("Content-Type", "application/json");

            $response->setContent(json_encode($data));

            return $response;
        }
    }

    public function placebetAction() {

        $reference_id = $this->reference();
        $user_id = $this->request->getPost('user_id', 'int');
        $bet_amount = $this->request->getPost('bet_amount', 'int');
        $total_odd = $this->request->getPost('total_odd', 'int');
        $possible_win = $bet_amount * $total_odd;
        $src = $this->request->getPost('src', 'string');
        $betslip = $this->session->get('betslip');
        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");
        $src == 'mobile';
        if (!$user_id || !$bet_amount || !$total_odd || !$possible_win) {
            if ($src == 'mobile') {
                $this->flashSession->error($this->flashMessages('all fields are required'));
                $this->response->redirect('betmobile');
                $this->view->disable();
            } else {
                $data = ["status_code" => 421, "message" => "All fields are required --------"];
                $response->setContent(json_encode($data));
                return $response;
                $this->view->disable();
            }
        } else {
            if ($bet_amount < 50) {
                if ($src == 'mobile') {
                    $this->flashSession->error($this->flashMessages('Bet amount should be at least Ksh. 50'));
                    $this->response->redirect('betmobile');
                    // Disable the view to avoid rendering
                    $this->view->disable();
                } else {
                    $data = ["status_code" => 421, "message" => "Bet amount should be at least Ksh. 50"];
                    $response->setContent(json_encode($data));
                    return $response;
                    $this->view->disable();
                }
            } else {

                $phql = "SELECT * from Profile where profile_id='$user_id' limit 1";
                $checkUser = $this->modelsManager->executeQuery($phql);

                $checkUser = $checkUser->toArray();

                $mobile = $checkUser['0']['msisdn'];

                $totalMatch = sizeof($betslip);

                $slip = [];

                foreach ($betslip as $match) {
                    $parent_match_id = $match['parent_match_id'];
                    $bet_pick = $match['bet_pick'];
                    $odd_value = $match['odd_value'];
                    $sub_type_id = $match['sub_type_id'];
                    $home_team = $match['home_team'];
                    $away_team = $match['away_team'];
                    $special_bet_value = $match['special_bet_value'];

                    $thisMatch = ["sub_type_id" => $sub_type_id, "special_bet_value" => $special_bet_value, "pick_key" => $bet_pick, "odd_value" => $odd_value, "parent_match_id" => $parent_match_id];

                    $slip[] = $thisMatch;
                }

                $bet = ["bet_string" => 'sms',
                    "possible_win" => $possible_win,
                    "profile_id" => $user_id,
                    "stake_amount" => $bet_amount,
                    "bet_total_odds" => $total_odd,
                    "slip" => $slip];
                    $placeB = $this->bet($bet);

                    if ($placeB['status_code'] == 201) {
                        $message = $placeB['message'];
                        $sms = "msisdn=$mobile&message=$message&short_code=29008&correlator=&message_type=BULK&link_id=";
                        //$this->sendSMS($sms);
                    }

                if ($src == 'mobile') {
                    $feedback = $placeB['message'];
                    if ($placeB['status_code'] == 201) {
                        $feedback = $placeB['message'];
                        $this->session->remove("betslip");
                        $this->flashSession->success($this->flashSuccess($feedback));
                    } else {
                        $this->flashSession->error($this->flashMessages($feedback));
                    }

                    $this->response->redirect('betmobile');
                    // Disable the view to avoid rendering
                    $this->view->disable();
                } else {

                    if ($placeB['status_code'] == 201) {
                        $this->deleteReference();
                        $this->session->remove("betslip");
                        $this->reference();
                    }

                    $response->setContent(json_encode($placeB));
                    return $response;
                    $this->view->disable();
                }
            }
        }
    }

    
    public function betJackpotAction() {

        $reference_id = $this->reference();
        $user_id = $this->request->getPost('user_id', 'int');
        $jackpot_id = $this->request->getPost('jackpot_id', 'int');
        $jackpot_type = $this->request->getPost('jackpot_type', 'int');
        $total_matches = $this->request->getPost('total_matches', 'int');
        $bet_amount = $this->request->getPost('bet_amount', 'int');
        $total_odd = $this->request->getPost('total_odd', 'int');
        $possible_win = $bet_amount * $total_odd;
        $src = $this->request->getPost('src', 'string');
        $betslip = $this->session->get('betslip');
        $bet_type = 'jackpot';

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");
        $src == 'mobile';
        if (!$user_id || !$bet_amount) {
            if ($src == 'mobile') {
                $this->flashSession->error($this->flashMessages('All fields are required'));
                $this->response->redirect('betmobile');
                $this->view->disable();
            } else {
                $data = ["status_code" => 421, "message" => "All fields are required --------"];
                $response->setContent(json_encode($data));
                return $response;
                $this->view->disable();
            }
        } else {
            if ($bet_amount < 50) {
                if ($src == 'mobile') {
                    $this->flashSession->error($this->flashMessages('Bet amount should be at least Ksh. 50'));
                    $this->response->redirect('betmobile');
                    // Disable the view to avoid rendering
                    $this->view->disable();
                } else {
                    $data = ["status_code" => 421, "message" => "Bet amount should be at least Ksh. 50"];
                    $response->setContent(json_encode($data));
                    return $response;
                    $this->view->disable();
                }
            } else {

                $matches = [];

                $betslip = $this->session->get("betslip");

                foreach ($betslip as $slip) {
                    if ($slip['bet_type'] == $bet_type) {
                        $matches[$slip['match_id']] = $slip;
                    }
                }
		
                $matches = $this->array_msort($matches, ['pos' => SORT_ASC]);

                $totalMatch = sizeof($matches);

                if ($totalMatch < $total_matches) {
                    if ($src == 'mobile') {
                        $this->flashSession->error($this->flashMessages('You must select an outcome for all Jackpot Matches'));
                        $this->response->redirect('betmobile');
                        // Disable the view to avoid rendering
                        $this->view->disable();
                    } else {
                        $data = [
                            "status_code"  => "421",
                            "message"      => "You must select an outcome for all Jackpot Matches",
                            "jackpot_type" => $jackpot_type,
                        ];
                        $response->setContent(json_encode($data));

                        return $response;
                        $this->view->disable();
                    }
                } else {

                    $phql = "SELECT * from Profile where profile_id='$user_id' limit 1";
                    $checkUser = $this->modelsManager->executeQuery($phql);

                    $checkUser = $checkUser->toArray();

                    $mobile = $checkUser['0']['msisdn'];

                    $slip = [];

                    foreach ($matches as $match) {
                        $parent_match_id = $match['parent_match_id'];
                        $bet_pick = $match['bet_pick'];
                        $odd_value = $match['odd_value'];
                        $sub_type_id = $match['sub_type_id'];
                        $home_team = $match['home_team'];
                        $away_team = $match['away_team'];
                        $special_bet_value = $match['special_bet_value'];

                        $thisMatch = ["sub_type_id" => $sub_type_id, "special_bet_value" => $special_bet_value, "pick_key" => $bet_pick, "odd_value" => $odd_value, "parent_match_id" => $parent_match_id];

                        $slip[] = $thisMatch;
                    }

                    $bet = ["bet_string" => 'sms',
                        "possible_win" => $possible_win,
                        "profile_id" => $user_id,
                        "jackpot_id" => $jackpot_id,
                        "jackpot_type" => $jackpot_type,
                        "stake_amount" => $bet_amount,
                        "bet_total_odds" => $total_odd,
                        "slip" => $slip];

                        $placeB = $this->betJackpot($bet);

                        if ($placeB['status_code'] == 201) {
                            $message = $placeB['message'];
                            $sms = "msisdn=$mobile&message=$message&short_code=29008&correlator=&message_type=BULK&link_id=";
                            //$this->sendSMS($sms);
                        }

                    if ($src == 'mobile') {
                        $feedback = $placeB['message'];
                        if ($placeB['status_code'] == 201) {
                            $feedback = $placeB['message'];
                            $this->session->remove("betslip");
                            $this->flashSession->success($this->flashSuccess($feedback));
                        } else {
                            $this->flashSession->error($this->flashMessages($feedback));
                        }

                        $this->response->redirect('betmobile');
                        // Disable the view to avoid rendering
                        $this->view->disable();
                    } else {

                        if ($placeB['status_code'] == 201) {
                            $this->deleteReference();
                            $this->session->remove("betslip");
                            $this->reference();
                        }

                        $response->setContent(json_encode($placeB));
                        return $response;
                        $this->view->disable();
                    }
                }
            }
        }
    }


    private function array_msort($array, $cols) {
        $colarr = array();
        foreach ($cols as $col => $order) {
            $colarr[$col] = array();
            foreach ($array as $k => $row) {
                $colarr[$col]['_' . $k] = strtolower($row[$col]);
            }
        }
        $eval = 'array_multisort(';
        foreach ($cols as $col => $order) {
            $eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
        }
        $eval = substr($eval, 0, -1) . ');';
        eval($eval);
        $ret = array();
        foreach ($colarr as $col => $arr) {
            foreach ($arr as $k => $v) {
                $k = substr($k, 1);
                if (!isset($ret[$k]))
                    $ret[$k] = $array[$k];
                $ret[$k][$col] = $array[$k][$col];
            }
        }
        return $ret;
    }

}
