<?php

class DepositController extends ControllerBase {

    public function indexAction() {
        
        if ($this->request->isPost() && $this->session->get('auth')) {

            $amount = $this->request->getPost('amount', 'int');
            $data = 'amount='.$amount.'&msisdn='.$this->session->get('auth')['mobile'];
            $success = $this->topup($data);
            if($success == 200){
               $message = 'Deposit prompt sent to your mobile number. Kindly follow the prompt to complete your request';
            }else{
               $message = 'Failed to sent deposit request. Kindly try again';
            }

           $this->flashSession->error($this->flashMessages($message));
        }

        $this->view->setVars(["topLeagues" => $this->topLeagues()]);
        $this->tag->setTitle('How to Deposit');
    }
    

    public function topupAction() {
        $amount = $this->request->getPost('amount', 'int');

        if ($amount < 10) {
            $this->flashSession->error($this->flashMessages('Sorry, minimum top up amount is Ksh. 10'));
            return $this->response->redirect('deposit');
            $this->view->disable();
        } elseif ($amount > 70000) {
            $this->flashSession->error($this->flashMessages('Sorry, maximum top up amount is Ksh. 70,000'));
            return $this->response->redirect('deposit');
            $this->view->disable();
        } else {
            $mobile = $this->session->get('auth')['mobile'];

            $push = "msisdn=$mobile&amount=$amount";
            $data['msisdn'] = $mobile;
            $data['amount'] = $amount;

            $this->topup($push);
            //$this->topupmpesa($data);

            $this->flashSession->error($this->flashSuccess('Lipa na M-PESA Online transaction has been initiated on your phone, your account will credited immediately on successful completion of the transaction.'));

            $this->response->redirect('deposit');
            // Disable the view to avoid rendering
            $this->view->disable();
        }
    }

}