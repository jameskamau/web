<?php

use Phalcon\Mvc\Controller;
use Phalcon\Crypt;
use \Firebase\JWT\JWT;

class ControllerBase extends Controller {

    public function beforeExecuteRoute($dispatcher) {
        if ($this->cookies->has('auth')) {
            $token = $this->cookies->get('auth');
            $key = "5eBOGiKXt7dsKwaaJcRX8owIH7BbJ8F9";
            if (!$this->session->has("auth")) {
                try {
                    $user = JWT::decode($token, $key, array('HS256'));
                    $user = $user->user;
                    if ($user->remember == '1' || $user->device == '1') {
                        $user = ['id' => $user->id, 'name' => $user->name, 'mobile' => $user->mobile, 'device' => $user->device, 'remember' => $user->remember];
                        $this->_registerSession($user);
                    }
                } catch (Exception $e) {
                    $decoded = $e->getMessage();
                }
            }
        }

        /**
         79 | Soccer, 80 | Tennis, 82 | Ice Hockey, 
         83 | Handball, 84 | Volleyball, 85 | Basketball       
         86 | Badminton, 96 | Futsal, 97 | Rugby            
         98 | Cricket, 99 | Darts, 100 | Bowls, 102 | Snooker          
        104 | Table Tennis, 105 | Beach Volley     
        106 | Dota 2, 107 | Baseball, 108 | Squash           
        109 | Aussie Rules, 162 | American Football
        163 | Counter-Strike, 164 | League of Legends **/

        $sportType = [
            '79' => 'competition',
            '85' => 'twoway',
            '97' => 'competition',
            '84' => 'twoway',
            '82' => 'competition',
            '83' => 'competition',
            '86' => 'competition',
            '96' => 'competition',
            '98' => 'twoway',
            '99' => 'competition',
            '100' => 'competition',
            '102' => 'competition',
            '104' => 'competition',
            '105' => 'competition',
            '106' => 'competition',
            '107' => 'competition',
            '108' => 'competition',
            '109' => 'competition',
            '162' => 'competition',
            '163' => 'competition',
            '164' => 'competition',
            '165' => 'twoway',
            '80' => 'twoway',
            '239'=> 'competition',
        ];


        $betslip = $this->session->get('betslip');
        $slipCount = sizeof($betslip);

        $jackpotSlip = 0;

        $sports = $this->sports();
        //coreUtils::flog('INFO', "beforeExecutionRoute returns >>>>>> | " . json_encode($sports), __CLASS__, __FUNCTION__, __LINE__);
        $this->view->setVars(['sports' => $sports, 'slipCount' => $slipCount, 'sportType' => $sportType, 'betslip'   => $betslip, 'jackpotSlip'=>$jackpotSlip]);
    }


     /**
     * @return mixed
     */
    protected function sports() {
        $sports = $this->rawQueries("select ux.ux_id, ux.sport_name, if(LENGTH(ux.category) <17, ux.category, concat(SUBSTRING(ux.category, 1,15), '...')) as category, if(LENGTH(ux.competition_name) <17, ux.competition_name, concat(SUBSTRING(ux.competition_name, 1,15), '...')) as competition_name, ux.sport_id, ux.category_id, ux.competition_id, country_code, (select count(*) from `match` m where competition_id = ux.competition_id and m.bet_closure > now() and m.status=1)as games, (select count(*) from `match` mm where competition_id in (select competition_id from competition cc where category_id=ux.category_id and mm.bet_closure > now() and mm.status=1 ))as cat_games from ux_categories ux inner join category c on c.category_id = ux.category_id having games > 0 order by ux.sport_id asc, ux.category asc");

        #$sports = $this->rawQueries("select ux.ux_id, ux.sport_name, if(LENGTH(ux.category) <17, ux.category, concat(SUBSTRING(ux.category, 1,15), '...')) as category, if(LENGTH(ux.competition_name) <17, ux.competition_name, concat(SUBSTRING(ux.competition_name, 1,15), '...')) as competition_name, ux.sport_id, ux.category_id, ux.competition_id, country_code from ux_categories ux inner join category c on c.category_id = ux.category_id order by ux.sport_id asc, ux.category asc ");
        return $sports;
    }

    protected function getMicrotime() {
        list ($msec, $sec) = explode(" ", microtime());
        return ((float) $msec + (float) $sec);
    }

    protected function topLeagues() {
        $leagues = $this->rawQueries('select c.competition_id, c.competition_name, c.category from competition c inner join `match` m on m.competition_id = c.competition_id where c.sport_id = 79 and m.start_time BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 DAY) group by m.competition_id order by c.priority desc , c.competition_name limit 10');

        return $leagues;
    }

    protected function allLeagues() {
        $leagues = $this->rawQueries('select * from ux_categories where sport_id = 79');

        return $leagues;
    }

    protected function rugby() {
        $rugby = $this->rawQueries('select category,competition_id, competition_name from competition where sport_id=41 group by category order by category asc');

        return $rugby;
    }

    /**
     * @param $statement
     *
     * @param array $bindings
     *
     * @return mixed
     */
    protected function rawQueries($statement, $bindings = [])
    {
        $connection = $this->di->getShared("db");
        $success = $connection->query($statement, $bindings);
        $success->setFetchMode(Phalcon\Db::FETCH_ASSOC);
        $success = $success->fetchAll($success);

        return $success;
    }


    protected function rawInsert($statement) {
        $connection = $this->di->getShared("db");
        $success = $connection->query($statement);

        $id = $connection->lastInsertId();

        return $id;
    }

    protected function flashMessages($message) {
        return $message;
    }

    protected function flashSuccess($message) {
        return $message;
    }

    protected function reference() {

        if (!$this->cookies->has('referenceID')) {

            $crypt = new Crypt();

            $key = 'FbxH8j7SPeeVDE7i';
            $text = $_SERVER['HTTP_USER_AGENT'] . time() . uniqid();

            $key = $crypt->encryptBase64($text, $key);

            $this->cookies->set('referenceID', $key, time() + 15 * 86400);
        }

        $referenceID = $this->cookies->get('referenceID');

        $referenceID = $referenceID->getValue();

        return $referenceID;
    }

    protected function registerAuth($user) {
        $exp = time() + (3600 * 24 * 5);
        $token = $this->generateToken($user, $exp);
        $this->cookies->set('auth', $token, $exp);
        $this->_registerSession($user);
    }

    private function _registerSession($user) {
        $this->session->set('auth', $user);
    }

    protected function generateToken($data, $exp) {
        $key = "5eBOGiKXt7dsKwaaJcRX8owIH7BbJ8F9";
        $token = array(
            "iss" => "http://scorepesa.co.ke",
            "iat" => 1356999524,
            "nbf" => 1357000000,
            "exp" => $exp,
            "user" => $data
        );

        $jwt = JWT::encode($token, $key);

        return $jwt;
    }

    protected function decodeToken($token) {
        $key = "5eBOGiKXt7dsKwaaJcRX8owIH7BbJ8F9";
        $decoded = JWT::decode($token, $key, array('HS256'));
        return $decoded;
    }

    protected function deleteReference() {

        $this->cookies->set('referenceID');
    }

    protected function getDevice() {
        $device = '2';
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            $device = '1';
        }
        return $device;
    }

    protected function sendSMS($sms) {
        
        //coreUtils::flog('INFO', "sendSMS function" . print_r($sms, true), __CLASS__, __FUNCTION__, __LINE__);

        $URL = "35.187.20.191:8787/sendsms";

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, "$sms");
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = json_decode($results);
        //coreUtils::flog('INFO', "sendSMS response" . print_r($results, true), __CLASS__, __FUNCTION__, __LINE__);
        return $response;
    }

    protected function topup($data) {

        $URL = "35.187.20.191:1580/mpesa/deposit";

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, "$data");
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = json_decode($results);

        return $status_code;
    }

    protected function topupmpesa($data) {

        $url = "http://127.0.0.1/payment_relay/demo.php";
        $params = [
            "msisdn" => $data['msisdn'],
            "amount" => $data['amount'],
            "callback_url" => '',
            "reference_id" => 'shikabet',
            "paybill" => 290028,
        ];

        $params = json_encode($params);

        $httpRequest = curl_init($url);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $params);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($params)));
        curl_setopt($httpRequest, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);
        $response = ["status_code" => $status_code, "message" => $results];
        return $response;
    }

    protected function withdraw($transaction) {

        $URL = "35.187.20.191:8787/macatm";

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, "$transaction");
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = json_decode($results);

        return $status_code;
    }

    protected function bonus($transaction) {

        $URL = "35.187.20.191:8787/profilemgt";

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, "$transaction");
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = json_decode($results);

        return $status_code;
    }

    protected function betTransaction($transaction) {
        $URL = "35.187.20.191:8787/bet";

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, "$transaction");
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = ["status_code" => $status_code, "message" => $results];

        return $response;
    }

    protected function bet($data) {

        $URL = "http://35.187.20.191:8787/bet";
        $bet = json_encode($data);
        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $bet);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($bet)));
        curl_setopt($httpRequest, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = ["status_code" => $status_code, "message" => $results];
        //coreUtils::flog('INFO', "response is --" . json_encode($response), __CLASS__, __FUNCTION__, __LINE__);
        return $response;
    }

    
    protected function betJackpot($data) {

        $URL = "http://35.187.20.191:8787/jp/bet";
        $bet = json_encode($data);
        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $bet);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($bet)));
        curl_setopt($httpRequest, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = ["status_code" => $status_code, "message" => $results];
        return $response;
    }


    protected function formatMobileNumber($number) {
        $regex = '/^(?:\+?(?:[1-9]{3})|0)?7([0-9]{8})$/';

        if (preg_match_all($regex, $number, $capture)) {
            $msisdn = '2547' . $capture[1][0];
        } else {
            $msisdn = false;
        }

        return $msisdn;
    }

    public function postToUrl($data, $url) {
        $encodedData = json_encode($data);
        $httpRequest = curl_init($url);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $encodedData);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($encodedData)));
        curl_setopt($httpRequest, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);
        $response = ["status_code" => $status_code, "message" => $results];
        return $response;
    }

        /**
     * @param $keyword
     * @param $skip
     * @param $limit
     *
     * @param string $filter
     * @param string $orderBy
     *
     * @return array
     */
    /*protected function getGames($keyword, $skip, $limit, $filter = "", $orderBy = "start_time asc")
    {
        $search_where = "where start_time >= now() and date(start_time) <= date(now() + INTERVAL 1 MONTH) " . $filter;
        $where = "where start_time >= now() and date(start_time) <= date(now() + INTERVAL 1 MONTH) and home_odd is not null and under_25_odd is not null " . $filter;

        if ($keyword) {

            $bindings = [
                'keyword' => "%$keyword%",
            ];

            $whereClause = "$search_where and (game_id like :keyword or home_team like :keyword or away_team like :keyword or competition_name like :keyword)";
            
            // $query = "SELECT * FROM ux_todays_highlights $whereClause ORDER BY $orderBy LIMIT $skip, $limit";
            
            $query = "SELECT c.priority, (SELECT count(DISTINCT e.sub_type_id)
                        FROM event_odd e INNER JOIN odd_type o ON (o.sub_type_id = e.sub_type_id AND 
                        o.parent_match_id = e.parent_match_id) WHERE e.sub_type_id not in 
                        (select sub_type_id from prematch_disabled_market where status = 1) 
                        and e.parent_match_id = m.parent_match_id
                        AND o.active = 1) AS side_bets, o.sub_type_id, 
                        MAX(CASE WHEN o.odd_key = m.home_team THEN odd_value END) AS home_odd,
                        MAX(CASE WHEN o.odd_key = 'draw' THEN odd_value END) AS neutral_odd,
                        MAX(CASE WHEN o.odd_key = m.away_team THEN odd_value END) AS away_odd,
                        MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  'draw') 
                        THEN odd_value END) AS double_chance_1x_odd, 
                        MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', 'draw ' 'or',  m.away_team) THEN odd_value END)
                        AS double_chance_x2_odd, MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  
                        m.away_team) THEN odd_value END) AS double_chance_12_odd, MAX(CASE WHEN o.odd_key =
                        'under 2.5' THEN odd_value END) AS under_25_odd, MAX(CASE WHEN o.odd_key = 'over 2.5'
                        THEN odd_value END) AS over_25_odd, m.game_id, m.match_id, m.start_time,
                        m.away_team, m.home_team, m.parent_match_id,c.competition_name,c.category 
                        FROM `match` m INNER JOIN event_odd o ON m.parent_match_id = o.parent_match_id
                        INNER JOIN competition c ON c.competition_id = m.competition_id INNER JOIN sport s
                        ON s.sport_id = c.sport_id $whereClause  AND o.sub_type_id IN (1,10,18) AND m.status <> 3
                         GROUP BY m.parent_match_id ORDER BY m.priority DESC, c.priority DESC, m.start_time
                          LIMIT $skip,$limit";

            $items = $this->rawQueries("select count(*) as total FROM `match` m INNER JOIN
             event_odd o ON m.parent_match_id = o.parent_match_id INNER JOIN competition c
              ON c.competition_id = m.competition_id INNER JOIN sport s ON s.sport_id
               = c.sport_id $whereClause LIMIT 1", $bindings);

            $today = $this->rawQueries($query, $bindings);
            // $items = $this->rawQueries("select count(*) as total from ux_todays_highlights $whereClause limit 1", $bindings);
        } else {
            $whereClause = "$where ";

            $today = $this->rawQueries("select * from ux_todays_highlights s where 
                s.m_priority >= 0 and s.start_time > now() and date(s.start_time) 
                <= date(now() + INTERVAL 10 DAY) and s.home_odd is not null and 
                s.under_25_odd is not null order by s.m_priority desc, s.priority desc,
                s.start_time asc limit $skip, $limit");

            $items = $this->rawQueries("select count(*) as total from 
                ux_todays_highlights s $whereClause limit 1");
        }

        // $results = [];

        // foreach ($today as $day) {
        //     $results[(new DateTime($day['start_time']))->format($this->getDefaultDateFormat())][] = $day;
        // }

        return [
            $today,
            $items,
            $this->getCompetitions(),
        ];
    }*/


     protected function getGames($keyword, $skip, $limit, $filter = "", $orderBy = "start_time asc")
    {
        $search_where = "where start_time >= now() and date(start_time) <= date(now() + INTERVAL 1 MONTH) " . $filter;
        $where = "where start_time >= now() and date(start_time) <= date(now() + INTERVAL 1 MONTH) and home_odd is not null and under_25_odd is not null " . $filter;

        if ($keyword) {

            $bindings = [
                'keyword' => "%$keyword%",
            ];

            $whereClause = "$search_where and (game_id like :keyword or home_team like :keyword or away_team like :keyword or competition_name like :keyword)";
            
            // $query = "SELECT * FROM ux_todays_highlights $whereClause ORDER BY $orderBy LIMIT $skip, $limit";
            
            $query = "SELECT c.priority, (SELECT count(DISTINCT e.sub_type_id)
                        FROM event_odd e INNER JOIN odd_type o ON (o.sub_type_id = e.sub_type_id AND 
                        o.parent_match_id = e.parent_match_id) WHERE e.sub_type_id not in 
                        (select sub_type_id from prematch_disabled_market where status = 1) 
                        and e.parent_match_id = m.parent_match_id
                        AND o.active = 1) AS side_bets, o.sub_type_id, 
                        MAX(CASE WHEN o.odd_key = m.home_team THEN odd_value END) AS home_odd,
                        MAX(CASE WHEN o.odd_key = 'draw' THEN odd_value END) AS neutral_odd,
                        MAX(CASE WHEN o.odd_key = m.away_team THEN odd_value END) AS away_odd,
                        MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  'draw') 
                        THEN odd_value END) AS double_chance_1x_odd, 
                        MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', 'draw ' 'or',  m.away_team) THEN odd_value END)
                        AS double_chance_x2_odd, MAX(CASE WHEN o.odd_key = CONCAT_WS(' ', m.home_team, 'or',  
                        m.away_team) THEN odd_value END) AS double_chance_12_odd, MAX(CASE WHEN o.odd_key =
                        'under 2.5' THEN odd_value END) AS under_25_odd, MAX(CASE WHEN o.odd_key = 'over 2.5'
                        THEN odd_value END) AS over_25_odd, m.game_id, m.match_id, m.start_time,
                        m.away_team, m.home_team, m.parent_match_id,c.competition_name,c.category 
                        FROM `match` m INNER JOIN event_odd o ON m.parent_match_id = o.parent_match_id
                        INNER JOIN competition c ON c.competition_id = m.competition_id INNER JOIN sport s
                        ON s.sport_id = c.sport_id $whereClause  AND o.sub_type_id IN (1,10,18) AND m.status <> 3
                         GROUP BY m.parent_match_id ORDER BY m.priority DESC, c.priority DESC, m.start_time
                          LIMIT $skip,$limit";

            $items = $this->rawQueries("select count(*) as total FROM `match` m INNER JOIN
             event_odd o ON m.parent_match_id = o.parent_match_id INNER JOIN competition c
              ON c.competition_id = m.competition_id INNER JOIN sport s ON s.sport_id
               = c.sport_id $whereClause LIMIT 1", $bindings);

            $today = $this->rawQueries($query, $bindings);
            // $items = $this->rawQueries("select count(*) as total from ux_todays_highlights $whereClause limit 1", $bindings);
        } else {
            $whereClause = "$where ";

            $today = $this->rawQueries("select * from ux_todays_highlights s where 
                s.m_priority >= 0 and s.start_time > now() and date(s.start_time) 
                <= date(now() + INTERVAL 10 DAY) and s.home_odd is not null and 
                s.under_25_odd is not null order by s.m_priority desc, s.priority desc,
                s.start_time asc limit $skip, $limit");

            $items = $this->rawQueries("select count(*) as total from 
                ux_todays_highlights s $whereClause limit 1");
        }

        // $results = [];

        // foreach ($today as $day) {
        //     $results[(new DateTime($day['start_time']))->format($this->getDefaultDateFormat())][] = $day;
        // }

        return [
            $today,
            $items,
            $this->getCompetitions(),
        ];
    }


    /**
     * @return array
     */
    protected function getCompetitions()
    {
        return [
            'UEFA Youth League, Group A'                     => 'UEFA Youth',
            'UEFA Youth League, Group B'                     => 'UEFA Youth',
            'UEFA Youth League, Group C'                     => 'UEFA Youth',
            'UEFA Youth League, Group D'                     => 'UEFA Youth',
            'UEFA Youth League, Group E'                     => 'UEFA Youth',
            'UEFA Youth League, Group F'                     => 'UEFA Youth',
            'UEFA Youth League, Group H'                     => 'UEFA Youth',
            'UEFA Youth League, Group G'                     => 'UEFA Youth',
            'Int. Friendly Games, Women'                     => 'Women',
            'U17 European Championship, Group B'             => 'U17',
            'U17 European Championship, Qualification Gr. 9' => 'U17',
            'U21'                                            => 'U21',
            'Premier League 2, Division 1'                   => 'Amateur',
            'Premier League 2, Division 2'                   => 'Amateur',
            'Youth League'                                   => 'Youth',
            'Development League'                             => 'Youth',
            'U19 European Championship Qualif. - Gr. 1'      => 'U19',
            'U19 European Championship Qualif. - Gr. 2'      => 'U19',
            'U19 European Championship Qualif. - Gr. 3'      => 'U19',
            'U19 European Championship Qualif. - Gr. 4'      => 'U19',
            'U19 European Championship Qualif. - Gr. 5'      => 'U19',
            'U19 European Championship Qualif. - Gr. 6'      => 'U19',
            'U19 European Championship Qualif. - Gr. 7'      => 'U19',
            'U19 European Championship Qualif. - Gr. 8'      => 'U19',
            'U19 European Championship Qualif. - Gr. 9'      => 'U19',
            'U19 European Championship Qualif. - Gr. 10'     => 'U19',
            'U19 European Championship Qualif. - Gr. 11'     => 'U19',
            'U19 European Championship Qualif. - Gr. 12'     => 'U19',
            'U19 European Championship Qualif. - Gr. 13'     => 'U19',
            'U19 Int. Friendly Games'                        => 'U19',
            'U20 Friendly Games'                             => 'U20',
            'U21 Friendly Games'                             => 'U21',
            'U21 EURO, Qualification, Group 1'               => 'U21',
            'U21 EURO, Qualification, Group 2'               => 'U21',
            'U21 EURO, Qualification, Group 3'               => 'U21',
            'U21 EURO, Qualification, Group 4'               => 'U21',
            'U21 EURO, Qualification, Group 5'               => 'U21',
            'U21 EURO, Qualification, Group 6'               => 'U21',
            'U21 EURO, Qualification, Group 7'               => 'U21',
            'U21 EURO, Qualification, Group 8'               => 'U21',
            'UEFA Youth League, Knockout stage'              => 'UEFA Youth',
            'U21 EURO, Qualification, Group 9'               => 'U21',
            'U21 EURO, Qualification, Playoffs'              => 'U21',
            'UEFA Champions League, Women'                   => 'W',
            'Africa Cup Of Nations, Women, Group A'          => 'W',
            'Primera Division Femenina'                      => 'W',
            'W-League'                                       => 'W',
            'A-Jun-BL West'                                  => 'U19',
            'Bundesliga, Women'                              => 'W',
            'Campionato Primavera, Girone A'                 => 'Youth',
            'Campionato Primavera, Girone B'                 => 'Youth',
            'Campionato Primavera, Girone C'                 => 'Youth',
        ];
    }

    /**
     * @return array
     */
    protected function getPaginationParams()
    {
        $page = $this->request->get('page', 'int') ?: 0;
        if ($page < 0) {
            $page = 0;
        }
        $limit = $this->request->get('limit', 'int') ?: 100;
        $skip = $page * $limit;

        return [
            $page,
            $limit,
            $skip,
        ];
    }


    /**
     * @param $total
     * @param $limit
     *
     * @return float|int
     */
    protected function getResultPages($total, $limit)
    {
        $pages = ceil($total / $limit);

        if ($pages > 14) {
            $pages = 14;
        }

        return $pages;
    }


    /**
     * @return string
     */
    protected function getDefaultDateFormat()
    {
        return 'l jS, F Y';
    }


    protected function topSports(){
        return array(
           '79'=>'Soccer',
            '85'=>'Basketball',
            '97'=>'Rugby',
            '98'=>'Cricket',
            '80' =>'Tennis',
            '84' =>'Volleyball',
            '82' =>'Ice Hockey',
            '107' =>'Baseball',
            '163' =>'Field Hockey',
            '165' =>'Football'
        );

    }

}
