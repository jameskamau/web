<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Http\Response;

class IndexController extends ControllerBase {

    public function initialize() {
        $this->tag->setTitle('About us');
        $connection = $this->di->getShared("db");
    }

    public function indexAction() {

        $sport_id = $this->request->get('sp', 'int');
        $keyword = $this->request->getPost('keyword', 'string');

        if(empty($sport_id)) {$sport_id = 79;}

        //$sCompetitions = $this->getCompetitions();
	    list($page, $limit, $skip) = $this->getPaginationParams();

        //$today=$this->rawQueries("select * from ux_todays_highlights where m_priority >= 0 and start_time > now() and date(start_time) <= date(now() + INTERVAL 10 DAY) and home_odd is not null and under_25_odd is not null order by m_priority desc, priority desc,start_time asc limit $skip, $limit");

        $where = "";
        if ($sport_id){
            $where = " and s.sport_id = '$sport_id'";
        }

        $orderBy = "m_priority desc, priority desc, start_time asc";

        list($today, $total, $sCompetitions) = $this->getGames($keyword, $skip, $limit, $where, $orderBy);

        $total = $total['0']['total'];

        $tomorrow=[];
        //$this->rawQueries("select * from ux_todays_highlights where m_priority >= 0 and date(start_time) = date(now() + INTERVAL 1 DAY) order by m_priority desc, priority desc limit 40");

        $theBetslip = $this->session->get("betslip");

        $this->view->setVars([
            'today'         => $today,
            'tomorrow'      => $tomorrow,
            'theBetslip'    => $theBetslip,
            'sCompetitions' => $sCompetitions,
            'jackpotSlip'   => 0,
            'total'         => $total,
            'pages'         => $this->getResultPages($total, $limit),
            'page'          => $page,
            'topSports'     => $this->topSports(),
            'sportId'       => $sport_id,
            'keyword'       => $keyword
        ]);

        $this->tag->setTitle("ScorePesa - Leading sports betting site in Kenya.");
    }

    public function matchjsonAction() {
        $today = $this->rawQueries("select * from ux_todays_highlights where start_time > now() limit 30");

        $this->cookies->set('thematch', "this is a new cookie", time() + 15 * 86400);

        $cookieMatches = $this->cookies->get('thematch');

        $cookieMatches = $cookieMatches->getValue();
    }

    public function aboutAction($statement) {
        if ($this->view->getCache()->exists("sidebar")) {
            return $this->forward('/');
        }
    }

    public function signupAction() {
        # code...
    }

}
