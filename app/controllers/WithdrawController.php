<?php

class WithdrawController extends ControllerBase {

    public function indexAction() {
        // $data = configs::$MO_CONSUMER_ENDPOINTS;
        // coreUtils::flog('INFO', "in withdraw function >>>>>> | ". json_encode($data), __CLASS__, __FUNCTION__, __LINE__);
        $this->tag->setTitle('Online withdrawal');
    }

    public function withdrawalAction() {

        // ControllerBase::flog('INFO', "in withdraw function ++++++++++++ | ", __CLASS__, __FUNCTION__, __LINE__);
        $amount = $this->request->getPost('amount', 'int');

        if ($amount < 50) {
            $this->flashSession->error($this->flashMessages('Sorry, minimum withdraw amount is Ksh. 50.'));
            return $this->response->redirect('withdraw');
            $this->view->disable();
        } elseif ($amount > 70000) {
            $this->flashSession->error($this->flashMessages('Sorry, maximum withdraw amount is Ksh. 70,000.'));
            return $this->response->redirect('deposit');
            $this->view->disable();
        } else {
            $mobile = $this->session->get('auth')['mobile'];

            $data = ['amount' => $amount, 'msisdn' => $mobile];

            $exp = time() + 3600;

            $token = $this->generateToken($data, $exp);

            $transaction = "token=$token";

            $withdraw = $this->withdraw($transaction);

            $this->flashSession->success($this->flashSuccess('Your withdrawal is being processed, you will receive a confirmation on SMS shortly'));

            $this->response->redirect('withdraw');

            $this->view->disable();
        }
    }

}
