<?php

class BetmobileController extends ControllerBase
{

    public function indexAction()
    {
    	$reference_id = $this->reference();

    	$betslip = $this->session->get('betslip'); 

    	$bets = [];

        foreach ($betslip as $slip) {
            if ($slip['bet_type'] == 'prematch') {
                $jackpotSlip = 0; 
            }else{
                $jackpotSlip = 1; 
            }
            $bets[$slip['pos']] = $slip;
        }

        if($jackpotSlip == 1){

            $jackpot = $this->rawQueries("SELECT jackpot_type,jackpot_event_id,total_games FROM jackpot_event WHERE status = 'ACTIVE' ORDER BY 1 DESC LIMIT 1");

            $jackpotID = $jackpot['0']['jackpot_event_id'];
            $jackpotType = $jackpot['0']['jackpot_type'];
            $totalMatches = $jackpot['0']['total_games'];
        }

    	$referenceID = $this->reference();

    	$this->tag->setTitle('Betslip - Shindabet');

    	$this->view->setVars(["betslip"=>$betslip, "referenceID"=>$referenceID, 
            "slipCount"=>count($bets), "jackpotType"=>$jackpotType,
            "jackpotSlip"=>$jackpotSlip,"jackpotID"=>$jackpotID]);

    }

}

