<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Http\Response;

class GamesController extends ControllerBase
{
	public function initialize()
    {
        $this->tag->setTitle('About us');
    }

    public function indexAction()
    {
        $sCompetitions = $this->getCompetitions();
        list($page, $limit, $skip) = $this->getPaginationParams();
        $tab = $this->request->get('t', 'int');
        $tab = $tab == null ? -1 : $tab;
        $select_date = null;
        $filter_date = "";
        if($tab > 0){
            $tab -= 1;
            $date = new DateTime(); 
            $interval = new DateInterval('P'.$tab.'D');
            $date->add($interval);
            $filter_date = " and date(start_time) =  '"
                . $date->format('Y-m-d') . "' ";
        }

    	$today=$this->rawQueries("select * from ux_todays_highlights where start_time > now() $filter_date and home_odd is not null and over_25_odd is not null order by start_time limit $skip, $limit");

     

        $theBetslip = $this->session->get("betslip");

        $this->view->setVars(['today'=>$today,'theBetslip'=>$theBetslip,'sCompetitions'=>$sCompetitions]);

    	$this->tag->setTitle('ScorePesa - The Best in Online & SMS Sports Betting, Jackpot, Bonuses and More');

        if($tab != -1){

            //select template to load tab only
            //$this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);
            $this->view->pick("partials/upcoming");
        }
    }

}

