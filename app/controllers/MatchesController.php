<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\View;

class MatchesController extends ControllerBase {

    public function indexAction() {

        $today = $this->rawQueries("select c.priority, (select count(distinct sub_type_id) from event_odd where parent_match_id = m.parent_match_id) as side_bets, o.sub_type_id, MAX(CASE WHEN o.odd_key = '1' THEN odd_value END) AS home_odd, MAX(CASE WHEN o.odd_key = 'x' THEN odd_value END) AS neutral_odd, MAX(CASE WHEN o.odd_key = '2' THEN odd_value END) AS away_odd, m.game_id, m.match_id, m.start_time, m.away_team, m.home_team, m.parent_match_id from `match` m inner join event_odd o on m.parent_match_id = o.parent_match_id inner join competition c on c.competition_id = m.competition_id inner join sport s on s.sport_id = c.sport_id where date(m.start_time) = date(now()) and m.start_time > now() and s.sport_id = 5 and o.sub_type_id = 10 group by m.parent_match_id order by m.priority desc, c.priority desc , m.start_time limit 30");

        $theBetslip = $this->session->get("betslip");

        $this->view->setVars(["topLeagues" => $this->topLeagues(), 'today' => $today, 'theBetslip' => $theBetslip]);

        $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

        $this->view->pick("partials/matches");
    }

    public function listmatchAction() {
        $sCompetitions = ['UEFA Youth League, Group A' => 'UEFA Youth', 'UEFA Youth League, Group B' => 'UEFA Youth', 'UEFA Youth League, Group C' => 'UEFA Youth', 'UEFA Youth League, Group D' => 'UEFA Youth', 'UEFA Youth League, Group E' => 'UEFA Youth', 'UEFA Youth League, Group F' => 'UEFA Youth', 'UEFA Youth League, Group H' => 'UEFA Youth', 'UEFA Youth League, Group G' => 'UEFA Youth', 'Int. Friendly Games, Women' => 'Women', 'U17 European Championship, Group B' => 'U17', 'Int. Friendly Games, Women' => 'Women', 'U17 European Championship, Qualification Gr. 9' => 'U17', 'U21' => 'U21', 'Premier League 2, Division 1' => 'Amateur', 'Premier League 2, Division 2' => 'Amateur', 'Youth League' => 'Youth', 'Development League' => 'Youth', 'U19 European Championship Qualif. - Gr. 1' => 'U19', 'U19 European Championship Qualif. - Gr. 2' => 'U19', 'U19 European Championship Qualif. - Gr. 3' => 'U19', 'U19 European Championship Qualif. - Gr. 4' => 'U19', 'U19 European Championship Qualif. - Gr. 5' => 'U19', 'U19 European Championship Qualif. - Gr. 6' => 'U19', 'U19 European Championship Qualif. - Gr. 7' => 'U19', 'U19 European Championship Qualif. - Gr. 8' => 'U19', 'U19 European Championship Qualif. - Gr. 9' => 'U19', 'U19 European Championship Qualif. - Gr. 10' => 'U19', 'U19 European Championship Qualif. - Gr. 11' => 'U19', 'U19 European Championship Qualif. - Gr. 12' => 'U19', 'U19 European Championship Qualif. - Gr. 13' => 'U19', 'U19 European Championship Qualif. - Gr. 8' => 'U19', 'U19 Int. Friendly Games' => 'U19', 'U20 Friendly Games' => 'U20', 'U21 Friendly Games' => 'U21', 'U21 EURO, Qualification, Group 1' => 'U21', 'U21 EURO, Qualification, Group 2' => 'U21', 'U21 EURO, Qualification, Group 3' => 'U21', 'U21 EURO, Qualification, Group 4' => 'U21', 'U21 EURO, Qualification, Group 5' => 'U21', 'U21 EURO, Qualification, Group 6' => 'U21', 'U21 EURO, Qualification, Group 7' => 'U21', 'U21 EURO, Qualification, Group 8' => 'U21', 'UEFA Youth League, Knockout stage' => 'UEFA Youth', 'U21 EURO, Qualification, Group 9' => 'U21'];

        $today = $this->rawQueries("select * from ux_todays_highlights where m_priority >= 0 and start_time > now() and date(start_time) = date(now()) order by m_priority desc, priority desc limit 55");

        $tomorrow = $this->rawQueries("select * from ux_todays_highlights where m_priority >= 0 and date(start_time) = date(now() + INTERVAL 1 DAY) order by m_priority desc, priority desc limit 40");

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $response->setContent(json_encode($today));

        return $response;
    }

}
