<?php

class TwowayController extends ControllerBase
{

    public function indexAction()
    {
    	$id = $this->request->get('id','int');
        list($page, $limit, $skip) = $this->getPaginationParams();

        $matches = $this->rawQueries("SELECT c.priority, (SELECT count(DISTINCT e.sub_type_id) FROM event_odd e INNER JOIN odd_type o ON (o.sub_type_id = e.sub_type_id AND o.parent_match_id = e.parent_match_id) WHERE e.parent_match_id = m.parent_match_id AND o.active = 1) AS side_bets, o.sub_type_id, MAX(CASE WHEN o.odd_key = m.home_team THEN odd_value END) AS home_odd, MAX(CASE WHEN o.odd_key = 'draw' THEN odd_value END) AS neutral_odd, MAX(CASE WHEN o.odd_key = m.away_team THEN odd_value END) AS away_odd, m.game_id, m.match_id, m.start_time, m.away_team, m.home_team, m.parent_match_id,c.competition_name,c.category, sport_name FROM `match` m INNER JOIN event_odd o ON m.parent_match_id = o.parent_match_id INNER JOIN competition c ON c.competition_id = m.competition_id INNER JOIN sport s ON s.sport_id = c.sport_id WHERE c.competition_id=? AND m.start_time > now() AND o.sub_type_id = 186 AND m.status <> 3 GROUP BY m.parent_match_id ORDER BY m.priority DESC, c.priority DESC , m.start_time LIMIT $skip,$limit", [
            $id,
        ]);

        $sport_id = $this->request->get('sp', 'int');
        $keyword = $this->request->getPost('keyword', 'string');

    	$theCompetition = $this->rawQueries("select competition_name,competition_id,category, sport_name from competition c inner join sport s using(sport_id) where competition_id='$id' limit 1");

        $theBetslip = $this->session->get("betslip");

        $bts = [];

        foreach ($theBetslip as $slip) {
            if ($slip['bet_type'] == 'prematch') {
                $bts[$slip['pos']] = $slip;
            }
        }

        $title = $theCompetition['0']['competition_name'].", ".$theCompetition['0']['category'];

        $this->tag->setTitle($title);

    	$this->view->setVars([
            'matches'=>$matches,
            'theCompetition'=>$theCompetition,
            'theBetslip'=>$bts,
            'sportId' =>$sport_id,
            'keyword' =>$keyword,
        ]);

    	$this->view->pick("competition/twoway");
    }

}