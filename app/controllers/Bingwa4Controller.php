<?php

class Bingwa4Controller extends ControllerBase
{

    public function indexAction()
    {
    	$games=$this->rawQueries("select j.game_order as pos, e.sub_type_id, concat(group_concat(concat(odd_key)), ',other') as correctscore, m.game_id, m.match_id, m.start_time, m.parent_match_id, m.away_team, m.home_team from jackpot_match j inner join `match` m on m.parent_match_id = j.parent_match_id inner join event_odd e on e.parent_match_id = m.parent_match_id where j.jackpot_event_id=34 and e.sub_type_id=2 group by j.parent_match_id order by pos;");

    	$theBetslip = $this->session->get("betslip");

    	$theBetslip[] = '';

    	$betslip = $this->session->get("betslip");

        unset($theBetslip);     

        foreach ($betslip as $slip) {
            if ($slip['bet_type']=='bingwafour') {
                $theBetslip[$slip['match_id']]=$slip;
            }
        }

        $slipCountJ = sizeof($theBetslip);

    	$this->tag->setTitle('Bingwa Four Midweek Jackpot - Betika.com');

        $this->view->setVars(["games"=>$games,'theBetslip'=>$theBetslip,'slipCountJ'=>$slipCountJ]);

        $this->view->pick("bingwafour/index");

    }

}