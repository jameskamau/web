<?php

class VerifyController extends ControllerBase {

    public function indexAction() {
        $this->view->setVars(["topLeagues" => $this->topLeagues()]);
        $title = "Verify account";

        $this->tag->setTitle($title);
    }

    public function checkAction() {
        $mobile = $this->request->get('mobile', 'int');
        $verification_code = $this->request->get('verification_code', 'int');

        if (!$mobile || !$verification_code) {
            $this->flashSession->error($this->flashMessages('All fields are required'));
            $this->response->redirect('verify');

            // Disable the view to avoid rendering
            $this->view->disable();
        }


        $mobile = $this->formatMobileNumber($mobile);


        $phql = "SELECT * from Profile where msisdn='$mobile' limit 1";
        $user = $this->modelsManager->executeQuery($phql);


        $user = $user->toArray();

        if ($user == false) {
            $this->flashSession->error($this->flashMessages('Invalid username/password'));
            $this->response->redirect('verify');
            // Disable the view to avoid rendering
            $this->view->disable();
        }

        $profile_id = $user['0']['profile_id'];

        $phql = "SELECT * from ProfileSettings where profile_id='$profile_id' and verification_code='$verification_code' limit 1";

        $checkUser = $this->modelsManager->executeQuery($phql);

        $checkUser = $checkUser->toArray();

        if ($checkUser == false) {
            $this->flashSession->error($this->flashMessages('Invalid username/verification code'));
            $this->response->redirect('verify');
            // Disable the view to avoid rendering
            $this->view->disable();
        } else {
            $data = $this->rawInsert("update profile_settings set status='1' where profile_id='$profile_id'");

            $dat = ['profile_id' => $profile_id];

            $exp = time() + 3600;

            $token = $this->generateToken($dat, $exp);

            $transaction = "token=$token";

            $bonus = $this->bonus($transaction);

            $this->flashSession->error($this->flashMessages('Login to access your account'));
            $this->response->redirect('login');
            // Disable the view to avoid rendering
            $this->view->disable();
        }
    }

}
