<?php

class LoginController extends ControllerBase
{

    public function indexAction()
    {  	
        $this->tag->setTitle("Login to ScorePesa");
    }

    public function authenticateAction()
    {
        
        //coreUtils::flog('INFO', "in login controller >>>>>> | ", __CLASS__, __FUNCTION__, __LINE__);
    	if ($this->request->isPost()) {

    	$mobile = $this->request->getPost('mobile', 'int');
        
        //coreUtils::flog('INFO', "mobile number >>>>>> | ". json_encode($mobile), __CLASS__, __FUNCTION__, __LINE__);

        $remember = $this->request->getPost('remember', 'int') ?: 0;

    	$password = $this->request->getPost('password');

    	if (!$mobile || !$password ) {
        	$this->flashSession->error($this->flashMessages('All fields are required'));
            return $this->response->redirect('login');
	        $this->view->disable();
        }

        $mobile = $this->formatMobileNumber($mobile);

    	$phql = "SELECT * from Profile where msisdn='$mobile' limit 1";
		$user = $this->modelsManager->executeQuery($phql);

		$user=$user->toArray();

    	if ($user == false) {
    		$this->flashSession->error($this->flashMessages('User does not exist'));
    		$this->response->redirect('login');
		    // Disable the view to avoid rendering
		    $this->view->disable();
    	}

    	$profile_id = $user['0']['profile_id'];

    	$phql = "SELECT * from ProfileSettings where profile_id='$profile_id' limit 1";
		$checkUser = $this->modelsManager->executeQuery($phql);

		$checkUser=$checkUser->toArray();

        if ($checkUser['0']['status'] == '0') {
            $this->flashSession->error($this->flashMessages('Your account is not verified, please enter the code sent to your phone to verify'));
            $this->response->redirect('verify');
            // Disable the view to avoid rendering
            $this->view->disable();
        }else{
        
                if ($checkUser) {
                    $thePassword = $checkUser['0']['password'];
        
                    if (!$this->security->checkHash($password, $thePassword)) {
                        $this->flashSession->error($this->flashMessages('Invalid password'));
                        $this->response->redirect('login');
                        // Disable the view to avoid rendering
                        $this->view->disable();
                    }else{
                        $device=$this->getDevice();
                        $sessionData=['id' => $checkUser['0']['profile_id'],'remember' => $remember,'mobile'=>$mobile,'device'=>$device];
                        $exp=time()+(3600*24*5);
                        $this->registerAuth($sessionData,$exp);
                        $this->response->redirect('');
                    }
                    
                }else{
                    $this->flashSession->error($this->flashMessages('User does not exist....'));
                    $this->response->redirect('login');
                    // Disable the view to avoid rendering
                    $this->view->disable();
                }
        }
    }
}

}

