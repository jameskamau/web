<?php
/**
 * Copyright (c) Reuben Wafula 2018.
 *
 * All rights reserved.
 */

/**
 * Created by Sublime.
 * User: reuben
 * Date: 30/08/2018
 * Time: 20:06
 */
class FlagService implements \Phalcon\Di\InjectionAwareInterface
{
    /**
     * @var string
     */
    private $json = "";

    private $di;

    /**
     * @param $countryCode
     *
     * @return mixed
     */
    public function getFlag($countryCode)
    {
        # FIXME: Add uefa flag
        $codes = json_decode(file_get_contents(__DIR__ . '/../storage/country-codes.json'), true);
        $flag = key_exists($countryCode, $codes) ? $codes[$countryCode] : 'EN';

        return strtolower('/flag/' . $flag . '.svg');
    }

    /**
     * Sets the dependency injector
     *
     * @param \Phalcon\DiInterface $dependencyInjector
     */
    public function setDI($dependencyInjector)
    {
        $this->di = $dependencyInjector;
    }

    /**
     * Returns the internal dependency injector
     *
     * @return \Phalcon\DiInterface
     */
    public function getDI()
    {
        return $this->di;
    }
}
