{{ partial("sections/header") }}



<div class="page-wrapper">
    <!-- Navigation Bar -->
{{ partial("sections/left") }}

    <!-- Main Container -->
    <div class="container-fluid" style="min-height:800px">

        <div class="row">
            <!-- Game listing -->
            <div class ="col-md-8 np">
                <div class="match-list my-bets">
                    <div class="card-header">
                        <h4 class="card-title m-b-0">My Bets</h4>
                    </div>


                    <div class="card-body collapse show">
                     <div class="table-responsive font-14">
                    {{ this.flashSession.output() }}
                    <table class="table color-bordered-table purple-bordered-table">
                        <thead>
                            <tr>
                                <th scope="col">BET ID</th>
                                <th scope="col">Date</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Possible Win</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($myBets as $bet): ?>

                            <tr class='clickable-row' data-href="mybets?id=<?= $bet['bet_id'] ?>"> 
                                <td data-label="Date"><a href="mybets?id=<?= $bet['bet_id'] ?>"><?= $bet['bet_id'] ?></a></td>  
                                <td data-label="Bet">
                                    <a href="mybets?id=<?= $bet['bet_id'] ?>"><?= date('d/m H:i', strtotime($bet['created'])) ?>
                                    </a>
                                </td> 
                                <td data-label="Amount">
                                <a href="mybets?id=<?= $bet['bet_id'] ?>"><?= $bet['bet_amount'] ?></a>
                                </td>
                                <td data-label="Possible Win">
                                <?php if($bet['jackpot_bet_id'] < 1) { ?>
                                <a href="mybets?id=<?= $bet['bet_id'] ?>">
                                    <?= $bet['possible_win'] ?></a>
                                <?php } else { ?>
                                 <a href="mybets?id=<?= $bet['bet_id'] ?>">
                                    1,000,000.00 </a>   
                                <?php } ?>
                                </td>


                                <td data-label="Status"><a href="mybets?id=<?= $bet['bet_id'] ?>">
                                        <?php
                                        if($bet['xstatus']==1){
                                        echo 'Not Due';
                                        }elseif($bet['xstatus']==5){
                                        echo 'Won';
                                        }elseif($bet['xstatus']==3){
                                        echo 'Lost';
                                        }elseif($bet['xstatus']==4){
                                        echo 'Cancelled';
                                        }elseif($bet['xstatus']==9){
                                        echo 'Awaiting Jackpot';
                                        }else{
                                        echo 'Lost';
                                        }
                                        ?></a>
                                </td> 
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                    </div>

                </div>
            </div> <!-- match list -->
            </div > <!-- end col md-8 for middle content -->
            <!-- Start right bar -->
            <div class="col-md-4 np bs">
                {{ partial("sections/right") }}
            </div>
            <!-- end right side bar -->
        </div> <!-- end row -->
    </div>
    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
</div>
