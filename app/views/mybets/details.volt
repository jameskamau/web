{{ partial("sections/header") }}


<div class="page-wrapper">
    
<!-- Navigation Bar -->
{{ partial("sections/left") }}

    <!-- Main Container -->
    <div class="container-fluid" style="min-height:800px">

        <div class="row">
            <!-- Game listing -->
            <div class ="col-md-8 np">
                <div class="match-list">
                    <div class="card-header">
                        <h4 class="card-title m-b-0">My Bets</h4>
                    </div>


                    <div class="card-body collapse show">
                     <div class="table-responsive font-14">


                    <table class="table color-bordered-table purple-bordered-table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Bet</th>
                                <th scope="col">Date</th>
                                <th scope="col">Bet Amount</th>
                                <th scope="col">Possible Win</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-label="ID"><?= $myBet['bet_id'] ?></td>
                                <td data-label="Bet"><?php 
                                    if($myBet['total_matches']>1){
                                    echo "Multi Bet";
                                    }else{
                                    echo "Single Bet";
                                    }
                                    ?></td>
                                <td data-label="Date"><?= date('d/m H:i', strtotime($myBet['created'])) ?></td>
                                <td data-label="Bet Amount"><?= $myBet['bet_amount'] ?></td>
                                <td data-label="Possible Win"><?= $myBet['possible_win'] ?></td>
                                <td data-label="Status">              
                                    <?php
                                    if($myBet['status']==1){
                                    echo 'Pending results';
                                    }elseif($myBet['status']==5){
                                    echo 'Won';
                                    }elseif($myBet['status']==3){
                                    echo 'Lost';
                                    }elseif($myBet['status']==4){
                                    echo 'Cancelled';
                                    }elseif($myBet['status']==9){
                                    echo 'Pending Jackpot';
                                    }else{
                                    echo 'Lost';
                                    }
                                    ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="title"><span>Match Details</span></div>
                    <table class="table color-bordered-table purple-bordered-table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Date</th>
                                <th scope="col" class="web-view">Name</th>
                                <!-- <th scope="col">Odds</th> -->
                                <th scope="col">Type</th>
                                <th scope="col">Pick</th>
                                <th scope="col">Outcome</th>
                                <th scope="col">Results</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($betDetails as $bet): ?>
                            <tr class="mobile-view tr">
                                <td colspan="100%" style="opacity:0.7; color:#fff; padding-top: 10px;"><?= $bet['home_team']." v ".$bet['away_team'] ?></td>
                            </tr>
                            <tr>
                                <td data-label="ID"><?= $bet['game_id'] ?></td>
                                <td data-label="Date"><?= date('d/m H:i', strtotime($bet['start_time'])) ?></td>
                                <td data-label="Name" class="web-view"><?= $bet['home_team']." v ".$bet['away_team'] ?></td>
                                <!-- <td data-label="Odds"><?= $bet['odd_value'] ?></td> -->
                                <td data-label="Type"><?= $bet['bet_type'] ?></td>
                                <td data-label="Pick"><?= $bet['bet_pick'] ?></td>
                                <td data-label="Outcome"><?php 
                                    if(empty($bet['winning_outcome']))
                                    echo "Pending";
                                    else
                                    echo $bet['winning_outcome'];

                                    ?></td>
                                    <td><?= $bet['ft_score'] ?></td>
                                
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

              </div>

                </div>
            </div> <!-- match list -->
            </div > <!-- end col md-8 for middle content -->
            <!-- Start right bar -->
            <div class="col-md-4 np bs">
                {{ partial("sections/right") }}
            </div>
            <!-- end right side bar -->
        </div> <!-- end row -->
    </div>
    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
</div>
