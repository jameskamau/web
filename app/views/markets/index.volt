<?php
function clean($string) {
    $string = str_replace(' ', '-', $string);
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    return preg_replace('/-+/', '-', $string);
}
?> 
{{ partial("sections/header") }}

<div class="page-wrapper">
<!-- Navigation Bar -->
{{ partial("sections/left") }}



    <!-- Main Container -->
    <div class="container-fluid" style="min-height:800px">

        <div class="row">
            <!-- Game listing -->
            <div class ="col-md-8 np">
                <div class="match-list">
                    
                    <?php if (!empty($matchInfo)) { ?>
                        <div class="card-header">
                            
                             <h4 class="card-title m-b-0">
                                <?php echo $matchInfo['competition_name'] . ", " . $matchInfo['category']; ?>
                            </h4>
                        </div>
                        <div class="tab-content-header">
                            <div class="tab-content-header-text detail-page">

                            <?php echo $matchInfo['home_team'] . " vs " . $matchInfo['away_team']; ?> <br/>

                            <small>
                            <?php echo "ID: " . $matchInfo['game_id']; ?>, 
                            <?php echo "START TIME: ". date('m-d-Y g:i A', strtotime($matchInfo['start_time'])) ; ?> 
                            </small>
                            </div>   
                         </div>
                        <?php
                    }
                    ?>

                <?php
                $sub_type_id = '';
                ?>
                <div class="card-body collapse show"> 
                     <div class="table-responsive font-14 top-matches">

                    <?php foreach($subTypes as $bt): ?>
                    <?php
                    $theMatch = @$theBetslip[$bt['match_id']];
                    ?>
                    <?php if($sub_type_id!=$bt['sub_type_id']): ?>
                      <?php $iteration = 0; ?>

                      <?php $market = ucwords($bt['name']); ?>

                      <div class="col-sm-12 top-matches market-header yellow"><?php echo str_replace("Total", "Over/Under", $market);  ?></div>

                    <?php endif; ?>

                    <?php
                    if($bt['name']){
                      $sub_type_id=$bt['sub_type_id'];
                      $special_bet_value=$bt['special_bet_value'];
                    }
                    ?>

                    <?php if($sub_type_id==$bt['sub_type_id']): ?>
                      <?php $iteration += 1; ?>

                      <?php
                      if(($bt['odd_key'] == 'draw' || $bt['odd_key'] == $awayteam) && $iteration == 1) {
                        $temp = $bt;
                        continue;
                      }

                      ?>

                        <button class="pick col-sm-4 odds <?php echo $bt['match_id']; ?> <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$special_bet_value);
                        if($theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                          echo ' picked';
                        }
                        ?>" oddtype="<?= $bt['name'] ?>" parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch' hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>" oddvalue="<?php echo $bt['odd_value']; ?>" custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$special_bet_value); ?>" target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>" value="<?php echo $bt['sub_type_id']; ?>" special-value-value="<?= $special_bet_value ?>" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))" title="<?php echo ucwords($bt['display']); ?>"><span class="label label-inverse blueish odd-name"> <?php echo ucwords($bt['display']); ?></span> <span class="label label-inverse blueish odd-value"> <?php echo $bt['odd_value']; ?></span> </button>
                      <?php if(isset($temp)): ?>

                          <button class="pick col-sm-4 odds <?php echo $temp['match_id']; ?> <?php echo clean($temp['match_id'].$temp['sub_type_id'].$temp['odd_key'].$special_bet_value);
                          if($theMatch['bet_pick']==$temp['odd_key'] && $theMatch['sub_type_id']==$temp['sub_type_id'] && $theMatch['special_bet_value']==$temp['special_bet_value']){
                            echo ' picked';
                          }
                          ?>" oddtype="<?= $temp['name'] ?>" parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch' hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>" oddvalue="<?php echo $temp['odd_value']; ?>" custom="<?php echo clean($temp['match_id'].$temp['sub_type_id'].$temp['odd_key'].$special_bet_value); ?>" target="javascript:;" id="<?php echo $temp['match_id']; ?>" odd-key="<?php echo $temp['odd_key']; ?>" value="<?php echo $temp['sub_type_id']; ?>" special-value-value="<?= $special_bet_value ?>" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))" title="<?php echo ucwords($temp['display']); ?>"><span class="label label-inverse blueish odd-name"> <?php echo ucwords($temp['display']); ?></span> <span class="label label-inverse blueish odd-value"> <?php echo $temp['odd_value']; ?></span> </button>
                        <?php unset($temp) ?>
                      <?php endif; ?>
                    <?php endif; ?>

                  <?php endforeach; ?>
                     </div>
                </div>

          </div> <!-- match list -->
            </div > <!-- end col md-8 for middle content -->
            <!-- Start right bar -->
            <div class="col-md-4 np bs">
                {{ partial("sections/right") }}
            </div>
            <!-- end right side bar -->
        </div> <!-- end row -->
    </div>
    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
</div>
