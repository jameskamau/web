{{ partial("sections/header") }}
<style>
    #customers {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td, #customers th {

        padding:1px 6px 1px 6px;
    }

    #customers tr:nth-child(even){background-color: #f2f2f2;}

    #customers tr:hover {background-color: #ddd;}

    #customers th {
        padding-top: 10px;
        padding-bottom: 10px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }


    table {
        border-collapse: collapse;
        margin: 0;
        padding: 0;
        width: 100%;
        table-layout: fixed;
    }
    table caption {
        font-size: 1.5em;
        margin: .5em 0 .75em;
    }
    table tr {
        padding: .35em;
    }
    table th,
    table td {
        padding: .625em;
        text-align: center;
        font-size: .75em;
        text-align: left;
    }
    table th {
        font-size: .75em;
        letter-spacing: .1em;
        text-transform: uppercase;
    }
    @media screen and (max-width: 600px) {
        table {
            border: 0;
        }
        table caption {
            font-size: 1.3em;
        }
        table thead {
            border: none;
            clip: rect(0 0 0 0);
            height: 1px;
            margin: -1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            width: 1px;
        }
        table tr {
            border-bottom: 3px solid #ddd;
            display: block;
            margin-bottom: .625em;
        }
        table td {
            border-bottom: 1px solid #ddd;
            display: block;
            font-size: .8em;
            text-align: right;
        }
        table td:before {
            /*
            * aria-label has no advantage, it won't be read inside a table
            content: attr(aria-label);
            */
            content: attr(data-label);
            float: left;
            font-weight: bold;
            text-transform: uppercase;
        }
        table td:last-child {
            border-bottom: 0;
        }
    }
</style>
<div id="container">
    <!-- Navigation Bar -->
    {{ partial("sections/top-nav") }}

    <!-- Breadcrumb -->
    <div class="breadcrumb-container" style="height:4px;margin-bottom: 2px">
        <div class="container">
            <nav aria-label="breadcrumb" role="navigation">
                <!--<ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index-2.html">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Games</li>
                </ol>-->
            </nav>
        </div>
    </div>

    <!-- Main Container -->
    <div class="container" style="min-height:800px">
        <div class="row">
            {{ partial("sections/left") }}
            <div class="col-sm-6 col-lg-7 mb-1 p-0">
                <?php
                if (!empty($matchInfo)) {
                    ?>
                    <div class="title">
                        <?php

                        function clean($string) {
                            $string = str_replace(' ', '-', $string);
                            $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
                            return preg_replace('/-+/', '-', $string);
                        }
                        ?> 
                        <span> 
                            <?php echo $matchInfo['home_team'] . " vs " . $matchInfo['away_team']; ?> - <?php echo $matchInfo['competition_name'] . ", " . $matchInfo['category']; ?>
                        </span>
                    </div>
                    <p><?php echo date('jS M Y', strtotime($matchInfo['start_time'])) . " - " . date('g:i a', strtotime($matchInfo['start_time'])); ?>    |
                        <?php echo "Game ID: " . $matchInfo['game_id']; ?></p>
                    <?php
                }
                ?>

                <?php
                $sub_type_id = '';
                ?>
                <div class="row"> 
                <?php foreach ($subTypes as $bt): ?>
                  
                        <?php
                        $theMatch = @$theBetslip[$bt['match_id']];
                        ?>
                        <?php if ($bt['name']): ?>
                            <?php if ($sub_type_id != $bt['sub_type_id']): ?>
                               <p>{{bt['name']}}</p><br/>
                            <?php endif; ?>

                            <?php
                            if ($bt['name']) {
                                $sub_type_id = $bt['sub_type_id'];
                                $special_bet_value = $bt['special_bet_value'];
                            }
                            ?>

                            <?php if (($sub_type_id == $bt['sub_type_id'])): ?>

                                <button class="pick col-sm-3 odds <?php echo $bt['match_id']; ?> <?php
                                echo clean($bt['match_id'] . $bt['sub_type_id'] . $bt['odd_key'] . $special_bet_value);
                                if ($theMatch['bet_pick'] == $bt['odd_key'] && $theMatch['sub_type_id'] == $bt['sub_type_id'] && $theMatch['special_bet_value'] == $bt['special_bet_value']) {
                                    //echo ' picked';
                                }
                                ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;' oddtype="<?= $bt['name'] ?>" parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch' hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>" oddvalue="<?php echo $bt['odd_value']; ?>" custom="<?php echo clean($bt['match_id'] . $bt['sub_type_id'] . $bt['odd_key'] . $special_bet_value); ?>" target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>" value="<?php echo $bt['sub_type_id']; ?>" special-value-value="<?= $special_bet_value ?>" onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
                                    <span style="float:left"> <?php echo $bt['display'] . ' ' . $special_bet_value; ?></span> 
                                    <span style="float:right"> <?php echo $bt['odd_value']; ?></span>

                                </button> 
                            <?php endif; ?>
                        <?php endif; ?>
                    
                <?php endforeach; ?>
                </div>

            </div>
            {{ partial("sections/right") }}
        </div>
    </div>
    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
</div>



