{{ partial("sections/header") }}

<div class="page-wrapper">
<!-- Navigation Bar -->
{{ partial("sections/left") }}


    <!-- Main Container -->
    <div class="container-fluid" style="min-height:800px">

        <div class="row">
            <!-- Game listing -->
            <div class ="col-md-8 np">
                <div class="match-list terms">

                <div class="card-header">
                    <h4 class="card-title m-b-0">Terms and Conditions</h4>
                </div>
                    {{ partial("partials/terms") }}
                </div> <!-- match list -->
            </div> <!- middle section -->
            <!-- Start right bar -->
            <div class="col-md-4 np bs">
                {{ partial("sections/right") }}
            </div>
            <!-- end right side bar -->
        </div> <!-- end row -->
      </div>
    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
</div>

