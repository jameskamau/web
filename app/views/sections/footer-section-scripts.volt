<!-- jQuery first, then Popper.js, then Bootstrap JS, then required plugins -->
{{ javascript_include('js/jquery-3.2.1.min.js') }}    
{{ javascript_include('js/ajax.js') }}
{{ javascript_include('js/popper.min.js') }}
{{ javascript_include('bootstrap/js/bootstrap.min.js') }}
{{ javascript_include('js/bootstrap3-typeahead.min.js') }}

{{ javascript_include('js/toolkit.js') }}
{{ javascript_include('js/jquery.slimscroll.js') }}
{{ javascript_include('js/sidebar-metis-menu.js') }}
{{ javascript_include('js/sticky-kit.min.js') }}

{{ javascript_include('js/sidebar-menu.js') }}
{{ javascript_include('js/shikabetslips.js') }}

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d6ed3ff77aa790be3323f27/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->