<?php

$id = $this->session->get('auth')['id'];

  $phql = "SELECT ProfileBalance.balance,ProfileBalance.bonus_balance from ProfileSettings left join ProfileBalance on ProfileSettings.profile_id=ProfileBalance.profile_id where ProfileSettings.profile_id='$id' limit 1";
  $checkUser = $this->modelsManager->executeQuery($phql);

  $checkUser=$checkUser->toArray();
  
 ?>
<!-- Offcanvas Menu -->
<nav class="offcanvas">
    <div class="offcanvas-content">
        <div id="list-menu" class="list-menu list-group" data-children=".sub-menu1">
            <a href="{{ url('/') }}">Home</a>
            <!-- <a href="{{ url('jackpot') }}">Jackpot</a> -->
            <a href="{{ url('howtoplay') }}">How To Play 
                <!--<span class="badge badge-secondary badge-pill float-right mt-1">4</span>-->
            </a>
            {% if session.get('auth') != null %}
                <a href="{{ url('mybets') }}">Bets</a>
                <a href="{{ url('withdraw') }}">Withdraw</a>
                <a href="{{ url('logout') }}">Logout</a>
            {% else %}
                <a href="{{ url('login') }}">Login</a>
            {% endif %}

        </div>
    </div>
</nav>

<div class="content-overlay"></div>

<!-- Top Header -->
<div class="top-header">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="d-flex justify-content-between">
                    <nav class="nav">
                        <a class="nav-item nav-link d-none d-sm-block" href="{{ url('/') }}">Help</a>
                        <a class="nav-item nav-link d-none d-sm-block" href="{{ url('/') }}">+254 722 998 908</a>
                        <a class="nav-item nav-link d-none d-sm-block" href="{{ url('/') }}">Download App</a>
                    </nav>
                    <ul class="nav">
                        
                        {% if session.get('auth') != null %}

                            <li class="nav-item d-sm-none">
                                <a href="{{url('signup')}}" class="nav-link">
                                    Bal: KSH.
                                    <?php 
                                    echo @$checkUser['0']['balance'];
                                    ?> | Bonus: <?= @$checkUser['0']['bonus_balance']; ?>

                                </a>
                            </li>
                            <li class="nav-item d-sm-none">
                                <a href="{{url('logout')}}" class="nav-link" >Logout</a>
                            </li>
                        {% else %}
                            <li class="nav-item d-sm-none">
                                <a href="{{url('signup')}}" class="nav-link">Signup</a>
                            </li>
                            <li class="nav-item d-sm-none">
                                <a href="{{url('login')}}" class="nav-link" >Login</a>
                            </li>
                        {% endif %}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Middle Header -->

<div class="middle-header" style="margin-top:30px">
    <div class="container">
        <div class="row py-2 py-lg-0">
            <div class="col-2 col-sm-1 d-block d-lg-none">
                <div class="d-flex align-items-center h-100 justify-content-center menu-btn-wrapper">
                    <button class="btn btn-lg border-0 btn-link offcanvas-btn p-0" type="button">
                        <i class="fa fa-bars"></i/>
                    </button>
                </div>
            </div>
            <div class="col-2 col-sm-1 col-lg-3 pr-0">
                <div class="d-flex align-items-center h-100 logo-wrapper">
                    <!--<a href="{{ url('') }}" class="d-lg-none"><img alt="Logo" src="{{url('images/logo-teal-small.png')}}" class="img-fluid"/></a>-->
                    <a href="{{ url('') }}" class="d-none d-lg-flex mb-2 mb-lg-0"><img alt="Logo" src="images/logo.jpg" class="img-fluid"/></a>
                </div>
            </div>
            <div class="col-8 col-sm-6 col-md-7 col-lg-6">
                <div class="d-flex align-items-center h-100">
                    <div class="input-group input-group-search">

                        <input type="text" class="form-control" id="search-input" placeholder="Search here..." aria-label="Search here..." autocomplete="off">
                        <span class="input-group-append">
                            <button class="btn btn-theme btn-search" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-4 col-sm-4 col-md-3 col-lg-3 d-none d-sm-block">
                <div class="d-flex align-items-center h-100 float-right abg-secondary">
                    {% if session.get('auth') != null %}
                        <div class="btn-group btn-group-sm mr-3" role="group" aria-label="Balance">
                            <a class="btn btn-outline-theme" href="{{ url('login') }}" role="button">
                            Bal: KSH.
                            <?php 
                            echo @$checkUser['0']['balance'];
                            ?> | Bonus: <?= @$checkUser['0']['bonus_balance']; ?>
                             

                            </a>
                            <a class="btn btn-outline-theme" href="{{ url('withdraw') }}" role="button"><i class="fa fa-money d-none d-lg-inline-block"></i> Withdraw</a>
                        </div>
                    {% else %}
                        <div class="btn-group btn-group-sm mr-3" role="group" aria-label="Login Sign Up">
                            <a class="btn btn-outline-theme" href="#" role="button"><i class="fa fa-fw fa-hourglass"></i> SLIP  (<span class="slip-counter"><?= $slipCount ?></span>)</a>
                            <a class="btn btn-outline-theme" href="{{ url('login') }}" role="button"><i class="fa fa-sign-in d-none d-lg-inline-block"></i> Login</a>
                            <a class="btn btn-outline-theme" href="{{ url('signup') }}" role="button"><i class="fa fa-user-plus d-none d-lg-inline-block"></i> Sign Up</a>
                        </div>
                    {% endif %}
                    
                </div>
            </div>
        </div>
    </div>
</div>
