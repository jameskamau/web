<aside class="left-sidebar web-view showonmobile">
    <div class="gn sidebar ">

        <div class="web-element scroll-sidebar">
            <!-- Start navigation left -->

            <nav class="sidebar-nav active">
           
                <ul id="sidebarnav" class="in">
                    <li class="active"> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="true"><span class="hide-menu">TOP SOCCER </span></a>
                        <ul aria-expanded="true" class="collapse">

                            <?php foreach($this->topCompetitions as $competition): ?>
                            <li>
                                <a class="" href="{{ url('competition?id=') }}<?php echo $competition['competition_id']?>">
                                    <span class="col-sm-1" style="padding:0;"><img class="side-icon" src="<?php echo $this->categoryLogo->getFlag($competition['country_code']) ?>"/> </span>
                                    <span class="col-sm-9 topl" style=""><?php echo $competition['competition_name']?></span>
                                    <span class="label label-rounded label-success">{{ competition['games']}}</span>
                                </a>
                            </li>
                            <?php endforeach; ?>
                        </ul>

                    </li>

                    <li class="active"> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="true"><span class="hide-menu">OTHER GAMES </span></a>
                     <ul aria-expanded="false" class="collapse">
                    <?php $id=''; $categoryID=''; $competitionID='';?>
                    <?php foreach($sports as $sp): ?>
                    <?php if($id!=$sp['sport_id']): ?>
                    <?php $id=$sp['sport_id']; ?>

                    <li> 
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                    <img class="side-icon" src="/sport/{{sp['sport_name']}}.png"></img>
                    <span class="hide-menu">{{ sp['sport_name'] }}</span></a>

                    <ul aria-expanded="false" class="collapse">

                        <?php foreach($sports as $s): ?>
                        <?php $url=$sportType[$s['sport_id']].'?id='.$s['competition_id']; ?>
                        <?php if($id == $s['sport_id'] && $categoryID != $s['category_id']): ?>

                        
                            <li><a href="{{ url(url) }}" class="has-arrow">
                            <img class="side-icon" src="<?php echo $this->categoryLogo->getFlag($s['country_code']) ?>"/> {{ s['category'] }}  <span class="label label-rounded label-success">{{ s['cat_games']}}</span></a>

                            <ul aria-expanded="false" class="collapse">
                                <?php $categoryID=$s['category_id']; ?>
                                <?php foreach($sports as $st): ?>
                                    <?php $url=$sportType[$s['sport_id']].'?id='.$st['competition_id']; 
                                    $competitionID=$st['competition_id'];
                                    ?>

                                    <?php if($categoryID == $st['category_id']): ?>
                                        <li><a href="{{ url(url) }}">{{ st['competition_name'] }}<span class="label label-rounded label-success">{{ st['games']}}</span></a></li>
                                    <?php endif; ?>
                                    <?php $categoryID=$s['category_id']; ?>
                                <?php endforeach; ?>
                              </ul>

                          </li>
                        <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                    </li>
                <?php endif; ?>
                <?php endforeach; ?>
                </ul>
                </li>
                <li class="nav-devider"></li>
                </ul>

            </nav>
       
        <!-- end navigation -->

    </div>
</div>

</aside>