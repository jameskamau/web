{{ partial("sections/header") }}

<style>
    #customers {
        font-family: 'Open Sans', 'Arial';
        border-collapse: collapse;
        width: 100%;
        font-size: 14px;
    }

    #customers td, #customers th {

        padding:0px 0px 0px 0px;
        font-size: 14px;
        border: 3px solid #B8B8B8;
        background: #fff
    }

    #customers tr:nth-child(even){
        border-bottom: 1px solid #D0D0D0;
        margin-bottom: 10px;
    }

    #customers tr:hover {}

    #customers th {
        padding-top: 10px;
        padding-bottom: 10px;
        text-align: center;
        color: white;
    }


    table {
        border-collapse: collapse;
        margin: 0;
        padding: 0;
        width: 100%;
        table-layout: fixed;
    }
    table caption {
        font-size: 1.5em;
        margin: .5em 0 .75em;
    }
    table tr {
        padding: .35em;
        margin-top: 5px;
    }
    table th,
    table td {
        padding: .625em;
        text-align: center;
        font-size: .75em;
        text-align: left;
    }
    table th {
        font-size: .75em;
        letter-spacing: .1em;
        text-transform: uppercase;
    }
    @media screen and (max-width: 600px) {
        table {
            border: 0;
        }
        table caption {
            font-size: 1.3em;
        }
        table thead {
            visibility: hidden;
            border: none;
            clip: rect(0 0 0 0);
            height: 1px;
            margin: -1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            width: 1px;
        }
        table tr {
            border-bottom: 3px solid #ddd;
            display: block;
            margin-bottom: .625em;
        }
        table td {
            border-bottom: 1px solid #ddd;
            display: block;
            font-size: .8em;
            text-align: right;
            width: 33%;
        }
        table td:before {
            /*
            * aria-label has no advantage, it won't be read inside a table
            content: attr(aria-label);
            */
            content: attr(data-label);
            float: left;
            font-weight: bold;
            text-transform: uppercase;
        }
        table td:last-child {
            border-bottom: 0;
        }
    }
</style>
<div id="container">

    <!-- Navigation Bar -->
    {{ partial("sections/top-nav") }}

    <!-- Breadcrumb -->
    <div class="breadcrumb-container" style="height:4px;margin-bottom: 2px">
        <div class="container">
            <nav aria-label="breadcrumb" role="navigation">
                <!--<ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index-2.html">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Games</li>
                </ol>-->
            </nav>
        </div>
    </div>

    <!-- Main Container -->
    <div class="container" style="min-height:800px">
        <div class="row">
            {{ partial("sections/left") }}
            <div class="col-sm-6 col-lg-6 mb-1 p-0">


                <ul class="nav" style="background:grey;margin: 6px 2px 5px 2px;">
                    <li class="nav-item border-0">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" style="color:#fff;">Today</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" style="color:#fff;">Tomorrow</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane border-0 border-top-0 border-left-0 p-0 show active" id="home" 
                         role="tabpanel" aria-labelledby="home-tab">

                        <div class="mb-4" style='margin:0px'>
                            <?php

                            function clean($string) {
                                $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
                                $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

                                return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
                            }
                            ?>
                            <table id="customers">
                                <tbody>
                                    <?php foreach ($today as $day): ?>

                                        <?php
                                        $theMatch = @$theBetslip[$day['match_id']];
                                        ?>
                                       <tr>
                                           <td colspan="100%" class="theodds">
                                               <span style="float:left;padding:3px;">
                                                   Game ID <?php echo $day['game_id']; ?> - <?php echo $day['competition_name']; ?> | <?php echo $day['category']; ?>
                                               </span>
                                               <span style="float:right;padding:3px;">
                                                   <?php echo date('d/m H:i', strtotime($day['start_time'])); ?>
                                               </span>
                                           </td>
                                        </tr>

                                        <tr>
   
                                            <td colspan="42%" data-label="Home Team" >
                                            <div class="col-sm-12 top-matches" style="padding:0">
                                                <button class="<?php echo $day['match_id']; ?> <?php
                                                echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']);
                                                if ($theMatch['bet_pick'] == $day['home_team'] && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                                                    echo ' picked';
                                                }
                                                ?> "
                                                hometeam="<?php echo $day['home_team']; ?>" 
                                                oddtype="3 Way" 
                                                bettype='prematch' 
                                                awayteam="<?php echo $day['away_team']; ?>" 
                                                oddvalue="<?php echo $day['home_odd']; ?>" 
                                                target="javascript:;" 
                                                odd-key="<?php echo $day['home_team'];?>" 
                                                parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" 
                                                custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']); ?>" 
                                                value="<?php echo $day['sub_type_id']; ?>" 
                                                special-value-value="0" 
                                                onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">

                                                <span class="theteam col-sm-9" style="float:left;">
                                                        <?php echo $day['home_team']; ?>
                                                    </span> 
                                                    <span class="theodds col-sm-3"><?php echo $day['home_odd']; ?></span>
                                                </button>
                                            </div>
                                            </td>
                                            <td colspan="10%" data-label="Draw">
                                             <div class="top-matches" style="padding:0">
                                                <button class="<?php echo $day['match_id']; ?> <?php
                                                echo clean($day['match_id'] . $day['sub_type_id'] . 'draw');
                                                if ($theMatch['bet_pick'] == 'draw' && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                                                    echo ' picked';
                                                }
                                                ?> " 
                                                hometeam="<?php echo $day['home_team']; ?>" 
                                                oddtype="3 Way" bettype='prematch' 
                                                awayteam="<?php echo $day['away_team']; ?>" 
                                                oddvalue="<?php echo $day['neutral_odd']; ?>" 
                                                custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . "draw"); ?>" 
                                                value="<?php echo $day['sub_type_id'];?>" 
                                                odd-key="draw" 
                                                target="javascript:;" 
                                                parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                                                id="<?php echo $day['match_id']; ?>" 
                                                special-value-value="0" onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">

                                                    <span class="theodds">
                                                        <?php echo $day['neutral_odd']; ?>
                                                    </span>

                                                </button>
                                               </div>
                                            </td>
                                            <td colspan="42%" data-label="Away Team" >
                                              <div class="top-matches" style="padding:0">
                                                <button class="<?php echo $day['match_id']; ?> <?php
                                                echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']);
                                                if ($theMatch['bet_pick'] == $day['away_team'] && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                                                    echo ' picked';
                                                }
                                                ?> " 
                                                hometeam="<?php echo $day['home_team']; ?>" 
                                                oddtype="3 Way" bettype='prematch' 
                                                awayteam="<?php echo $day['away_team']; ?>" 
                                                oddvalue="<?php echo $day['away_odd']; ?>" 
                                                value="<?php echo $day['sub_type_id'];?>" 
                                                custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']); ?>" 
                                                odd-key="<?php echo $day['away_team'];?>" 
                                                target="javascript:;" 
                                                parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                                                id="<?php echo $day['match_id']; ?>" 
                                                special-value-value="0" 
                                                onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">

                                                    <span class="theteam col-sm-9" style="float:left;">
                                                        <?php echo $day['away_team']; ?> 
                                                    </span> 
                                                    <span class="theodds col-sm-3"><?php echo $day['away_odd']; ?></span>

                                                </button>
                                               </div>
                                            </td>
                                            <td colspan="6%" data-label="Mkts">
                                                <?php
                                                if ($theMatch && $theMatch['sub_type_id'] != 10) {
                                                    //echo ' picked';
                                                }
                                                ?>
                                                <a class="side" style="padding:4px;" href="<?php echo 'markets?id=' . $day['match_id']; ?>">+<?php echo $day['side_bets']; ?> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                             <td colspan="100%" style="line-height:60px;padding-top: 9px;background:transparent;border: none">
                                        </td>
                                    </tr>

<?php endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div class="tab-pane border-0 border-top-0 border-left-0 p-0" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="mb-4">

                            <table id="customers">
                                <thead>
                                     <tr>
                                        <th scope="col"width="10%">Date</th>
                                        <th scope="col" width="11%">GAME ID</th>
                                        <th scope="col" width="33%"><b style=''>1</b></th>
                                        <th scope="col" width="10%"><b style=''>X</b></th>
                                        <th scope="col" width="33%"><b style=''>2</b></th>
                                        <th scope="col" width="5%">Mkts</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($tomorrow as $day): ?>

                                        <?php
                                        $theMatch = @$theBetslip[$day['match_id']];
                                        ?>
                                        <tr>
                                           <td colspan="100%" style="border-top: 2px solid #1d1d56;">
                                            <?php echo $day['competition_name']; ?> | <?php echo $day['category']; ?>
                                           </td>
                                        </tr>
                                        <tr>
                                            <td data-label="Date"><?php echo date('d/m H:i', strtotime($day['start_time'])); ?></td>
                                            <td data-label="GAME ID"><?php echo $day['game_id']; ?></td>
                                            <td data-label="Match">
                                                <?php echo $day['home_team']; ?> Vs <?php echo $day['away_team']; ?></td>
                                            <td data-label="Home Team" >
                                                <button class="<?php echo $day['match_id']; ?> <?php
                                                echo clean($day['match_id'] . $day['sub_type_id'] . '1');
                                                if ($theMatch['bet_pick'] == '1' && $theMatch['sub_type_id'] == '10') {
                                                    echo ' picked';
                                                }
                                                ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:#000080;border-color:#ffffff;' hometeam="<?php echo $day['home_team']; ?>" oddtype="3 Way" bettype='prematch' awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['home_odd']; ?>" target="javascript:;" odd-key="1" parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '1'); ?>" value="10" special-value-value="0" onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">


                                                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
    <?php echo $day['home_odd']; ?></span>
                                                </button>
                                            </td>
                                            <td data-label="Draw">
                                                <button class="<?php echo $day['match_id']; ?> <?php
                                                echo clean($day['match_id'] . $day['sub_type_id'] . 'X');
                                                if ($theMatch['bet_pick'] == 'X' && $theMatch['sub_type_id'] == '10') {
                                                    echo ' picked';
                                                }
                                                ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:#000080;border-color:#ffffff;' hometeam="<?php echo $day['home_team']; ?>" oddtype="3 Way" bettype='prematch' awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['neutral_odd']; ?>" custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . "X"); ?>" value="10" odd-key="X" target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" special-value-value="0" onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">

                                                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
    <?php echo $day['neutral_odd']; ?>
                                                    </span>

                                                </button>
                                            </td>
                                            <td data-label="Away Team" >
                                                <button class="<?php echo $day['match_id']; ?> <?php
                                                echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                                                if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '10') {
                                                    echo ' picked';
                                                }
                                                ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:#000080;border-color:#ffffff;' 
                                                        hometeam="<?php echo $day['home_team']; ?>" 
                                                        oddtype="3 Way" bettype='prematch' 
                                                        awayteam="<?php echo $day['away_team']; ?>" 
                                                        oddvalue="<?php echo $day['away_odd']; ?>" 
                                                        value="10" 
                                                        custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                                                        odd-key="2" target="javascript:;" 
                                                        parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                                                        id="<?php echo $day['match_id']; ?>" 
                                                        special-value-value="0" onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">

                                                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;"> <?php echo $day['away_odd']; ?> </span>
                                                </button>
                                            </td>
                                            <td data-label="Mkts">
                                                <?php
                                                if ($theMatch && $theMatch['sub_type_id'] != 10) {
                                                    //echo ' picked';
                                                }
                                                ?>
                                                <a class="side" href="<?php echo 'markets?id=' . $day['match_id']; ?>">+<?php echo $day['side_bets']; ?> </a>
                                            </td>
                                        </tr>
<?php endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>


            </div>
            {{ partial("sections/right") }}
        </div>
    </div>
    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
</div>




