<div class="gn sidebar">
    <!-- <div class="qv rc aog alu web-element">
    <header><div class="header-holder"><span class="col-sm-10">Live Betting</span><span class="col-sm-2 header-icon"><i class="fa fa-rss" aria-hidden="true"></i></span></div></header>
    </div> -->
    <div class="qv rc aog alu web-element">
        <div class="logo">
            <a class="e" href="{{url('')}}" title="Kwikbet">
                <img src="img/logo.png" alt="Kwikbet" title="Kwikbet">
            </a>
        </div>
    </div>

    {% cache "sidebar" 6000 %}


    <?php
    $id = '';
    $categoryID = '';
    $competitionID = '';
    ?>
    <ul class="cd-accordion-menu animated d-none d-lg-inline-block" style="list-style-type: none; padding:1px; background-color: #17a2b8;margin-top: 0px">
        <?php foreach ($sports as $sp): ?>
            <?php if ($id != $sp['sport_id']): ?>

                <?php
                $sport = str_replace(" ", "", $sp['sport_name']);
                $sport = strtolower($sport);
                ?>

                <li class="has-children" style="background: #17a2b8;">
                    <input  type="checkbox" name ="<?php echo $sp['sport_id']; ?>" id="<?php echo $sp['sport_id']; ?>" >
                    <label style="margin-bottom: 0px;padding-left: 2px;" for="<?php echo $sp['sport_id']; ?>"> {{sp['sport_name']}}</label>




                    <ul>
                        <?php $id = $sp['sport_id']; ?>


                        <?php foreach ($sports as $s): ?>
                            <?php $url = @$sportType[$s['sport_id']] . '?id=' . $s['competition_id']; ?>
                            <?php if ($id == $s['sport_id'] && $categoryID != $s['category_id']): ?>
                        
                                <li class="has-children">
                                    
                                    <input type="checkbox" name ="sub-<?php echo $cat['sport_id']; ?>" id="sub-<?php echo $cat['sport_id']; ?>">
                                    <label style="margin-left: -39px;margin-bottom: 0px;padding: 3px 3px 3px 18px;font-size: 12px;" 
                                           for="sub-<?php echo $cat['sport_id']; ?>"  >
                                             <a href="{{url(url)}}">   <?php echo $cat['category']; ?></a>
                                    </label>
                                    
                                    

                                    <ul style="padding-top: 2px;">
                                        <?php $categoryID = $s['category_id']; ?>
                                        <?php foreach ($sports as $st): ?>
                                            <?php
                                            $url = @$sportType[$s['sport_id']] . '?id=' . $st['competition_id'];
                                            $competitionID = $st['competition_id'];
                                            ?>
                                            <?php if ($categoryID == $st['category_id']): ?>
                                                <li style="margin-left: -55px;margin-top:0px;font-size: 8px;">
                                                    
                                                        <a style="padding: 3px 3px 3px 20px;font-size: 12px;" href="{{url(url)}}">
                                                             {{st['competition_name']}}
                                                        </a>
                                                    
                                                </li>
                                            <?php endif; ?>
                                            <?php $categoryID = $s['category_id']; ?>
                                        <?php endforeach; ?>
                                    </ul>

                                </li>
                                
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>

    {% endcache %}

    <ul class="cd-accordion-menu animated d-none d-lg-inline-block" style="list-style-type: none; padding:1px; background-color: #17a2b8;margin-top: 0px">
        <?php
        $data = array();
        foreach ($sports as $spp):
            array_push($data, $spp['sport_id']);
        endforeach;
        $data = array_unique($data);
        //var_dump($data); die('kkk');
        ?>
        <?php
        $rep_sport = array();
        foreach ($sports as $sp):
            if ((in_array($sp['sport_id'], $data)) && (!in_array($sp['sport_id'], $rep_sport))) {
                array_push($rep_sport, $sp['sport_id']);
                ?>
                <li class="has-children" style="background: #17a2b8;">
                    <input  type="checkbox" name ="<?php echo $sp['sport_id']; ?>" id="<?php echo $sp['sport_id']; ?>" >
                    <label style="margin-bottom: 0px;padding-left: 2px;" for="<?php echo $sp['sport_id']; ?>"> {{sp['sport_name']}}</label>
                    <ul>
                        <?php
                        $data_cat = array();
                        foreach ($sports as $cat):
                            array_push($data_cat, $cat['category_id']);
                        endforeach;
                        ?>
                        <?php
                        $rep_cat = array();
                        foreach ($sports as $cat):
                            ?>
                            <?php
                            if (($sp['category_id'] == $cat['category_id']) && (!in_array($cat['category_id'], $rep_cat))) {
                                array_push($rep_cat, $cat['category_id']);
                                ?>
                                <li class="has-children">
                                    <input type="checkbox" name ="sub-<?php echo $cat['sport_id']; ?>" id="sub-<?php echo $cat['sport_id']; ?>">
                                    <label style="margin-left: -39px;margin-bottom: 0px;padding: 3px 3px 3px 18px;font-size: 12px;" 
                                           for="sub-<?php echo $cat['sport_id']; ?>"  >
                                               <?php echo $cat['category']; ?>
                                    </label>
                                    
                                    <ul style="padding-top: 2px;">
                                        <?php foreach ($sports as $cmp): ?>
                                            <?php if ($cmp['category_id'] == $cat['category_id']) { ?>
                                        
                                                <li style="margin-left: -55px;margin-top:0px;font-size: 8px;">
                                                    <a style="padding: 3px 3px 3px 20px;font-size: 12px;" href="#0">
                                                        <?php echo $cmp['competition_name']; ?>
                                                    </a>
                                                </li>
                                                
                                            <?php } ?>
                                        <?php endforeach; ?>
                                    </ul>
                                    
                                    
                                </li>
                            <?php } ?>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <?php
            }
        endforeach;
        ?>
    </ul> 

</div>