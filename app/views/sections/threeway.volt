
{{ partial("sections/header") }}
<div id="container">

    <!-- Navigation Bar -->
    {{ partial("sections/top-nav") }}

    <!-- Breadcrumb 
    <div class="breadcrumb-container" style="height:4px;margin-bottom: 2px">
        <div class="container">
            <nav aria-label="breadcrumb" role="navigation">
            </nav>
        </div>
    </div>
    -->

    <!-- Main Container -->
    <div class="container" style="min-height:800px">
        <div class="row">
            {{ partial("sections/left") }}
            <div class="mb-1 p-2 match-list">

                <h2 class="competition_header">
                    <?php foreach($theCompetition as $compe): ?>
                    <?php echo $compe['competition_name'].", ".$compe['category']; ?>11111
                    <?php endforeach; ?>
                </h2>

                <div class="tab-content">
                    <div class="tab-pane border-0 border-top-0 border-left-0 p-0 show active" id="home" 
                         role="tabpanel" aria-labelledby="home-tab">

                        <div class="mb-4" style='margin:0px'>
                            <?php

                            function clean($string) {
                            $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
                            $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

                            return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
                            }
                            ?>
                            <table id="customers">
                                <tbody>
                                    <?php foreach($matches as $day): ?>
                                    <?php
                                    $theMatch = @$theBetslip[$day['match_id']];
                                    ?>
                                    <tr>
                                        <td colspan="100%" style="background-color:#C1CDCD;">
                                            <span style="float:left;padding:3px;">
                                                Game ID <?php echo $day['game_id']; ?> - <?php echo $day['competition_name']; ?> | <?php echo $day['category']; ?>
                                            </span>
                                            <span style="float:right;padding:3px;">
                                                <?php echo date('d/m H:i', strtotime($day['start_time'])); ?>
                                            </span>
                                        </td>
                                    </tr>

                                    <tr>

                                        <td colspan="42%" data-label="Home Team" >
                                            <div class="col-sm-12 top-matches" style="padding:0">
                                                <button class="<?php echo $day['match_id']; ?> <?php
                                                        echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']);
                                                        if ($theMatch['bet_pick'] == $day['home_team'] && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                                                            echo ' picked';
                                                        }
                                                        ?> "
                                                        hometeam="<?php echo $day['home_team']; ?>" 
                                                        oddtype="3 Way" 
                                                        bettype='prematch' 
                                                        awayteam="<?php echo $day['away_team']; ?>" 
                                                        oddvalue="<?php echo $day['home_odd']; ?>" 
                                                        target="javascript:;" 
                                                        odd-key="<?php echo $day['home_team']; ?>" 
                                                        parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" 
                                                        custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']); ?>" 
                                                        value="<?php echo $day['sub_type_id']; ?>" 
                                                        special-value-value="0" 
                                                        onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">


                                                    <span class="theteam col-sm-9" style="float:left;">
                                                        <?php echo $day['home_team']; ?>
                                                    </span> 
                                                    <span class="theodds col-sm-3"><?php echo $day['home_odd']; ?></span>
                                                </button>
                                            </div>
                                        </td>
                                        <td colspan="10%" data-label="Draw">
                                            <div class="top-matches" style="padding:0">
                                                <button class="<?php echo $day['match_id']; ?> <?php
                                                        echo clean($day['match_id'] . $day['sub_type_id'] . 'draw');
                                                        if ($theMatch['bet_pick'] == 'draw' && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                                                            echo ' picked';
                                                        }
                                                        ?> " 
                                                        hometeam="<?php echo $day['home_team']; ?>" 
                                                        oddtype="3 Way" bettype='prematch' 
                                                        awayteam="<?php echo $day['away_team']; ?>" 
                                                        oddvalue="<?php echo $day['neutral_odd']; ?>" 
                                                        custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . 'draw'); ?>" 
                                                        value="<?php echo $day['sub_type_id']; ?>" 
                                                        odd-key="draw" 
                                                        target="javascript:;" 
                                                        parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                                                        id="<?php echo $day['match_id']; ?>" 
                                                        special-value-value="0" onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">

                                                        <span class="theodds">
                                                        <?php echo $day['neutral_odd']; ?>
                                                    </span>

                                                </button>
                                            </div>
                                        </td>
                                        <td colspan="42%" data-label="Away Team" >
                                            <div class="top-matches" style="padding:0">
                                                <button class="<?php echo $day['match_id']; ?> <?php
                                                        echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']);
                                                        if ($theMatch['bet_pick'] == $day['away_team'] && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                                                            echo ' picked';
                                                        }
                                                        ?> " 
                                                        hometeam="<?php echo $day['home_team']; ?>" 
                                                        oddtype="3 Way" bettype='prematch' 
                                                        awayteam="<?php echo $day['away_team']; ?>" 
                                                        oddvalue="<?php echo $day['away_odd']; ?>" 
                                                        value="<?php echo $day['sub_type_id']; ?>" 
                                                        custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']); ?>" 
                                                        odd-key="<?php echo $day['away_team']; ?>" 
                                                        target="javascript:;" 
                                                        parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                                                        id="<?php echo $day['match_id']; ?>" 
                                                        special-value-value="0" 
                                                        onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">

                                                    <span class="theteam col-sm-9" style="float:left;">
                                                        <?php echo $day['away_team']; ?> 
                                                    </span> 
                                                    <span class="theodds col-sm-3"><?php echo $day['away_odd']; ?></span>

                                                </button>
                                            </div>
                                        </td>
                                        <td colspan="6%" style="background-color:#f58c00;">
                                            <?php
                                            if ($theMatch && $theMatch['sub_type_id'] != 10) {
                                            //echo ' picked';
                                            }
                                            ?>
                                            <a class="side" style="padding:12px;color:#fff;font-weight: bold;" href="<?php echo 'markets?id=' . $day['match_id']; ?>">+<?php echo $day['side_bets']; ?> </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="100%" style="line-height:60px;padding-top: 9px;background:transparent;border: none">
                                        </td>
                                    </tr>

                                    <?php endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div class="tab-pane border-0 border-top-0 border-left-0 p-0 show active" id="profile" 
                         role="tabpanel" aria-labelledby="home-tab">

                        <div class="mb-4" style='margin:0px'>

                            <table id="customers">
                                <tbody>
                                    <?php foreach ($tomorrow as $day): ?>

                                    <?php
                                    $theMatch = @$theBetslip[$day['match_id']];
                                    ?>
                                    <tr>
                                        <td colspan="100%" style="background-color:#C1CDCD;">
                                            <span style="float:left;padding:3px;">
                                                Game ID <?php echo $day['game_id']; ?> - <?php echo $day['competition_name']; ?> | <?php echo $day['category']; ?>
                                            </span>
                                            <span style="float:right;padding:3px;">
                                                <?php echo date('d/m H:i', strtotime($day['start_time'])); ?>
                                            </span>
                                        </td>
                                    </tr>

                                    <tr>

                                        <td colspan="42%" data-label="Home Team" >
                                            <div class="col-sm-12 top-matches" style="padding:0">
                                                <button class="<?php echo $day['match_id']; ?> <?php
                                                        echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']);
                                                        if ($theMatch['bet_pick'] == $day['home_team'] && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                                                            echo ' picked';
                                                        }
                                                        ?> "
                                                        hometeam="<?php echo $day['home_team']; ?>" 
                                                        oddtype="3 Way" 
                                                        bettype='prematch' 
                                                        awayteam="<?php echo $day['away_team']; ?>" 
                                                        oddvalue="<?php echo $day['home_odd']; ?>" 
                                                        target="javascript:;" 
                                                        odd-key="<?php echo $day['home_team']; ?>" 
                                                        parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" 
                                                        custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']); ?>" 
                                                        value="<?php echo $day['sub_type_id']; ?>" 
                                                        special-value-value="0" 
                                                        onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">


                                                    <span class="theteam col-sm-9" style="float:left;">
                                                        <?php echo $day['home_team']; ?>
                                                    </span> 
                                                    <span class="theodds col-sm-3"><?php echo $day['home_odd']; ?></span>
                                                </button>
                                            </div>
                                        </td>
                                        <td colspan="10%" data-label="Draw">
                                            <div class="top-matches" style="padding:0">
                                                <button class="<?php echo $day['match_id']; ?> <?php
                                                        echo clean($day['match_id'] . $day['sub_type_id'] . 'draw');
                                                        if ($theMatch['bet_pick'] == 'draw' && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                                                            echo ' picked';
                                                        }
                                                        ?> " 
                                                        hometeam="<?php echo $day['home_team']; ?>" 
                                                        oddtype="3 Way" bettype='prematch' 
                                                        awayteam="<?php echo $day['away_team']; ?>" 
                                                        oddvalue="<?php echo $day['neutral_odd']; ?>" 
                                                        custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . "draw"); ?>" 
                                                        value="<?php echo $day['sub_type_id']; ?>" 
                                                        odd-key="draw" 
                                                        target="javascript:;" 
                                                        parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                                                        id="<?php echo $day['match_id']; ?>" 
                                                        special-value-value="0" onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">

                                                        <span class="theodds">
                                                        <?php echo $day['neutral_odd']; ?>
                                                    </span>

                                                </button>
                                            </div>
                                        </td>
                                        <td colspan="42%" data-label="Away Team" >
                                            <div class="top-matches" style="padding:0">
                                                <button class="<?php echo $day['match_id']; ?> <?php
                                                        echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']);
                                                        if ($theMatch['bet_pick'] == $day['away_team'] && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                                                            echo ' picked';
                                                        }
                                                        ?> " 
                                                        hometeam="<?php echo $day['home_team']; ?>" 
                                                        oddtype="3 Way" bettype='prematch' 
                                                        awayteam="<?php echo $day['away_team']; ?>" 
                                                        oddvalue="<?php echo $day['away_odd']; ?>" 
                                                        value="<?php echo $day['sub_type_id']; ?>" 
                                                        custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']); ?>" 
                                                        odd-key="<?php echo $day['away_team']; ?>" 
                                                        target="javascript:;" 
                                                        parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                                                        id="<?php echo $day['match_id']; ?>" 
                                                        special-value-value="0" 
                                                        onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">

                                                    <span class="theteam col-sm-9" style="float:left;">
                                                        <?php echo $day['away_team']; ?> 
                                                    </span> 
                                                    <span class="theodds col-sm-3"><?php echo $day['away_odd']; ?></span>

                                                </button>
                                            </div>
                                        </td>
                                        <td colspan="6%" style="background-color:#f58c00;">
                                            <?php
                                            if ($theMatch && $theMatch['sub_type_id'] != 10) {
                                            //echo ' picked';
                                            }
                                            ?>
                                            <a class="side" style="padding:12px;color:#fff;font-weight: bold;" href="<?php echo 'markets?id=' . $day['match_id']; ?>">+<?php echo $day['side_bets']; ?> </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="100%" style="line-height:60px;padding-top: 9px;background:transparent;border: none">
                                        </td>
                                    </tr>

                                    <?php endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>


            </div>
            {{ partial("sections/right") }}
        </div>
    </div>
    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
</div>




