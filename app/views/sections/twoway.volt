
<?php
function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

    return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}
?>
<!-- load upcoming games here -->
                            
<?php foreach ($matches as $day): ?>
<?php $theMatch = @$theBetslip[$day['match_id']]; ?>

<div class="col-md-12 row np hrow">
   <div id="team-details" class="col-md-7 col-sm-12 row team-details np">
      <div id="customers" class="col-md-12 col-sm-12 row np c-header">
       <?php echo $day['category']; ?> - 
       <?php echo $day['competition_name']; ?>
    
      </div>
      <div class="col-md-2 col-sm-4 np mg-column">
        <?php echo date('H:i', strtotime($day['start_time'])); ?> 
      </div>
      <div class="col-md-10 col-sm-8 np mg-column">
        ID <?php echo $day['game_id']; ?>
      </div>
      <div class="col-md-2 col-sm-4 np mg-column">
        <?php echo date('d/m/y', strtotime($day['start_time'])); ?> 
      </div>

      <div class="col-md-10 col-sm-8 np mg-column">
         <?php echo $day['home_team']; ?> VS 
         <?php echo $day['away_team']; ?>
      </div>

      <div class="col-12 np row mobile-mkts mobile-view m1x2h">
        <div class="col-11 np">
            <div class="col-6 np ">1</div>
            <div class="col-6 np">2</div>
        </div>
        <div class="col-1 np">More</div>
        
      </div>
     
   </div>
   
   <div id="markets" class="col-md-4 col-11 row p-0 mmkts">
      <div class="col-md-12 col-12 row np">
        <div class="col-md-6 col-6 np">
        <div class="top-matches">
            <button class="<?php echo $day['match_id']; ?> 
             <?php 
               echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']);
                   if ($theMatch['bet_pick'] == $day['home_team'] 
                       && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                       echo ' picked';
                   }
                ?> "
                hometeam="<?php echo $day['home_team']; ?>" 
                oddtype="3 Way" 
                bettype='prematch' 
                awayteam="<?php echo $day['away_team']; ?>" 
                oddvalue="<?php echo $day['home_odd']; ?>" 
                target="javascript:;" 
                odd-key="<?php echo $day['home_team']; ?>" 
                parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" 
                custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']); ?>" 
                value="<?php echo $day['sub_type_id']; ?>" 
                special-value-value="0" 
                onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))"><?php echo $day['home_odd']; ?>
            </button>
        </div> 

        </div>
        
        <div class="col-md-6 col-6 np">
        <div class="top-matches">
         <button class="<?php echo $day['match_id']; ?> <?php
                echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']);
                if ($theMatch['bet_pick'] == $day['away_team'] && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                    echo ' picked';
                }
                ?> " 
                hometeam="<?php echo $day['home_team']; ?>" 
                oddtype="3 Way" bettype='prematch' 
                awayteam="<?php echo $day['away_team']; ?>" 
                oddvalue="<?php echo $day['away_odd']; ?>" 
                value="<?php echo $day['sub_type_id']; ?>" 
                custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']); ?>" 
                odd-key="<?php echo $day['away_team']; ?>" 
                target="javascript:;" 
                parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                id="<?php echo $day['match_id']; ?>" 
                special-value-value="0" 
                onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">

            <?php echo $day['away_odd']; ?>

        </button>
        </div>
        </div>
      </div>      
   </div>
   <div class="col-md-1 col-1 np more-markets">
    <a class="side"  href="<?php echo 'markets?id=' . $day['match_id']; ?>">+<?php echo $day['side_bets']; ?> </a>
  </div>
</div>
<?php endforeach; ?>
<!-- End of  games -->                           
