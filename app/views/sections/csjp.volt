
<table id="customers">
    <thead>
        <tr>
            <th scope="col">Team : Odds</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($csgames as $day): ?>
        <?php
        $theMatch = $theBetslip[$day['match_id']];
        $odds = $day['ht_ft'];
        $odds = explode(',', $odds);

        $home = $odds['2'];
        $away = $odds['1'];
        $draw = $odds['0'];
        ?>
        <tr>
            <td data-label="Team : Odds">
                <p>
                    Team : 
                    <b>
                        <?php echo $day['pos'] . '. ' . $day['home_team']; ?> vs <?php echo $day['away_team']; ?> : <?php echo date('d/m H:i', strtotime($day['start_time'])); ?> 
                    </b>
                </p>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> 
                    <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '1');
                    if ($theMatch['bet_pick'] == '1' && $theMatch['sub_type_id'] == '1') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?php echo $day['away_team']; ?>" 
                    oddvalue="<?php echo $home; ?>" 
                    target="javascript:;" odd-key="1" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '1'); ?>" 
                    value="1" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        0:0
                        <!--{{home}}-->
                    </span> 
                </button>

                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . 'X');
                    if ($theMatch['bet_pick'] == 'X' && $theMatch['sub_type_id'] == 'X') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?php echo $day['away_team']; ?>" 
                    oddvalue="<?php echo $draw; ?>" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . 'X'); ?>" 
                    value="1" 
                    odd-key="X" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        0:1
                        <!--{{home}}-->
                    </span> 
                </button>

                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        0:2
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        0:3
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        0:4
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        0:5
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        0:6
                        <!--{{home}}-->
                    </span> 
                </button>

                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        1:0
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        1:1
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        1:2
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        1:3
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        1:4
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        1:5
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        2:0
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        2:1
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        2:2
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        2:3
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        2:4
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        3:0
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        3:1
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        3:2
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        3:3
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        4:0
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        4:1
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        4:2
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        5:0
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        5:1
                        <!--{{home}}-->
                    </span> 
                </button>
                <button 
                    class="
                    <?php echo $day['match_id']; ?> <?php
                    echo clean($day['match_id'] . $day['sub_type_id'] . '2');
                    if ($theMatch['bet_pick'] == '2' && $theMatch['sub_type_id'] == '2') {
                        echo ' picked';
                    }
                    ?> btn btn-outline-theme" style='padding:2px 14px 2px 14px; background:greenyellow;border-color:#ffffff;'
                    pos="<?= $day['pos']; ?>" 
                    hometeam="<?php echo $day['home_team']; ?>" 
                    oddtype="3 Way" 
                    bettype='jackpot' 
                    awayteam="<?= $day['away_team']; ?>" 
                    oddvalue="<?php echo $away; ?>" 
                    value="1" 
                    custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . '2'); ?>" 
                    odd-key="2" 
                    target="javascript:;" 
                    parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                    id="<?php echo $day['match_id']; ?>" 
                    special-value-value="0" 
                    onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'), this.getAttribute('pos'))">
                    <span style="font-size:12px; padding: 0;text-align: left;color: #fff;font-weight: bold;">
                        6:0
                        <!--{{home}}-->
                    </span> 
                </button>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<br/>
<p>Total Stake : <b>Ksh 50</b></p>
{% if session.get('auth') != null %}
    <button id="place_bet_button" class="btn btn-theme" onclick="jpBet()">PLACE JACKPOT BET</button>
{% else %}
    <span class="btn btn-theme">
        <a style="color:white" href="{{ url('login') }}">
            PLACE JACKPOT BET
        </a>
    </span>
{% endif %}
<button id="place_bet_button" class="btn btn-theme" onclick="clearSlip()">RESET GAMES</button>



