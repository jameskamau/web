<nav class="navbar navbar-light bg-light p-0  d-lg-flex navbar-theme">
    <div class="container inner">
        <div class="pos-r d-flex w-100 menu_header">
            <ul class="navbar-nav mr-auto main-menu">

                <li class="nav-item"> 
                    <a class="" href="{{ url('') }}">
                    <span class="web-view">Today's Special</span>
                    <span class="mobile-view">Highlights </span>
                    </a>
                </li>
                 <li class="nav-item web-view">
                     <a class="" href="{{ url('games') }}">
                     All Games </a>
                 </li>
                 <li class="nav-item web-view" >
                    <a href="{{url('live')}}"> Live Games</a>
                </li>
                
                <li class="nav-item">
                    <a class="" href="{{url('jackpot')}}">Jackpot
                    </a>
                </li>
                
                <li class="nav-item web-view" >
                    <a href="{{url('howtoplay')}}"> How to Play</a>
                </li>
                <!--
                <li class="nav-item">
                    <a href="#"><span class="web-view">Download </span>App</a>
                </li>
                -->
                {% if session.get('auth') == null %}
                    <li class="nav-item" >
                        <a href="{{url('deposit')}}"> Deposit</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('login')}}">Login</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('signup')}}">Register</a>
                    </li>

                {%endif%}

            </ul>
           
        </div>
    </div>
</nav>