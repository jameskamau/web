<div class="footer footer-cs">
    <div class="container">
        
        <div class="row">
            <div class="col-md-3"style="padding-right:15px; padding-left:15px;"></div>
            <div class="col-md-3"style="padding-right:15px;">
                <span style="font-weight:500; font-size:16px">SUPPORT</span>
                <div class="spacer"></div>
                <span class="foot-links"><a href="/howtoplay">How to Play</a></span><br>
                <span class="foot-links"><a href="/terms">Terms and Conditions</a></span><br>
                <span class="foot-links"><a href="/terms">Responsible Gaming</a></span><br>
                <span class="foot-links"><a href="/terms">Privacy Policy</a></span>
            </div>
            <div class="col-md-3" style="padding-right:15px;">
                <span style="font-weight:500; font-size:16px">LEGAL AND COMPLIANCE</span>
                <div class="spacer"></div>
                <span class="foot-links">This platform is open only to persons over the age of 18 years.</span><br><br>
                <span class="foot-links">Gambling may have adverse effects if not taken in moderation. Bet Resposibly!</span>
                
            </div>
            <div class="col-md-3" style="padding-right:15px;">
                <span style="font-weight:500; font-size:16px">LICENSE</span>
                <div class="spacer"></div>
                <span class="foot-links">BETWIN LIMITED Trading as ScorePesa is licensed by the Betting Control & Licensing Board under License Number 0000102</span>
                
            </div>
        </div>
    </div>
    <div class="text-center copyright">
        <a href="{{ url('terms') }}" style="color:white"> Terms</a> &nbsp; | &nbsp; Copyright &copy; 2019 All right reserved
    </div>
</div>