<?php

$id = $this->session->get('auth')['id'];

$phql = "SELECT ProfileBalance.balance,ProfileBalance.bonus_balance from ProfileSettings left join ProfileBalance on ProfileSettings.profile_id=ProfileBalance.profile_id where ProfileSettings.profile_id='$id' limit 1";

$checkUser = $this->modelsManager->executeQuery($phql);

$checkUser=$checkUser->toArray();

?>
<!-- Offcanvas Menu -->
<nav class="offcanvas menu_header">
    <div class="offcanvas-content">
        <div id="list-menu" class="list-menu list-group" data-children=".sub-menu1">
            <a href="{{ url('/') }}">Home</a>
            <!-- <a href="{{ url('jackpot') }}">Jackpot</a> -->
            <a href="{{ url('howtoplay') }}">How To Play 
                <!--<span class="badge badge-secondary badge-pill float-right mt-1">4</span>-->
            </a>
            {% if session.get('auth') != null %}
                <a href="{{ url('mybets') }}">Bets</a>
                <a href="{{ url('withdraw') }}">Withdraw</a>
                <a href="{{ url('logout') }}">Logout</a>
            {% else %}
                <a href="{{ url('login') }}">Login</a>
            {% endif %}

        </div>
    </div>
</nav>

<div class="content-overlay"></div>

<!-- Middle Header -->
<div class="topbar">
    <div class="top_header_logo">
        <div class="container site-header">
            
            <div class="navbar top-navbar navbar-light">
                <div class="col-1 col-sm-1 d-block d-lg-none np">
                    <a class="d-flex align-items-center h-100 justify-content-center menu-btn-wrapper nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark">
                        <button class="btn btn-lg border-0 btn-link offcanvas-btn p-0" type="button">
                            <i class="fa fa-bars"></i>
                        </button>
                    </a>
                </div>

                <div class="col-2 navbar-header">
                    <b>
                        <a class="navbar-brand" href="{{ url('') }}">
                            <!--End Logo icon -->
                            <!-- Logo text --><span>
                                <!-- Light Logo text -->    
                                <img src="images/logo.jpg" class="light-logo" alt="homepage"></span> </a>
                    </b>
                </div>
                
                <div class="col-5 mobile-view betslip-counter">
                    <p>
                        
                        <a href="{{ url('betmobile') }}">
                            Betslip(<span class="slip-counter slip-games-header" 
                            style="font-size: 12px;"><?= $slipCount ?></span>)
                        </a>
                        
                    </p>
                </div>

                <div class="col-md-7 col-xs-12">
                    <img src="images/banners/paybill-strip.png" width="100%" />
                </div>
                
                <!-- start login section -->

                <div class="login col-md-3 float-right np ">

                    {% if session.get('auth') != null %}
                        <div class="mr-3 pull-right bal-box" >
                            <div class="col-md-12 col-12 bal np">
                            <i class="fa fa-user" aria-hidden="true"></i> {{session.get('auth')['mobile']}} <br>
                            Balance: KSH. <?= $checkUser['0']['balance'] ?: 0;
                                ?> &nbsp; Bonus: <?= $checkUser['0']['bonus_balance'] ?: 0; ?>
                            </div>
                            <div class="col-md-12 col-12 quick-links np">
                                 <a class="" href="{{ url('mybets') }}" role="button">My Bets</a>
                                <a class="" href="{{ url('deposit') }}" role="button">Deposit</a>
                                <a class="" href="{{ url('withdraw') }}" role="button">Withdraw</a>
                                <a class="" href="{{ url('logout') }}" role="button">Logout</a>
                            </div>
                        </div>
                    {% else %}

                        <?php echo $this->tag->form(["login/authenticate","class"=>"col-md-12 row np float-right top-login"]); ?>
                        <div class="et leftpad mobile-f">
                            <input type="text" name="mobile" class="form-control msisdn" data-action="grow" placeholder="Mobile Number">
                            <span class="sticky-hidden below r-me">
                                <input type="checkbox" name="remember" 
                                value="1"> Remember me</input>
                            </span>
                        </div>
                        <div class="et leftpad pl-1 password-f">
                            <input type="password" name="password" class="form-control password" data-action="grow" placeholder="Password">
                            <a href="{{ url('reset') }}" title="ScorePesa reset password"><span class="sticky-hidden below r-me">Forgotten password?</span></a>
                        </div>
                        <div class="et pl-1 pr-0 login-btn-container" >

                            <button class="cg fp login-btn">Log In</button>

                            <a class="cg fm leftpad reg" href="{{ url('signup') }}" title="Join">Register</a>
                        </div>
                        </form>
                    {% endif %}
                </div>

            </div>
        </div>

    </div>
    {{ partial("sections/top-nav") }}

</div>



