    <?php
    function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
    ?>

    {{ partial("sections/header") }}

    <div class="page-wrapper">

        <div class="row topjpimg">
                <div class="col-md-12 imgjp">
                        <div class="ball">

                            <nav class="navbar navbar-light bg-green p-0  d-lg-flex">
                                <div class="container inner">
                                    <div class="pos-r d-flex w-100">
                                        <!-- <ul class="navbar-nav mr-auto main-menu">

                                            <li class="nav-item active">
                                                <a class="nav-link" href="#"><span class="">
                                                    </span>Football</a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" href="#"><span class=""></span>Basketball</a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" href="#"><span class=""></span>Tennis</a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" href="#"><span class=""></span>Rugby Union</a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" href="#"><span class=""></span>Boxing</a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" href="#"><span class=""></span>Cricket</a>
                                              </li>
                                        </ul> -->
                                       
                                    </div>
                                </div>
                            </nav>

                        </div>
                </div>
            </div>

        <!-- Navigation Bar -->
        
        {{ partial("sections/left") }}
          
        <!-- Main Container -->
        <div class="container-fluid" style="min-height:800px">
            
            <div class="row">
                <!-- Game listing -->
            <div class ="col-md-8  np">

            <!-- start crousel  -->
            <div class="home-scroll">
                <div class="col-lg-12 np">
                    <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators2" data-slide-to="1" class=""></li>
                            <li data-target="#carouselExampleIndicators2" data-slide-to="2" class=""></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <img class="img-responsive" src="images/banners/bonus-banner.png" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="img-responsive" src="images/banners/paybill-banner.png" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="img-responsive" src="images/banners/register-banner.png" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div> 
            </div> <!-- row -->
            <!-- end crousel --> 

                    <div class="match-list">

                        <div class="card-header row">
                            <div class="col-md-6">
                                <h4 class="card-title m-b-0">
                                    <span class="sport-on-match-list"> 
                                        <img class="side-icon" src="/sport/Soccer.png">  
                                        SOCCER
                                    </span> - 
                                    ScorePesa Today's Special
                                </h4>
                            </div>
                           <div class="ss col-sm-6 nopadding web-element">
                             <form name="search" class="row" method="post" action="/?sp={{sportId}}">
                                <div class="col-sm-9 nopadding submit-input">
                                    <input type="text" name="keyword" class="form-control" data-action="grow"
                                                             placeholder="Your search criteria" value="{{keyword}}">
                                </div>
                                <div class="col-sm-3 nopadding search-button">
                                    <button type="submit" class="search-btn">Search</button>
                                </div>
                                </form>
                            </div>
                        </div>
                        <?php 
                            $initial = new DateTime(); 
                            $interval = new DateInterval('P1D');
                        ?>
                        <!-- tabs menu -->
                        <ul class="nav nav-tabs md-tabs " id="highlight-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#panel0" role="tab" 
                                data-url="{{ url('games') }}?t=0" >Top</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#panel1" role="tab" 
                                data-url="{{ url('games') }}?t=1">
                                <?php
                                    echo 'TODAY';
                                ?>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#panel2" role="tab" 
                                data-url="{{ url('games') }}?t=2">
                                <?php
                                    $initial->add($interval);
                                    echo $initial->format('l jS');
                                ?>
                                </a>
                            </li>
                            <li class="nav-item web-view">
                                <a class="nav-link" data-toggle="tab" href="#panel3" role="tab" data-url="{{ url('games') }}?t=3">
                                <?php
                                    $initial->add($interval);
                                     echo $initial->format('l jS');
                                ?>
                                </a>
                            </li>
                            <li class="nav-item web-view">
                                <a class="nav-link" data-toggle="tab" href="#panel4" role="tab" data-url="{{ url('games') }}?t=4">
                                <?php
                                    $initial->add($interval);
                                    echo $initial->format('l jS');
                                ?>
                                </a>
                            </li>
                        </ul>
                        <!-- start tab content header -->
                        <div class="tab-content-header">
                            <div class="tab-content-header-text col-md-12 row p-0">
                                <div class="col-md-6 p-0 ">
                                    International Highlights
                                </div>
                                <div class="col-md-3 p-0 m1x2h web-view">
                                    1x2
                                </div>
                                <div class="col-md-3 p-0 movu web-view">
                                    Over/Under 2.5
                                </div>
                                <!-- 
                                <div class="col-md-3 p-0 mdc">
                                    Double Chance
                                </div>
                            -->
                            </div>
                        </div>
                        <div class=" col-md-12 row p-0 markets-desc web-view">
                            <div class="col-md-4 p-0">
                                &nbsp;
                            </div>
                            <div class="col-md-3 row np oddk m1x2h ">
                                <div class="col-md-4 np">1</div>
                                <div class="col-md-4 np">X</div>
                                <div class="col-md-4 np">2</div>
                            </div>
                            <div class="col-md-2 row np movu">
                                <div class="col-md-4 np">1X</div>
                                <div class="col-md-4 np">X2</div>
                                <div class="col-md-4 np">12</div>
                            </div>
                            <div class="col-md-2 row np movu">
                                <div class="col-md-6 np">Under 2.5</div>
                                <div class="col-md-6 np">Over 2.5</div>
                            </div>
                            <div class="col-md-1 row np movu">
                                More
                            </div>
                            <!--  No double chance
                            <div class="col-md-3 row p-0 mdc">
                                <div class="col-md-4">1X</div>
                                <div class="col-md-4">X2</div>
                                <div class="col-md-4">12</div>
                            </div>
                            -->
                        </div>

                        <!-- End ta content header -->
                        <!-- Tab panels -->
                        <div class="tab-content">
                            <!--Panel 1-->
                            <div class="tab-pane fade in show active" id="panel0" role="tabpanel">
                                <!-- load upcoming games here -->
                                
                                <?php foreach ($today as $day): ?>
                                <?php
                                    $theMatch = @$theBetslip[$day['match_id']];
                                ?>
                                <div class="col-md-12 row np hrow">
                                <div id="team-details" class="col-md-4 col-sm-12 row team-details np">
                                  <div id="customers" class="col-md-12 col-sm-12 row np c-header">
                                   <?php echo $day['category']; ?> - 
                                   <?php echo $day['competition_name']; ?>
                                
                                  </div>
                                  <div class="col-md-2 col-sm-4 np mg-column">
                                    <?php echo date('H:i', strtotime($day['start_time'])); ?> 
                                  </div>
                                  <div class="col-md-10 col-sm-8 np mg-column">
                                    ID <?php echo $day['game_id']; ?>
                                  </div>
                                  <div class="col-md-2 col-sm-4 np mg-column">
                                    <?php echo date('d/m/y', strtotime($day['start_time'])); ?> 
                                  </div>

                                  <div class="col-md-10 col-sm-8  np mg-column">
                                     <?php echo $day['home_team']; ?> VS 
                                     <?php echo $day['away_team']; ?>
                                  </div>
                                    
                                  <div class="col-12 np row hidden-xs m1x2h">
                                    <div class="col-11 np">
                                        <div class="col-1 np ">1</div>
                                        <div class="col-1 np">X</div>
                                        <div class="col-1 np">2</div>
                                        <div class="col-1 np">1X</div>
                                        <div class="col-1 np">X2</div>
                                        <div class="col-2 np">12</div>
                                        <div class="col-2 np">OV 2.5</div>
                                        <div class="col-2 np">UN 2.5</div>
                                    </div>
                                    <div class="col-1 np">More</div>
                                    
                                  </div>
                                 
                               </div>
                               
                               <div id="markets" class="col-md-8 col-11 row p-0 mmkts">
                                  <div class="col-md-4 col-xs-12 row np">
                                    <div class="col-md-4 col-xs-4 np">
                                    <div class="top-matches">
                                        <button class="<?php echo $day['match_id']; ?> 
                                         <?php 
                                           echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']);
                                               if ($theMatch['bet_pick'] == $day['home_team'] 
                                                   && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                                                   echo ' picked';
                                               }
                                            ?> "
                                            hometeam="<?php echo $day['home_team']; ?>" 
                                            oddtype="1x2" 
                                            bettype='prematch' 
                                            awayteam="<?php echo $day['away_team']; ?>" 
                                            oddvalue="<?php echo $day['home_odd']; ?>" 
                                            target="javascript:;" 
                                            odd-key="<?php echo $day['home_team']; ?>" 
                                            parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" 
                                            custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']); ?>" 
                                            value="<?php echo $day['sub_type_id']; ?>" 
                                            special-value-value="0" 
                                            onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
                                            <span class="odds-btn-label mobile-view">HOME<br/></span>
                                            <?php echo $day['home_odd']; ?>
                                        </button>
                                    </div> 

                                    </div>
                                    <div class="col-md-4 col-xs-4 np">
                                    
                                    <div class="top-matches">
                                        <button class="<?php echo $day['match_id']; ?> <?php
                                            echo clean($day['match_id'] . $day['sub_type_id'] . 'draw');
                                            if ($theMatch['bet_pick'] == 'draw' && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                                                echo ' picked';
                                            }
                                            ?> " 
                                            hometeam="<?php echo $day['home_team']; ?>" 
                                            oddtype="1x2" bettype='prematch' 
                                            awayteam="<?php echo $day['away_team']; ?>" 
                                            oddvalue="<?php echo $day['neutral_odd']; ?>" 
                                            custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . "draw"); ?>" 
                                            value="<?php echo $day['sub_type_id']; ?>" 
                                            odd-key="draw" 
                                            target="javascript:;" 
                                            parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                                            id="<?php echo $day['match_id']; ?>" 
                                            special-value-value="0" onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
                                            <span class="odds-btn-label mobile-view">DRAW<br/></span>
                                            <?php echo $day['neutral_odd']; ?>
                                
                                       </button>
                                    </div>
                                    </div>
                                    <div class="col-md-4  col-xs-4 np">
                                    <div class="top-matches">
                                     <button class="<?php echo $day['match_id']; ?> <?php
                                            echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']);
                                            if ($theMatch['bet_pick'] == $day['away_team'] && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                                                echo ' picked';
                                            }
                                            ?> " 
                                            hometeam="<?php echo $day['home_team']; ?>" 
                                            oddtype="1x2" bettype='prematch' 
                                            awayteam="<?php echo $day['away_team']; ?>" 
                                            oddvalue="<?php echo $day['away_odd']; ?>" 
                                            value="<?php echo $day['sub_type_id']; ?>" 
                                            custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']); ?>" 
                                            odd-key="<?php echo $day['away_team']; ?>" 
                                            target="javascript:;" 
                                            parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                                            id="<?php echo $day['match_id']; ?>" 
                                            special-value-value="0" 
                                            onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
                                            <span class="odds-btn-label mobile-view">AWAY<br/></span>
                                        <?php echo $day['away_odd']; ?>

                                    </button>
                                    </div>
                                    </div>
                                  </div>

                                  <!-- Double chance market remove -->
                                  <div class="col-md-4 col-xs-12 row np">
                                    <div class="col-md-4 col-xs-4 np">
                                    
                                    <div class="top-matches">
                                    <button class="<?php echo $day['match_id']; ?> <?php
                                            echo clean($day['match_id'] . '10' . $day['home_team'] . ' or draw');
                                            if ($theMatch['bet_pick'] == $day['home_team'] . ' or draw' && $theMatch['sub_type_id'] == 10) {
                                                echo ' picked';
                                            }
                                            ?> " 
                                            hometeam="<?php echo $day['home_team']; ?>" 
                                            oddtype="Double Chance" bettype='prematch' 
                                            awayteam="<?php echo $day['away_team']; ?>" 
                                            oddvalue="<?php echo $day['double_chance_1x_odd']; ?>" 
                                            value="10" 
                                            custom="<?php echo clean($day['match_id'] . '10' . $day['home_team'] . ' or draw'); ?>" 
                                            odd-key="<?= $day['home_team'] . ' or draw';?>" 
                                            target="javascript:;" 
                                            parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                                            id="<?php echo $day['match_id']; ?>" 
                                            special-value-value="0" 
                                            onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
                                            <span class="odds-btn-label mobile-view">1 OR X<br/></span>
                                        <?php echo $day['double_chance_1x_odd']; ?>

                                    </button>
                                    </div>
                                    </div>
                                    <div class="col-md-4 col-xs-4 np">
                                    
                                    <div class="top-matches">
                                    <button class="<?php echo $day['match_id']; ?> <?php
                                            echo clean($day['match_id'] . '10' . 'draw or '.$day['away_team']);
                                            if ($theMatch['bet_pick'] == 'draw or '.$day['away_team'] && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                                                echo ' picked';
                                            }
                                            ?> " 
                                            hometeam="<?php echo $day['home_team']; ?>" 
                                            oddtype="Double Chance" bettype='prematch' 
                                            awayteam="<?php echo $day['away_team']; ?>" 
                                            oddvalue="<?php echo $day['double_chance_x2_odd']; ?>" 
                                            value="10" 
                                            custom="<?php echo clean($day['match_id'] . '10' . 'draw or '.$day['away_team']); ?>" 
                                            odd-key="<?= 'draw or '.$day['away_team']; ?>" 
                                            target="javascript:;" 
                                            parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                                            id="<?php echo $day['match_id']; ?>" 
                                            special-value-value="0" 
                                            onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
                                            <span class="odds-btn-label mobile-view">X OR 2<br/></span>
                                        <?php echo $day['double_chance_x2_odd']; ?>

                                    </button>
                                    </div>
                                    </div>
                                    
                                    <div class="col-md-4 col-xs-4 np">
                                    
                                    <div class="top-matches">
                                    <button class="<?php echo $day['match_id']; ?> <?php
                                            echo clean($day['match_id'] . '10' . $day['home_team']. ' or '.$day['away_team']);
                                            if ($theMatch['bet_pick'] == $day['home_team']. ' or '.$day['away_team'] && $theMatch['sub_type_id'] == 10) {
                                                echo ' picked';
                                            }
                                            ?> " 
                                            hometeam="<?php echo $day['home_team']; ?>" 
                                            oddtype="Double Chance" bettype='prematch' 
                                            awayteam="<?php echo $day['away_team']; ?>" 
                                            oddvalue="<?php echo $day['double_chance_12_odd']; ?>" 
                                            value="10" 
                                            custom="<?php echo clean($day['match_id'] . '10' . $day['home_team']. ' or '.$day['away_team']); ?>" 
                                            odd-key="<?= $day['home_team']. ' or '.$day['away_team']; ?>" 
                                            target="javascript:;" 
                                            parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                                            id="<?php echo $day['match_id']; ?>" 
                                            special-value-value="0" 
                                            onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
                                            <span class="odds-btn-label mobile-view">1 OR 2<br/></span>
                                            <?php echo $day['double_chance_12_odd']; ?>

                                    </button>
                                    </div>

                                    </div> <!-- md4 double chance -->

                                    </div><!-- Double chance market -->

                                    <div class="col-md-3 col-xs-12 row np">
                                        <div class="col-md-6 col-xs-6 np">
                                        <div class="top-matches">
                                        <button class="<?php echo $day['match_id']; ?> <?php
                                                echo clean($day['match_id'] . '18' . 'under 2.5');

                                                if ($theMatch['bet_pick'] == 'under 2.5' && $theMatch['sub_type_id'] == '18') {
                                                    echo ' picked';
                                                }
                                                ?> " 
                                                hometeam="<?php echo $day['home_team']; ?>" 
                                                oddtype="Total" bettype='prematch' 
                                                awayteam="<?php echo $day['away_team']; ?>" 
                                                oddvalue="<?php echo $day['under_25_odd']; ?>" 
                                                value="18" 
                                                custom="<?php echo clean($day['match_id'] . 18 . 'under 2.5'); ?>" 
                                                odd-key="under 2.5" 
                                                target="javascript:;" 
                                                parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                                                id="<?php echo $day['match_id']; ?>" 
                                                special-value-value="2.5" 
                                                onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
                                                <span class="odds-btn-label mobile-view">UNDER 2.5<br/></span>
                                            <?php echo $day['under_25_odd']; ?>

                                        </button>
                                        </div>
                                        </div>
                                        <div class="col-md-6 col-xs-6 np">
                                        <div class="top-matches">
                                        <button class="<?php echo $day['match_id']; ?> <?php
                                                echo clean($day['match_id'] . '18' . 'over 2.5');
                                                if ($theMatch['bet_pick'] == 'over 2.5' && $theMatch['sub_type_id'] == 18) {
                                                    echo ' picked';
                                                }
                                                ?> " 
                                                hometeam="<?php echo $day['home_team']; ?>" 
                                                oddtype="Total" bettype='prematch' 
                                                awayteam="<?php echo $day['away_team']; ?>" 
                                                oddvalue="<?php echo $day['over_25_odd']; ?>" 
                                                value="18" 
                                                custom="<?php echo clean($day['match_id'] . '18' . 'over 2.5'); ?>" 
                                                odd-key="over 2.5" 
                                                target="javascript:;" 
                                                parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                                                id="<?php echo $day['match_id']; ?>" 
                                                special-value-value="2.5" 
                                                onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
                                                <span class="odds-btn-label mobile-view">OVER 2.5<br/></span>
                                            <?php echo $day['over_25_odd']; ?>

                                        </button>
                                        </div>
                                      </div>
                                      </div>
                                  
                                    <div class="col-md-1 col-1 np more-markets">
                                        <a class="side"  href="<?php echo 'markets?id=' . $day['match_id']; ?>">+<?php echo $day['side_bets']; ?> </a>
                                    </div>
                                 </div>
                             </div>
                            <?php endforeach; ?>
                            <!-- End of  games -->  
                            </div>
                            <!--/.Panel 1 -->
                            <!--Panel 2-->
                            <div class="tab-pane fade" id="panel1" role="tabpanel">Loading ...
                                
                            </div>
                            <!--/.Panel 2-->
                            <!--Panel 3-->
                            <div class="tab-pane fade" id="panel2" role="tabpanel">Loading ...
                            </div>
                            <!--/.Panel 3-->

                            <!--Panel 4-->
                            <div class="tab-pane fade" id="panel3" role="tabpanel">Loading ...
                            </div>
                            <!--/.Panel 4-->
                            <!--Panel 5-->
                            <div class="tab-pane fade" id="panel4" role="tabpanel">Loading ...
                            </div>
                            <!--/.Panel 5-->
                        </div>

                        <!-- end tabs -->

                    </div> <!-- end match-list -->

                </div > <!-- end col md-8 for middle content -->


                <!-- Start right bar -->
                <div class="col-md-4 np bs">
                    {{ partial("sections/right") }}
                </div>
                <!-- end right side bar -->
            </div> <!-- end row -->
        </div>

        <!-- Footer -->
        {{ partial("sections/footer") }}
        <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
                return false">
            <i class="fa fa-angle-double-up"></i>
        </a>
    </div>
