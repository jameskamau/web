<!-- Bootstrap CSS -->
{{ stylesheet_link('bootstrap/css/bootstrap.min.css') }}
{{ stylesheet_link('bootstrap/css/applications.css') }}

<!-- Third Party CSS  -->
{{ stylesheet_link('css/animate.min.css') }}
{{ stylesheet_link('css/owl.carousel.min.css') }}
{{ stylesheet_link('css/owl.theme.default.min.css') }}
<!-- Mimity CSS -->
{{ stylesheet_link('css/style.min.css') }}
{{ stylesheet_link('sidebar/css/reset.css') }}
{{ stylesheet_link('sidebar/css/style.css') }}

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

<!-- main site css --->
{{ stylesheet_link('css/toolkits.css') }} 
{{ stylesheet_link('css/sidebar-menu.css') }}  


