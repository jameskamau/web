<?php 
//$slipCount = $this->session->get('betslip');
//$slipCount = sizeof($slipCount);
?>

<div class="card card-outline-info web-view card-no-border bs-1">

    <div class="card-header">
        <h4 class="m-b-0 text-white">BET Slips (<span class="slip-counter slip-games-header"><?= $slipCount ?></span>)</h4>
    </div>

    <div class="alert alert-success ss betslip-success" role="alert" style="display:none"></div>
    <div id="betslip" class="betslip ">
        
    </div>
    <div class="col-md-12 np">
     <!-- loader -->
     <div class="loader col-md-12 p1" style="display:none">
      <img src="/images/loader.gif" class="loader">
     </div>
    <!-- end loader -->
    <!-- error message holder -->
    <div class="alert alert-danger ss betslip-error" role="alert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">X</span></button>
        You have insufficient balance please top up
    </div>
    <!-- end holder -->
    <div class="col-md-11 btsps pull-right">
    <button class="btn btn-theme  place-bet-btn remove" type="button" onclick="clearSlip()">Clear Slip</button>
    {% if session.get('auth') != null %}
        <!-- <div class="border rounded p-2 bg-light"> -->
        <?php if($jackpotSlip == 1){ ?>

        <button id="place_bet_button" type="button"  class="btn btn-theme  place_bet_btn " onclick="jackpotBet()">Place Bet</button>
    </div>
        <?php } else { ?>
        <input id="place_bet_button" class="btn btn-theme  place_bet_btn " type="submit" value="Place Bet" onclick="placeBet()">
        <?php } ?>
        <!-- </div> -->
    {% else %}
        <!-- <div class="border rounded p-2 bg-light"> -->
            <a   href="{{ url('login') }}" class="btn btn-theme place_bet_btn">Login to Bet</a>
       <!--  </div> -->

<!--                 <div class="border rounded p-2 bg-light">
            <div id="lg-fi" style="display:block;">

                <p>login to place a bet</p>
                <?php echo $this->tag->form("login/authenticate"); ?>
                <p>
                    <?php echo $this->tag->numericField(["mobile","placeholder"=>"07XX XXX XXX","class"=>"form-control"]) ?>
                </p>
                <p>
                    <?php echo $this->tag->passwordField(["password","name"=>"password","class"=>"form-control","placeholder"=>"Password"]) ?>
                </p>
                <div class="col-sm-12 zero-padding">
                    <div  ><?php echo $this->tag->submitButton(["Login","class"=>"btn btn-theme"]) ?></div>
                    <div ><a href="{{ url('signup') }}">Join now</a></div>
                </div>

                </form>
            </div>
        </div>
-->            {% endif %}
    </div>
</div> <!-- Betslip buttons closeup -->

</div>
<div class="card card-no-border payment-details">

    <div class="card-header">
        <h4 class="m-b-0 text-white">PAYBILL NUMBERS</h4>
    </div>
    <div class="row" style="margin:2px;">
        <div class="col-md-6 col-6 np">
            <img src="/images/lipa-na-mpesa-paybill-numbers-for-banks.fw_.png" width="110px" height="40px"></div>
        <div class="col-xl-6 col-6">
            <p style="font-family:'Roboto';text-align: right;font-size:14px;font-weight:400;color:white;margin-top:7px;">&nbsp;290080</p>
        </div>
    </div>
   <!-- <div class="row" style="margin:2px;">
        <div class="col-5 np">
            <img src="/images/airtel-money.png" width="110px"></div>
        <div class="col-md-5 col-6">
            <p style="font-family:'Roboto';font-size:14px;font-weight:400;color:white;margin-top:8px;margin-left:0px;">&nbsp;ScorePesa</p>
        </div>
    </div> -->
    
</div>

<div class="card card-no-border contacts">
    <div class="card-header">
        <h4 class="card-title m-b-0">Our Contacts</h4>
    </div>
    <div class="card-body">
        <ul class="feeds">
            <li>
                <div class="bg-info"><i class="fas fa-phone-volume"></i></div> +254 101 290 080
            </li>
            <li>
                <div class="bg-info"><i class="fas fa-phone-volume"></i></div> 0705 290 080
            </li>
        </ul>
    </div>
</div>