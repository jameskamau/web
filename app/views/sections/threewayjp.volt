<?php
function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

    return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}
?>
<div class=" col-md-12 row p-0 markets-desc web-view">
        <div class="col-md-7 p-0">
            &nbsp;
        </div>
        <div class="col-md-5 row np m1x2h ">
            <div class="col-md-4">HOME</div>
            <div class="col-md-4">DRAW</div>
            <div class="col-md-4">AWAY</div>
        </div>
</div>
<div class="card-body collapse show">
    <div class="table-responsive font-14">
<?php foreach ($games as $day): ?>
<?php 
$theMatch = @$theBetslip[$day['match_id']];
$odds = $day['threeway'];
$odds = explode(',',$odds);
$day['home_odd'] = $odds['0'];
$day['neutral_odd'] = $odds['2'];
$day['away_odd'] = $odds['1'];

?>

<div class="col-md-12 col-12 row np hrow">
    <div id="team-details" class="col-md-1 col-1 row team-details ">
         <?php echo $day['pos']; ?> 
    </div>
   <div id="team-details" class="col-md-6 col-11 row team-details np">
    <div id="customers" class="col-md-12 row np c-header">
     <?php echo $day['category']; ?> - 
     <?php echo $day['competition_name']; ?>
    
   </div>
      <div class="col-md-2 col-3 np mg-column">
        <?php echo date('g:i A', strtotime($day['start_time'])); ?> 
      </div>
      <div class="col-md-10 col-9 np mg-column">
        ID <?php echo $day['game_id']; ?>
      </div>
      <div class="col-md-2 col-3 np mg-column">
        <?php echo date('d/m/y', strtotime($day['start_time'])); ?> 
      </div>

      <div class="col-md-10 col-9 np mg-column">
         <?php echo $day['home_team']; ?> VS 
         <?php echo $day['away_team']; ?>
      </div>
   </div>
   
   <div id="markets" class="col-md-5 col-12 row p-0 mmkts">
      <div class="col-md-12 col-12 row np">
        <div class="col-md-4 col-4 np">
        <div class="top-matches">
            <button class="<?php echo $day['match_id']; ?> 
             <?php 
               echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']);
                   if ($theMatch['bet_pick'] == $day['home_team'] 
                       && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                       echo ' picked';
                   }
                ?> "
                hometeam="<?php echo $day['home_team']; ?>" 
                oddtype="3 Way" 
                bettype='jackpot' 
                awayteam="<?php echo $day['away_team']; ?>" 
                oddvalue="<?php echo $day['home_odd']; ?>" 
                target="javascript:;" 
                odd-key="<?php echo $day['home_team']; ?>" 
                parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" 
                custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']); ?>" 
                value="<?php echo $day['sub_type_id']; ?>" 
                special-value-value="0" 
                onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
                <span class="col-md-6 col-6 np mobile-view mobi-btn-mkt">1</span>
                <?php echo $day['home_odd']; ?>
            </button>
        </div> 

        </div>
        <div class="col-md-4 col-4 np">
        
        <div class="top-matches">
            <button class="<?php echo $day['match_id']; ?> <?php
                echo clean($day['match_id'] . $day['sub_type_id'] . 'draw');
                if ($theMatch['bet_pick'] == 'draw' && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                    echo ' picked';
                }
                ?> " 
                hometeam="<?php echo $day['home_team']; ?>" 
                oddtype="3 Way" bettype='jackpot' 
                awayteam="<?php echo $day['away_team']; ?>" 
                oddvalue="<?php echo $day['neutral_odd']; ?>" 
                custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . "draw"); ?>" 
                value="<?php echo $day['sub_type_id']; ?>" 
                odd-key="draw" 
                target="javascript:;" 
                parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                id="<?php echo $day['match_id']; ?>" 
                special-value-value="0" onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
                <span class="col-md-6 col-6 np mobile-view mobi-btn-mkt">X</span>
                <?php echo $day['neutral_odd']; ?>
    
           </button>
        </div>
        </div>
        <div class="col-md-4 col-4 np">
        <div class="top-matches">
         <button class="<?php echo $day['match_id']; ?> <?php
                echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']);
                if ($theMatch['bet_pick'] == $day['away_team'] && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                    echo ' picked';
                }
                ?> " 
                hometeam="<?php echo $day['home_team']; ?>" 
                oddtype="3 Way" bettype='jackpot' 
                awayteam="<?php echo $day['away_team']; ?>" 
                oddvalue="<?php echo $day['away_odd']; ?>" 
                value="<?php echo $day['sub_type_id']; ?>" 
                custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']); ?>" 
                odd-key="<?php echo $day['away_team']; ?>" 
                target="javascript:;" 
                parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                id="<?php echo $day['match_id']; ?>" 
                special-value-value="0" 
                onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
                <span class="col-md-6 col-6 np mobile-view mobi-btn-mkt">2</span>
            <?php echo $day['away_odd']; ?>

        </button>
        </div>
        </div>
      </div>


     
   </div>
 
</div>
<?php endforeach; ?>
               
    </div> <!-- end responsive table -->
       
</div><!-- end card body -->

<br/>
<div class="col-md-12 row" style="margin-bottom: 20px; color: #f6d900;">
    <div class="col-md-12 col-12 np">Total Stake : <b>Ksh 50</b></div>

    <div class="col-12 mobile-view np" style="text-align: center;">
      <div class="col-md-11 btsps pull-right">
        <button class="btn btn-theme  place-bet-btn remove" type="button" onclick="clearSlip()">Clear Slip</button>
        <button id="place_bet_button" type="button"  class="btn btn-theme  place_bet_btn" onclick="jackpotBet()">Place Bet</button>
      </div>
    </div>
    
</div>



