{{ partial("sections/header") }}

<div class="page-wrapper">
<!-- Navigation Bar -->
{{ partial("sections/left") }}

    <!-- Main Container -->
    <div class="container-fluid" style="min-height:800px">

        <div class="row">
            <!-- Game listing -->
            <div class ="col-md-8 np">
                <div class="match-list">

                {% if session.get('auth') != null %}
                <div class="card-header">
                    <h4 class="card-title m-b-0">MPESA Online Deposit</h4>
                </div>

                <div class="login-form p-3">
					<div class="inner-content">
						<p>Enter amount below, use your service pin to authorize the transaction. If you do not have a service pin, please follow the instructions to set.</p>

						 {{ this.flashSession.output() }}

							<?php echo $this->tag->form("deposit"); ?>

							 <p>
							    <label>Amount (KSH.) *</label>
							    <?php echo $this->tag->numericField(["amount","placeholder"=>"KSH. ","class"=>"form-control","required"=>"required"]) ?>
							 </p>

							  <p>
							    <?php echo $this->tag->submitButton(["Top up now","class"=>"btn btn-theme"]) ?> 
							 </p>

							</form>
					</div>
             </div> <!-- rounded -->

             {% else %}

            <div class="card-header">
                    <h4 class="card-title m-b-0">DEPOSIT</h4>
                </div>
            <div class="login-form p-3">

                <div class="inner-content">

                    <div id="definitions">
                        <div class="bigger-list">

                            <p> 1. Go to MPESA on your phone and select "Lipa Na MPESA"</p>

                            <p> 2. Select "Pay Bill" </p>

                            <p> 3. Select "Enter Business No."</p>

                            <p> 4. Key in 290080 as the Business Number and Click "OK"</p>

                            <p> 5. Select "Account no."</p>

                            <p> 6. Key in SCOREPESA as the Account no. and Click "OK"</p>

                            <p> 7. Key in the amount you wish to topup e.g. 20 and Click "OK"</p>

                            <p> 8. Proceed to specify your MPESA PIN and Click "OK"</p>

                            <p> 9. Confirm your transaction </p>

                            <p> 10. Your SCOREPESA Account will be successfully credited with the specified amount </p>

                        </div>
                    </div>
                </div>
            </div>
            {%endif%}

             </div> <!-- match list -->
            </div> <!- middle section -->
            <!-- Start right bar -->
            <div class="col-md-4 np bs">
                {{ partial("sections/right") }}
            </div>
            <!-- end right side bar -->
        </div> <!-- end row -->
      </div>
    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
</div>