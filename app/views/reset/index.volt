{{ partial("sections/header") }}


<div class="page-wrapper">
<!-- Navigation Bar -->
{{ partial("sections/left") }}


    <!-- Main Container -->
    <div class="container-fluid" style="min-height:800px">

        <div class="row">
            <!-- Game listing -->
            <div class ="col-md-8 np">
                <div class="match-list">

                <div class="card-header">
                    <h4 class="card-title m-b-0">Reset Password </h4>
                </div>

                <div class="forgot-pass p-3">

                    {{ this.flashSession.output() }}

                    <?php if(!@$profile_id): ?>
                    <?php echo $this->tag->form("reset/code"); ?>
                    <div class="form-group mb-1">
                        <label class="mb-0" for="inputCard">Mobile Number</label>
                        <?php echo $this->tag->numericField(["mobile","placeholder"=>"07XX XXX XXX","class"=>"form-control form-control-sm"]) ?>
                    </div>
                    <p>
                        <?php echo $this->tag->submitButton(["Reset","class"=>"btn btn-theme"]) ?>
                    </p>
                    </form>
                    <?php else: ?>
                    <?php echo $this->tag->form("reset/password"); ?>

                    <p>
                        <label>Mobile Number *</label>
                        <?php echo $this->tag->numericField(["mobile","disabled"=>"disabled","placeholder"=>"07XX XXX XXX","class"=>"form-control form-control-sm","value"=>$msisdn]) ?>
                    </p>


                    <p>
                        <label>Reset code *</label>
                        <?php echo $this->tag->numericField(["reset_code","placeholder"=>"XXXX","class"=>"form-control form-control-sm"]) ?>
                    </p>

                    <p>
                        <label>Password *</label>
                        <?php echo $this->tag->passwordField(["password","name"=>"password","class"=>"form-control form-control-sm","placeholder"=>"Password"]) ?>
                    </p>

                    <p>
                        <label>Confirm Password *</label>
                        <?php echo $this->tag->passwordField(["password","name"=>"repeatPassword","class"=>"form-control form-control-sm","placeholder"=>"Confirm password"]) ?>
                    </p>

                    <input type="hidden" value="{{profile_id}}" name="profile_id" />

                    <p>
                        <?php echo $this->tag->submitButton(["Reset Password","class"=>"btn btn-theme"]) ?>


                    </p>

                    </form>

                    <?php endif; ?>
                </div> <!-- rounded -->
            </div> <!-- match list -->
            </div> <!- middle section -->
            <!-- Start right bar -->
            <div class="col-md-4 np bs">
                {{ partial("sections/right") }}
            </div>
            <!-- end right side bar -->
        </div> <!-- end row -->
      </div>
    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
</div>

    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>

</div>



