{{ partial("sections/header") }}
<div id="container">
    <!-- Navigation Bar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light p-0 d-none d-lg-flex navbar-theme">
        <div class="container">
            <div class="pos-r d-flex w-100">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a class="nav-link" href="index-2.html">Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="products.html">Product List</a></li>
                    <li class="nav-item"><a class="nav-link" href="cart.html">Shopping Cart</a></li>
                    <li class="nav-item active"><a class="nav-link" href="checkout.html">Checkout</a></li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Pages
                        </a>
                        <div class="dropdown-menu t-90 smooth" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="about.html">About Us</a>
                            <a class="dropdown-item" href="blog.html">Blog</a>
                            <a class="dropdown-item" href="blog-detail.html">Blog Detail</a>
                            <a class="dropdown-item" href="compare.html">Compare</a>
                            <a class="dropdown-item" href="contact.html">Contact Us</a>
                            <a class="dropdown-item" href="cart-empty.html">Empty Shopping Cart</a>
                            <a class="dropdown-item" href="404.html">Error 404</a>
                            <a class="dropdown-item" href="faq.html">FAQ</a>
                            <a class="dropdown-item" href="login.html">Login</a>
                            <a class="dropdown-item" href="detail.html">Product Detail</a>
                            <a class="dropdown-item" href="register.html">Register</a>
                            <div class="dropdown-submenu">
                                <a href="#" class="dropdown-item">My Account</a>
                                <ul class="dropdown-menu smooth">
                                    <a href="account-order.html" class="dropdown-item">Orders</a>
                                    <a href="account-profile.html" class="dropdown-item">Profile</a>
                                    <a href="account-address.html" class="dropdown-item">Addresses</a>
                                    <a href="account-wishlist.html" class="dropdown-item">Wishlist</a>
                                    <a href="account-password.html" class="dropdown-item">Change Password</a>
                                    <div class="dropdown-submenu">
                                        <a href="#" class="dropdown-item">Submenu</a>
                                        <ul class="dropdown-menu smooth">
                                            <a href="#" class="dropdown-item">Submenu1</a>
                                            <a href="#" class="dropdown-item">Submenu2</a>
                                        </ul>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item dropdown mega-menu">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarMegaMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Mega Menu
                        </a>
                        <div class="dropdown-menu w-100 t-90 smooth" aria-labelledby="navbarMegaMenu">
                            <div class="mega-menu-content">
                                <div class="row">
                                    <div class="col">
                                        <h6 class="p-2 font-weight-bold border border-top-0 border-right-0 border-left-0">Top Categories</h6>
                                        <div class="list-group">
                                            <a href="products.html" class="list-group-item list-group-item-action py-1 px-3 border-0">Polo T-Shirt</a>
                                            <a href="products.html" class="list-group-item list-group-item-action py-1 px-3 border-0">Round Neck T-Shirt</a>
                                            <a href="products.html" class="list-group-item list-group-item-action py-1 px-3 border-0">V Neck T-Shirt</a>
                                            <a href="products.html" class="list-group-item list-group-item-action py-1 px-3 border-0">Hooded T-Shirt</a>
                                            <a href="products.html" class="list-group-item list-group-item-action py-1 px-3 border-0">Polo T-Shirt</a>
                                            <a href="products.html" class="list-group-item list-group-item-action py-1 px-3 border-0">Round Neck T-Shirt</a>
                                            <a href="products.html" class="list-group-item list-group-item-action py-1 px-3 border-0">V Neck T-Shirt</a>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <h6 class="p-2 font-weight-bold border border-top-0 border-right-0 border-left-0">Categories</h6>
                                        <div class="list-group">
                                            <a href="products.html" class="list-group-item list-group-item-action py-1 px-3 border-0">Dresses</a>
                                            <a href="products.html" class="list-group-item list-group-item-action py-1 px-3 border-0">Tops</a>
                                            <a href="products.html" class="list-group-item list-group-item-action py-1 px-3 border-0">Bottoms</a>
                                            <a href="products.html" class="list-group-item list-group-item-action py-1 px-3 border-0">Jackets / Coats</a>
                                            <a href="products.html" class="list-group-item list-group-item-action py-1 px-3 border-0">Sweaters</a>
                                            <a href="products.html" class="list-group-item list-group-item-action py-1 px-3 border-0">Gym Wear</a>
                                            <a href="products.html" class="list-group-item list-group-item-action py-1 px-3 border-0">Others</a>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-12 mb-2">
                                                <div class="card overlay-thumbnail">
                                                    <img src="images/product/type-polo.jpg" alt="" class="card-img">
                                                    <a href="products.html">
                                                        <div class="card-img-overlay d-flex justify-content-center align-items-center">
                                                            <div class="card-text">
                                                                <div>Polo T-Shirts <br/><small>40% OFF</small></div>
                                                                <button class="btn btn-sm btn-info rounded"><small>SHOP NOW</small></button>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="card overlay-thumbnail">
                                                    <img src="images/product/type-hooded.jpg" alt="" class="card-img">
                                                    <a href="products.html">
                                                        <div class="card-img-overlay d-flex justify-content-center align-items-center">
                                                            <div class="card-text">
                                                                <div>New Collection</div>
                                                                <button class="btn btn-sm btn-warning rounded"><small>SHOP NOW</small></button>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <a href="products.html"><img class="img-thumbnail" src="images/product/mega-menu.jpg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <span class="navbar-text">
                    <a><i class="fa ml-4 fa-truck"></i> Free Shipping</a>
                    <a><i class="fa ml-4 fa-money"></i> Cash on Delivery</a>
                    <a class="d-none d-xl-inline-block"><i class="fa ml-4 fa-lock"></i> Secure Payment</a>
                </span>
            </div>
        </div>
    </nav>

    <!-- Breadcrumb -->
    <div class="breadcrumb-container">
        <div class="container">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index-2.html">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                </ol>
            </nav>
        </div>
    </div>

    <!-- Main Container -->
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-4 mb-3">
                <div class="title"><span><span class="bg-dark text-white rounded-circle px-2">1</span> ADDRESS</span></div>
                <div class="form-group mb-1 mb-md-3">
                    <label for="inputName" class="mb-0 mb-md-2">Name *</label>
                    <input type="text" class="form-control" id="inputName">
                </div>
                <div class="form-group mb-1 mb-md-3">
                    <label for="inputAddress" class="mb-0 mb-md-2">Address Line 1 *</label>
                    <input type="text" class="form-control" id="inputAddress">
                </div>
                <div class="form-group mb-1 mb-md-3">
                    <label for="inputAddress2" class="mb-0 mb-md-2">Address Line 2 (optional)</label>
                    <input type="text" class="form-control" id="inputAddress2">
                </div>
                <div class="form-row">
                    <div class="form-group mb-1 mb-md-3 col-md-6">
                        <label for="inputCountry" class="mb-0 mb-md-2">Country *</label>
                        <input type="text" class="form-control" id="inputCountry">
                    </div>
                    <div class="form-group mb-1 mb-md-3 col-md-6">
                        <label for="inputZip" class="mb-0 mb-md-2">Zip/Postal Code *</label>
                        <input type="text" class="form-control" id="inputZip">
                    </div>
                    <div class="form-group mb-1 mb-md-3 col-md-6">
                        <label for="inputCity" class="mb-0 mb-md-2">City *</label>
                        <input type="text" class="form-control" id="inputCity">
                    </div>
                    <div class="form-group mb-1 mb-md-3 col-md-6">
                        <label for="inputRegion" class="mb-0 mb-md-2">Region *</label>
                        <input type="text" class="form-control" id="inputRegion">
                    </div>
                    <div class="form-group mb-1 mb-md-3 col-md-6">
                        <label for="inputPhone" class="mb-0 mb-md-2">Phone Number *</label>
                        <input type="text" class="form-control" id="inputPhone">
                    </div>
                    <div class="form-group mb-1 mb-md-3 col-md-6">
                        <label for="inputEmail" class="mb-0 mb-md-2">Email Address *</label>
                        <input type="email" class="form-control" id="inputEmail">
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4 mb-3">
                <div class="title"><span><span class="bg-dark text-white rounded-circle px-2">2</span> SHIPPING</span></div>
                <div class="mb-4">
                    <div class="custom-control custom-radio">
                        <input type="radio" id="shippingOption1" name="shipping-option" class="custom-control-input" checked="checked">
                        <label class="custom-control-label" for="shippingOption1">Standard Delivery</label>
                        <span class="float-right font-weight-bold">FREE</span>
                    </div>
                    <div class="ml-4 mb-2">(3-7 business days)</div>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="shippingOption2" name="shipping-option" class="custom-control-input">
                        <label class="custom-control-label" for="shippingOption2">Express Delivery</label>
                        <span class="float-right font-weight-bold">$10.00</span>
                    </div>
                    <div class="ml-4 mb-2">(2-4 business days)</div>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="shippingOption3" name="shipping-option" class="custom-control-input">
                        <label class="custom-control-label" for="shippingOption3">Next Business day</label>
                        <span class="float-right font-weight-bold">$20.00</span>
                    </div>
                </div>
                <div class="title"><span><span class="bg-dark text-white rounded-circle px-2">3</span> PAYMENT METHOD</span></div>
                <p class="mb-1">BILLING ADDRESS :</p>
                <div class="custom-control custom-checkbox mb-3">
                    <input type="checkbox" class="custom-control-input" id="billingAddress" checked="checked">
                    <label class="custom-control-label" for="billingAddress">Same as shipping address</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="paymentMethod1" name="payment-method" class="custom-control-input" checked="checked">
                    <label class="custom-control-label" for="paymentMethod1">Credit card</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="paymentMethod2" name="payment-method" class="custom-control-input">
                    <label class="custom-control-label" for="paymentMethod2">Debit card</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="paymentMethod3" name="payment-method" class="custom-control-input">
                    <label class="custom-control-label" for="paymentMethod3">Net banking</label>
                </div>
                <div class="bg-light p-3 rounded">
                    <div class="form-group mb-1">
                        <label class="mb-0" for="inputCard">Card Number</label>
                        <input type="text" class="form-control form-control-sm" id="inputCard">
                    </div>
                    <div class="form-group mb-1">
                        <label class="mb-0" for="inputFullname">Full name</label>
                        <input type="text" class="form-control form-control-sm" id="inputFullname">
                    </div>
                    <div class="form-row">
                        <div class="form-group mb-1 col-md-6">
                            <label class="mb-0" for="inputExpiry">Expiry date</label>
                            <input type="text" class="form-control form-control-sm" id="inputExpiry">
                        </div>
                        <div class="form-group mb-1 col-md-6">
                            <label class="mb-0" for="inputCvv">CVV code</label>
                            <input type="password" class="form-control form-control-sm" id="inputCvv">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="row">
                    <div class="col-md-6 col-lg-12">
                        <div class="title"><span>ORDER SUMMARY</span></div>
                        <div class="border rounded p-2 bg-light">
                            <div class="media mb-2">
                                <div class="media-body">
                                    <a href="detail.html" class="mt-0 text-default"><u>WranglerGrey Printed Slim Fit Round Neck T-Shirt</u></a>
                                    <div>Size: M <span class="mx-2">|</span> Color: Gray</div>
                                    <div>Price: $13.50 <span class="mx-2">|</span> Qty: 1 <span class="mx-2">|</span> Subtotal: $13.50</div>
                                </div>
                            </div>
                            <div class="media mb-2">
                                <div class="media-body">
                                    <a href="detail.html" class="mt-0 text-default"><u>CelioKhaki Printed Round Neck T-Shirt</u></a>
                                    <div>Size: M <span class="mx-2">|</span> Color: Gray</div>
                                    <div>Price: $13.50 <span class="mx-2">|</span> Qty: 1 <span class="mx-2">|</span> Subtotal: $13.50</div>
                                </div>
                            </div>
                            <div class="media mb-2">
                                <div class="media-body">
                                    <a href="detail.html" class="mt-0 text-default"><u>WranglerGrey Printed Slim Fit Round Neck T-Shirt</u></a>
                                    <div>Size: M <span class="mx-2">|</span> Color: Gray</div>
                                    <div>Price: $13.50 <span class="mx-2">|</span> Qty: 1 <span class="mx-2">|</span> Subtotal: $13.50</div>
                                </div>
                            </div>
                            <div class="media mb-2">
                                <div class="media-body">
                                    <a href="detail.html" class="mt-0 text-default"><u>WranglerGrey Printed Slim Fit Round Neck T-Shirt</u></a>
                                    <div>Size: M <span class="mx-2">|</span> Color: Gray</div>
                                    <div>Price: $13.50 <span class="mx-2">|</span> Qty: 1 <span class="mx-2">|</span> Subtotal: $13.50</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-12">
                        <div class="d-flex mt-3">
                            <div>Bag Total</div>
                            <div class="ml-auto font-weight-bold">$60.00</div>
                        </div>
                        <div class="d-flex">
                            <div>Bag Discount</div>
                            <div class="ml-auto font-weight-bold">$6.00</div>
                        </div>
                        <hr class="my-1">
                        <div class="d-flex">
                            <div>Coupon Discount</div>
                            <div class="ml-auto font-weight-bold">-</div>
                        </div>
                        <div class="d-flex">
                            <div>Shipping Cost</div>
                            <div class="ml-auto font-weight-bold">FREE</div>
                        </div>
                        <hr>
                        <div class="d-flex">
                            <div class="font-weight-bold h6">Order Total</div>
                            <div class="ml-auto price h5">$54.00</div>
                        </div>
                        <hr>
                        <div class="text-center"><button class="btn btn-theme">SUBMIT MY ORDER</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <div class="footer">
        <div class="container">
            <div class="row equal-height">
                <div class="col-lg-3 col-md-6">
                    <div class="title-footer"><span>About Us</span></div>
                    <ul>
                        <li>
                            Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et doloremmagna aliqua. Ut enim ad minim... <a href="about.html">Read More</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="title-footer"><span>Information</span></div>
                    <ul>
                        <li><i class="fa fa-angle-double-right"></i> <a href="faq.html">FAQ</a></li>
                        <li><i class="fa fa-angle-double-right"></i> <a href="#">Policy Privacy</a></li>
                        <li><i class="fa fa-angle-double-right"></i> <a href="#">Terms and Conditions</a></li>
                        <li><i class="fa fa-angle-double-right"></i> <a href="#">Shipping Methods</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="title-footer"><span>Categories</span></div>
                    <ul>
                        <li><i class="fa fa-angle-double-right"></i> <a href="products.html">Cras justo odio</a></li>
                        <li><i class="fa fa-angle-double-right"></i> <a href="products.html">Dapibus ac facilisis in</a></li>
                        <li><i class="fa fa-angle-double-right"></i> <a href="products.html">Morbi leo risus</a></li>
                        <li><i class="fa fa-angle-double-right"></i> <a href="products.html">Porta ac consectetur ac</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="title-footer"><span>Newsletter</span></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum, soluta, tempora, ipsa voluptatibus porro vel laboriosam</p>
                    <div class="input-group input-group-sm">
                        <input class="form-control" type="text" placeholder="Email Address" aria-label="Email Address">
                        <div class="input-group-append">
                            <button class="btn btn-theme" type="button">Subscribe</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="title-footer"><span>Our Store</span></div>
                    <ul class="footer-icon">
                        <li><span><i class="fa fa-map-marker"></i></span> 212 Lorem Ipsum. Dolor Sit</li>
                        <li><span><i class="fa fa-phone"></i></span> +123-456-789</li>
                        <li><span><i class="fa fa-envelope"></i></span> <a href="mailto:cs@domain.tld">cs@domain.tld</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="title-footer"><span>Follow Us</span></div>
                    <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum</p>
                    <ul class="follow-us">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-rss"></i></a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="title-footer"><span>Payment Method</span></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum, soluta, tempora, ipsa voluptatibus porro vel laboriosam</p>
                    <img src="images/payment-1.png" alt="Payment-1">
                    <img src="images/payment-2.png" alt="Payment-2">
                    <img src="images/payment-3.png" alt="Payment-3">
                    <img src="images/payment-4.png" alt="Payment-4">
                    <img src="images/payment-5.png" alt="Payment-5">
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="title-footer"><span>My Account</span></div>
                    <ul>
                        <li><i class="fa fa-angle-double-right"></i> <a href="account-order.html">Orders</a></li>
                        <li><i class="fa fa-angle-double-right"></i> <a href="account-profile.html">Profile</a></li>
                        <li><i class="fa fa-angle-double-right"></i> <a href="account-wishlist.html">Wishlist</a></li>
                        <li><i class="fa fa-angle-double-right"></i> <a href="#">Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="text-center copyright">
            Copyright &copy; 2018 Mimity All right reserved
        </div>
    </div>

    <!-- Login Modal -->
    <div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="LoginModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="LoginModalLabel">Enter Your Information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="InputUsername" class="font-weight-bold">Username</label>
                        <input type="text" class="form-control" id="InputUsername" placeholder="Enter Username">
                    </div>
                    <div class="form-group">
                        <label for="InputPassword" class="font-weight-bold">Password</label>
                        <input type="password" class="form-control" id="InputPassword" placeholder="Password">
                        <a href="#" class="float-right text-default"><small>Forgot Password ?</small></a>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="checkRememberMe">
                        <label class="custom-control-label" for="checkRememberMe">Remember Me</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-theme btn-sm">Enter</button>
                </div>
            </div>
        </div>
    </div>

    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
                    return false">
        <i class="fa fa-angle-double-up"></i>
    </a>

</div>
