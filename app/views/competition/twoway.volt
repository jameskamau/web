{{ partial("sections/header") }}

<div class="page-wrapper">
<!-- Navigation Bar -->
{{ partial("sections/left") }}

    <!-- Main Container -->
    <div class="container-fluid" style="min-height:800px">

        <div class="row">
            <!-- Game listing -->
            <div class ="col-md-8 np">
                <div class="match-list">

                    <div class="card-header">
                        <h4 class="card-title m-b-0">
                             <?php foreach($theCompetition as $compe): ?>
                              <span class="sport-on-match-list"> 
                                <img class="side-icon" src="/sport/<?=$compe['sport_name'];?>.png">  
                                <?php echo strtoupper($compe['sport_name']); ?>
                              </span> - 

                              <?php echo $compe['competition_name'].", ".$compe['category']; ?>
                              <?php endforeach; ?>
                        </h4>
                    </div>

                    <div class=" col-md-12 row p-0 markets-desc web-view">
                        <div class="col-md-7 p-0">
                            &nbsp;
                        </div>
                        <div class="col-md-4 row np m1x2h0 ">
                            <div class="col-md-6 np">1</div>
                            <div class="col-md-6 np">2</div>
                        </div>
                        
                         <div class="col-md-1 row np movu">
                            More
                         </div>
                        
                    </div>


                    <!-- End ta content header -->
                    <!-- Tab panels -->
                    <div class="tab-content">
                        <!--Panel 1-->
                        <div class="tab-pane fade in show active" id="panel0" role="tabpanel">

                           {{ partial("sections/twoway") }}
                        </div>
                    </div>
                
                </div> <!-- match list -->
            </div > <!-- end col md-8 for middle content -->
            <!-- Start right bar -->
            <div class="col-md-4 np bs">
                {{ partial("sections/right") }}
            </div>
            <!-- end right side bar -->
        </div> <!-- end row -->
    </div>
    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
</div>



