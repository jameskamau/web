{{ partial("sections/header") }}

<div class="page-wrapper">
<!-- Navigation Bar -->
{{ partial("sections/left") }}

<style>
    
    .jp-header{

        color: #FFC107 !important;
        font-size: 20px;
        font-weight: bold;
    }
</style>

    <!-- Main Container -->
    <div class="container-fluid" style="min-height:800px">
        <div class="row">
            <!-- Game listing -->
            <div class ="col-md-8 np">
                <div class="match-list">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-6 hidden-xs">
                                <h4 class="card-title m-b-0">SCOREPESA JACKPOT</h4>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <h4 class="card-title m-b-0">
                                    <span class="jp-header">
                                        KSH 1,000,000 TO BE WON DAILY
                                    </span>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content-header">
                        <div class="tab-content-header-text detail-page">

                            <!-- <img src="images/coming-soon.png" width="100%"/> -->
                            {{ partial("partials/jackpot") }}
                            
                        </div>   
                    </div>
                </div> <!-- match list -->
            </div >
             <!-- end col md-8 for middle content -->
            <!-- Start right bar -->
            <div class="col-md-4 np bs">
                {{ partial("sections/right") }}
            </div>
            <!-- end right side bar -->
        </div> <!-- end row -->
    </div>
    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
</div>