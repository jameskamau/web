{{ partial("sections/header") }}

<!-- Navigation Bar -->
{{ partial("sections/left") }}

<div class="page-wrapper">

    <!-- Main Container -->
    <div class="container-fluid" style="min-height:800px">

        <div class="row">
            <!-- Game listing -->
            <div class ="col-md-8 np">
                <div class="match-list">

                <div class="card-header">
                    <h4 class="card-title m-b-0">Sign Up </h4>
                </div>

                <div class="signup p-3">
                    <?= $this->flashSession->output(); ?>
                    <?php echo $this->tag->form("signup/join"); ?>
                    <div class="form-group mb-1">
                        <label class="mb-0" for="inputCard">Mobile Number</label>
                        <?php echo $this->tag->numericField(["mobile","placeholder"=>"07XX XXX XXX","class"=>"form-control form-control-sm"]) ?>
                    </div>
                    <div class="form-group mb-1">
                        <label class="mb-0" for="inputFullname">Password</label>
                        <?php echo $this->tag->passwordField(["password","name"=>"password","class"=>"form-control form-control-sm","placeholder"=>"Password"]) ?>
                    </div>
                    <div class="form-group mb-1">
                        <label class="mb-0" for="inputFullname">Confirm Password</label>
                        <?php echo $this->tag->passwordField(["password","name"=>"repeatPassword","class"=>"form-control form-control-sm","placeholder"=>"Confirm Pass"]) ?>
                    </div>

                    <p class="check-boxes">  
                        <input type="checkbox" name="terms" value="1"> I accept these <a href="{{url('terms')}}" style="padding-left:0;display:inline;">Terms & Conditions</a> * </input>
                        <br/>
                        <br/>
                        <input type="checkbox" name="age" value="1"> I'm over 18 years of age * </input>
                    </p>
                    <p>
                        <?php echo $this->tag->submitButton(["Register","class"=>"btn btn-theme"]) ?>
                    </p>
                    </form>
                </div> <!-- rounded -->
            </div> <!-- match list -->
            </div> <!- middle section -->
            <!-- Start right bar -->
            <div class="col-md-4 np bs">
                {{ partial("sections/right") }}
            </div>
            <!-- end right side bar -->
        </div> <!-- end row -->
      </div>
    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
</div>



