<div class="gn">

<div class='alert alert-success alert-dismissible betslip-success' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>Bet successfully placed!</div>
    <ul class="bet-option-list">
<div id='slip-holder' class="bet alu">
<header><div class="betslip-header"><span class="col-sm-2 bkmrk"><i class="fa fa-bookmark-o" aria-hidden="true"></i></span><span class="col-sm-8 slp">Jackpot Betslip</span><span class="col-sm-2 slip-counter"><?= $slipCountJ ?></span></div></header>
<img src="{{url('img/loader.gif')}}" class="loader" />
<button id="slip-button-close" type="button" class="close mobi" aria-hidden="true">×</button>
<div id="betslipB" class="betslipB ">

</div>

<div id="quick-login">

<p>Please login to place a bet</p>

<?php echo $this->tag->form("login/authenticate"); ?>

 <p>
    <label>Mobile Number *</label>
    <?php echo $this->tag->numericField(["mobile","placeholder"=>"07XX XXX XXX","class"=>"form-control"]) ?>
 </p>

 <p>
    <label>Password *</label>
    <?php echo $this->tag->passwordField(["password","name"=>"password","class"=>"form-control","placeholder"=>"Password"]) ?>
 </p>

  <div class="col-sm-12 zero-padding">
    <div class="col-sm-4 zero-padding"><?php echo $this->tag->submitButton(["Login","class"=>"cg fm"]) ?></div>
    <div class="col-sm-8 zero-padding"><a href="{{ url('signup') }}">Join now</a></div>
 </div>

</form>
</div>

</div>

<!-- <div class="qv rc alu bon">
  <a href="{{url('signup')}}"><img src="{{url('https://s3-eu-west-1.amazonaws.com/betika/img/join-bonus.jpg')}}" class="full-width" /></a>
</div> -->

<div class="qv rc alu">
<div class="qv rc alu paybill">
M-PESA PAYBILL<br/>
<span class="white main">290290</span>
ACCOUNT: <span class="white">BETIKA</span>
</div>
	  <div class="support">
    <div class="helpline">
      <span class="col-sm-4"><img src="{{url('img/contact-24.png')}}"/></span>
			<span class="col-sm-8">0729 290 290<br> 0780 290 290</span>
    </div>
    <div class="mail">
    <i class="fa fa-envelope-o" aria-hidden="true"></i> &nbsp; support@betika.com
    </div>
		</div>
      </div>

    </div>
