<div class="inner-content">
<h2>Verify account</h2>
<p>
Thanks for your interest in ScorePesa. Once you fill in the form below, you will receive the verification code via SMS. </p>
        
{{ this.flashSession.output() }}

<?php echo $this->tag->form("verify/check"); ?>

 <p>
    <label>Enter Mobile Number *</label>
    <?php echo $this->tag->numericField(["mobile","placeholder"=>"07XX XXX XXX","class"=>"form-control"]) ?>
 </p>

  <p>
    <label>Enter Verification Code *</label>
    <?php echo $this->tag->numericField(["verification_code","placeholder"=>"XXXX","class"=>"form-control"]) ?>
 </p>

  <p>
    <?php echo $this->tag->submitButton(["Verify","class"=>"cg fm"]) ?>
 </p>

</form>

<script>
(function(){
 postback(); 
})()
</script> 

</div>
