<div class="inner-content">
<h2>Login</h2>

        {{ this.flashSession.output() }}

<?php echo $this->tag->form("login/authenticate"); ?>

 <p>
    <label>Mobile Number *</label>
    <?php echo $this->tag->numericField(["mobile","placeholder"=>"07XX XXX XXX","class"=>"form-control"]) ?>
 </p>

 <p>
    <label>Password *</label>
    <?php echo $this->tag->passwordField(["password","name"=>"password","class"=>"form-control","placeholder"=>"Password"]) ?>
 </p>

<p>
  <input type="checkbox" name="remember" value="1"> Remember me</input>
 </p>

  <p>
    <?php echo $this->tag->submitButton(["Login","class"=>"cg fm"]) ?> <a class="forgot" href="{{ url('reset') }}"><span class="sticky-hidden">Forgotten password?</span></a>
 </p>

</form>
</div>