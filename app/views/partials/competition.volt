<section class="panel"> 
<div class="panel-header">
      <h2>
      <?php foreach($theCompetition as $compe): ?>
      <?php echo $compe['competition_name'].", ".$compe['category']; ?>
      <?php endforeach; ?>
      </h2>
    </div>  


<?php 
      function clean($string) {
         $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
         $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

         return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
      }

      function shorten($string){
      return substr($string, 0, 10)."...";
      }
    ?>

<div class="web-element"> 
<header class="top-matches match-header events-header">
   <div class="col-sm-2">Date</div>
   <div class="col-sm-2 events-odd">GAME ID</div>
   <div class="col-sm-3">1</div>
   <div class="col-sm-1 events-odd">X</div>
   <div class="col-sm-3">2</div>
   <div class="col-sm-1 events-odd"></div>
</header> 

<div class="mobi-list">         
<?php foreach($matches as $day): ?>
<?php
   $theMatch = @$theBetslip[$day['match_id']];
?>

  <div class="col-sm-12 top-matches">
   <div class="col-sm-2 pad"><?php echo date('m/d H:i', strtotime($day['start_time'])); ?></div>
   <div class="col-sm-2 events-odd pad"><?php echo $day['game_id']; ?></div>
   <div class="col-sm-3" style="padding:0;">
      <button class="<?php echo $day['match_id']; ?> <?php
                  echo clean($day['match_id'].$day['sub_type_id'].'1'); 
                     if($theMatch['bet_pick']=='1' && $theMatch['sub_type_id']=='10'){
                        echo ' picked';
                     }
                  ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="3 Way" bettype='prematch' awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['home_odd']; ?>" target="javascript:;" odd-key="1" parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id']."1"); ?>" value="10" special-value-value="0" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))"><span class="theteam col-sm-9"><?php echo $day['home_team']; ?></span> <span class="theodds col-sm-3"><?php echo $day['home_odd']; ?></span></button>
   </div>
   <div class="col-sm-1 events-odd" style="padding:0;">
   <button class="<?php echo $day['match_id']; ?> <?php
                   echo clean($day['match_id'].$day['sub_type_id']."X"); 
                     if($theMatch['bet_pick']=='X' && $theMatch['sub_type_id']=='10'){
                        echo ' picked';
                     }
                   ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="3 Way" bettype='prematch' awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['neutral_odd']; ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id']."X"); ?>" value="10" odd-key="X" target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" special-value-value="0" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))"><span class="label label-inverse"> <?php echo $day['neutral_odd']; ?></span></button>
   </div>
   <div class="col-sm-3" style="padding:0;">
      <button class="odds away <?php echo $day['match_id']; ?> <?php echo clean($day['match_id'].$day['sub_type_id']."2");
                     if($theMatch['bet_pick']=='2' && $theMatch['sub_type_id']=='10'){
                        echo ' picked';
                     }
                   ?>" value="10" parentmatchid="<?php echo $day['parent_match_id']; ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="3 Way" bettype='prematch' awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['away_odd']; ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id']."2"); ?>" odd-key="2" target="javascript:;" id="<?php echo $day['match_id']; ?>" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))"><span class="label label-inverse"> <?php echo substr($day['away_team'], 0, 10)."..."; ?> <span class="mobi-odd"><?php echo $day['away_odd']; ?></span></span></button>
   </div>
   <div class="col-sm-1 events-odd pad"><a class="side" href="<?php echo 'match?id='.$day['match_id']; ?>">+<?php echo $day['side_bets']; ?> </a></div>
</div>

<?php endforeach; ?>

            </div>

            <?php foreach($matches as $day): ?>
<?php
   $theMatch = @$theBetslip[$day['match_id']];
?>
      <div class="col-sm-12 top-matches mobi">

            <div class="teams highlited" style="">
            <span class="bold"><?php echo $day['home_team']; ?></span> &nbsp; v  &nbsp; <span class="bold"><?php echo $day['away_team']; ?></span>
            </div>

            <div class="col-sm-1" style="display: inline-block;font-size: 13px;float: right;">
            <a class="side" href="<?php echo 'match?id='.$day['match_id']; ?>">+<?php echo $day['side_bets']; ?> </a>
            </div>

            <div class="meta">
            <?php echo date('d/m', strtotime($day['start_time'])); ?> 
            <?php echo date('H:i', strtotime($day['start_time'])); ?>
            Game ID: <?php echo $day['game_id']; ?>
            </div>
            
            <div class="row">
            <div class="col-sm-12">
                  <button class="odds home <?php echo $day['match_id']; ?> <?php
                  echo clean($day['match_id'].$day['sub_type_id'].'1'); 
                     if($theMatch['bet_pick']=='1' && $theMatch['sub_type_id']=='10'){
                        echo ' picked';
                     }
                  ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="3 Way" bettype='prematch' awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['home_odd']; ?>" target="javascript:;" odd-key="1" parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id']."1"); ?>" value="10" special-value-value="0" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))"><span class="theteam col-sm-9"><?php echo substr($day['home_team'], 0, 10)."..."; ?></span> <span class="theodds col-sm-3"><?php echo $day['home_odd']; ?></span></button>

                  <button class="odds neutral <?php echo $day['match_id']; ?> <?php
                   echo clean($day['match_id'].$day['sub_type_id']."X"); 
                     if($theMatch['bet_pick']=='X' && $theMatch['sub_type_id']=='10'){
                        echo ' picked';
                     }
                   ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="3 Way" bettype='prematch' awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $day['neutral_odd']; ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id']."X"); ?>" value="10" odd-key="X" target="javascript:;" parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" special-value-value="0" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))"><span class="label label-inverse"> X <span class="mobi-odd"><?php echo $day['neutral_odd']; ?></span></span></button>
                  <button class="odds away <?php echo $day['match_id']; ?> <?php echo clean($day['match_id'].$day['sub_type_id']."2");
                     if($theMatch['bet_pick']=='2' && $theMatch['sub_type_id']=='10'){
                        echo ' picked';
                     }
                   ?>" value="10" hometeam="<?php echo $day['home_team']; ?>" awayteam="<?php echo $day['away_team']; ?>" parentmatchid="<?php echo $day['parent_match_id']; ?>" oddvalue="<?php echo $day['away_odd']; ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id']."2"); ?>" odd-key="2" target="javascript:;" id="<?php echo $day['match_id']; ?>" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))"><span class="label label-inverse"> <?php echo substr($day['away_team'], 0, 10)."..."; ?> <span class="mobi-odd"><?php echo $day['away_odd']; ?></span></span></button>
                  </div>
            </div>
            </div>
<?php endforeach; ?>
      </div>      
</section>