<?php
	if(!empty($matchInfo))
	{ 
?>
<div class="panel-header">
      <?php 
      function clean($string) {
         $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
         $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

         return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
      }
      ?> 
     <h4 class="inline-block"> <?php echo $matchInfo['home_team']." vs ".$matchInfo['away_team']; ?> </h4>  <span class="">- <?php echo $matchInfo['competition_name'].", ".$matchInfo['category']; 
      ?></span><br>
     <span class=""><?php echo date('jS M Y', strtotime($matchInfo['start_time']))." - ".date('g:i a', strtotime($matchInfo['start_time'])); ?>
     |
     <?php echo "Game ID: ".$matchInfo['game_id']; ?>
</span>
</div>
<?php
	}
?>


<?php 
$sub_type_id=''; 
?>
<div class="col-sm-12 top-matches match" style="padding:10px;"> 
<?php foreach($subTypes as $bt): ?>
<?php 
$theMatch = @$theBetslip[$bt['match_id']];
?>
<?php if($bt['name']): ?>
<?php if($sub_type_id!=$bt['sub_type_id']): ?>
   <div class="col-sm-12 top-matches header yellow">{{bt['name']}}</div>
<?php endif; ?>

<?php 
if($bt['name']){
$sub_type_id=$bt['sub_type_id']; 
$special_bet_value=$bt['special_bet_value']; 
}
?>

<?php if(($sub_type_id==$bt['sub_type_id']) && !(($bt['sub_type_id']=='47') || ($bt['sub_type_id']=='56' && $bt['special_bet_value']=='1.5') || ($bt['sub_type_id']=='56' && $bt['special_bet_value']=='3.5')  || ($bt['sub_type_id']=='56' && $bt['special_bet_value']=='0.5') || ($bt['sub_type_id']=='56' && $bt['special_bet_value']=='4.5') || ($bt['sub_type_id']=='56' && $bt['special_bet_value']=='5.5') || ($bt['sub_type_id']=='56' && $bt['special_bet_value']=='6.5') || ($bt['sub_type_id']=='55' && $bt['special_bet_value']=='0:2') || ($bt['sub_type_id']=='55' && $bt['special_bet_value']=='2:0') || ($bt['sub_type_id']=='55' && $bt['special_bet_value']=='0:2') || ($bt['sub_type_id']=='353') || ($bt['sub_type_id']=='352'))): ?>
 

<button class="pick col-sm-4 odds <?php echo $bt['match_id']; ?> <?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$special_bet_value); 
                    if($theMatch['bet_pick']==$bt['odd_key'] && $theMatch['sub_type_id']==$bt['sub_type_id'] && $theMatch['special_bet_value']==$bt['special_bet_value']){
                        echo ' picked';
                     }
                  ?>" oddtype="<?= $bt['name'] ?>" parentmatchid="<?php echo $matchInfo['parent_match_id']; ?>" bettype='prematch' hometeam="<?php echo $matchInfo['home_team']; ?>" awayteam="<?php echo $matchInfo['away_team']; ?>" oddvalue="<?php echo $bt['odd_value']; ?>" custom="<?php echo clean($bt['match_id'].$bt['sub_type_id'].$bt['odd_key'].$special_bet_value); ?>" target="javascript:;" id="<?php echo $bt['match_id']; ?>" odd-key="<?php echo $bt['odd_key']; ?>" value="<?php echo $bt['sub_type_id']; ?>" special-value-value="<?= $special_bet_value ?>" onClick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'))"><span class="label label-inverse blueish"> <?php echo $bt['display'].' '.$special_bet_value; ?></span> <span class="label label-inverse blueish odd-value"> <?php echo $bt['odd_value']; ?></span> </button> 
<?php endif; ?>
<?php endif; ?>
<?php endforeach; ?>
</div>
