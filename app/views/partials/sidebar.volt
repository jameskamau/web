<div class="gn sidebar">
<!-- <div class="qv rc aog alu web-element">
<header><div class="header-holder"><span class="col-sm-10">Live Betting</span><span class="col-sm-2 header-icon"><i class="fa fa-rss" aria-hidden="true"></i></span></div></header>
</div> -->
      <div class="qv rc aog alu web-element">
      <div class="logo">
      <a class="e" href="{{url('')}}" title="ScorePesa">
      <img src="img/logo.jpg" alt="ScorePesa" title="ScorePesa">
      </a>
      </div>
      </div>

{% cache "sidebar" 6000 %}

<div class="web-element">
<header><div class="header-holder"><span class=""> Sports </span></div></header>
<?php 
$id=''; 
$categoryID='';
$competitionID='';
?>
<ul class="sidebar-menu aoi nav">
<?php foreach($sports as $sp): ?>
<?php if($id!=$sp['sport_id']): ?>

  <?php 
    $sport = str_replace(" ","",$sp['sport_name']);
    $sport = strtolower($sport);
  ?>

<li class="treeview">
<a href="#">
<span class="topl"> <img src="{{ url('/img/sport/') }}{{sport}}.png" alt="{{sp['sport_name']}}" />  {{sp['sport_name']}} </span>
<img class="down-arrow pull-right" src="{{url('img/add.svg')}}">
</a>
<ul class="treeview-menu">
<?php $id=$sp['sport_id']; ?>

      
<?php foreach($sports as $s): ?>
  <?php $url=@$sportType[$s['sport_id']].'?id='.$s['competition_id']; ?>
  <?php if($id == $s['sport_id'] && $categoryID != $s['category_id']): ?>
      <li class="treeview"><a href="{{url(url)}}">
      <span> {{s['category']}} </span> <img class="down-arrow pull-right" src="{{url('img/add.svg')}}">
      </a>

      <ul class="treeview-menu">
      <?php $categoryID=$s['category_id']; ?>
      <?php foreach($sports as $st): ?>
        <?php 
        $url=@$sportType[$s['sport_id']].'?id='.$st['competition_id']; 
        $competitionID=$st['competition_id'];
        ?>
        <?php if($categoryID == $st['category_id']): ?>
            <li><a href="{{url(url)}}">
            <span> {{st['competition_name']}} </span>
            </a></li>
        <?php endif; ?>
        <?php $categoryID=$s['category_id']; ?>
      <?php endforeach; ?>
      </ul>

      </li>
  <?php endif; ?>
<?php endforeach; ?>
</ul>
</li>
<?php endif; ?>
<?php endforeach; ?>
</ul>
</div>

{% endcache %}
    </div>