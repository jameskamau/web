
<div class="panel-header">
	<h2>Online Withdrawal (MPESA)</h2>
</div>
<div class="inner-content">
<p>Enter amount below:</p>

        {{ this.flashSession.output() }}

<?php echo $this->tag->form("withdraw/withdrawal"); ?>

 <p>
    <label>Amount (Ksh.) *</label>
    <?php echo $this->tag->numericField(["amount","placeholder"=>"Ksh. ","class"=>"form-control","required"=>"required"]) ?>
 </p>

<p>
    <?php echo $this->tag->submitButton(["Withdraw now","class"=>"cg fm"]) ?> 
 </p>

</form>
</div>