<div class="">

<?php if(@$rugby): ?>
<ul class="f-list">
  <?php foreach((array)$rugby as $league): ?>
      <li>
        <a class="b fa fa-chevron-right" href="competition?country=<?= $league['category'] ?>"><span class="f-league"><?= $league['category'] ?></span></a>
      </li>
  <?php endforeach; ?>
</ul>
<?php else: ?>

	 <ul class="f-list">
          <!-- {% for league in topLeagues %}
            {% set name = league["competition_name"] %}
            {% set id = league["competition_id"] %}
            {% set category = league["category"] %}
            <li><a class="b fa fa-chevron-right" href="{{url('football?id=')}}{{id}}">
            <span class="f-league"> {{ name }}, {{ category }} </span>
            </a></li>
            {% endfor  %} -->
            <li ><a class="b fa fa-chevron-right" href="{{url('upcoming')}}">
            <span class="f-league"> Upcoming Events </span>
            </a></li>
            <li ><a class="b fa fa-chevron-right" href="{{url('competition?id=')}}26">
            <span class="f-league"> English Premier League </span>
            </a></li> 
           
            <li ><a class="b fa fa-chevron-right" href="{{url('competition?id=')}}7261">
            <span class="f-league"> Champions League, Group A </span>
            </a></li>
            <li ><a class="b fa fa-chevron-right" href="{{url('competition?id=')}}7263">
            <span class="f-league"> Champions League, Group C </span>
            </a></li>
            <li ><a class="b fa fa-chevron-right" href="{{url('competition?id=')}}7269">
            <span class="f-league"> Champions League, Group E </span>
            </a></li>
            <li ><a class="b fa fa-chevron-right" href="{{url('competition?id=')}}7270">
            <span class="f-league"> Champions League, Group F </span>
            </a></li>
            <li ><a class="b fa fa-chevron-right" href="{{url('competition?id=')}}7275`">
            <span class="f-league"> Champions League, Group G </span>
            </a></li>
            <li ><a class="b fa fa-chevron-right" href="{{url('competition?id=')}}7273">
            <span class="f-league"> Champions League, Group H </span>
            </a></li>

            <li ><a class="b fa fa-chevron-right" href="{{url('competition?id=')}}40">
            <span class="f-league"> Europa League, Group G </span>
            </a></li>
            <li ><a class="b fa fa-chevron-right" href="{{url('competition?id=')}}25">
            <span class="f-league"> Europa League, Group H </span>
            </a></li>
            <li ><a class="b fa fa-chevron-right" href="{{url('competition?id=')}}39">
            <span class="f-league"> Europa League, Group I </span>
            </a></li>
            <li ><a class="b fa fa-chevron-right" href="{{url('competition?id=')}}13">
            <span class="f-league"> Europa League, Group J </span>
            </a></li>

            <li ><a class="b fa fa-chevron-right" href="{{url('competition?id=')}}10">
            <span class="f-league" > Championship </span>
            </a></li>
            <li ><a class="b fa fa-chevron-right" href="{{url('competition?id=')}}6147">
            <span class="f-league"> La Liga </span>
            </a></li>
            <li ><a class="b fa fa-chevron-right" href="{{url('competition?id=')}}92">
            <span class="f-league"> Bundesliga </span>
            </a></li>
            <li ><a class="b fa fa-chevron-right" href="{{url('competition?id=')}}116">
            <span class="f-league"> Seria A </span>
            </a></li>
            <li ><a class="b fa fa-chevron-right" href="{{url('competition?id=')}}63">
            <span class="f-league"> Ligue 1 </span>
            </a></li>
  </ul>

<div class="panel-header">
  <h2>Football (A-Z)</h2>
</div>

<!-- <ul class="f-list">
<?php $theLeague=''; ?>
	<?php foreach($allLeagues as $league): ?>
    <?php if($league['category']!=$theLeague): ?>
			<li>
				<a class="b fa fa-chevron-right" href="competition?country=<?= $league['category'] ?>"><span class="f-league"><?= $league['category'] ?></span></a>
			</li>
    <?php endif; ?>
      <?php $theLeague=$league['category']; ?>
	<?php endforeach; ?>
</ul> -->


<?php 
$categoryID='';
?>
<ul class="f-list">
<?php foreach($sports as $sp): ?>

<?php if($sp['sport_id']=='5'): ?>

<?php if($categoryID!==$sp['category_id']): ?>
<li>
<a class="b fa fa-chevron-right" href="{{url('mobile/competition?country')}}={{sp['category']}}">
<span class="f-league"> {{sp['category']}} </span>
</a>
</li>

<?php endif; ?>

<?php endif; ?>
<?php $categoryID=$sp['category_id']; ?>
<?php endforeach; ?>
</ul>

<?php endif; ?>
</div>
