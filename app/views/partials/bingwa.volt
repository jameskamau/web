<section class="panel jackpot"> 
<header>
   <div class="header-holder">
   <span class="col-sm-1 main-icon"><i class="icon-football fa fa-futbol-o" aria-hidden="true"></i></span><span class="col-sm-11 main-header heads">MidWeek Jackpot - 19/10/2016 </span>
   </div>
</header>


<?php 
      function clean($string) {
         $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
         $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

         return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
      }
    ?>

<img src="{{url('https://s3-eu-west-1.amazonaws.com/betika/img/bingwafour.jpg')}}" class="full-width" alt="Betika Jackpot"/>

<!-- <div class="events-header"><div class="events-odd text-center">The first game of the Jackpot has already started.</div></div> -->


 <div class="">  
<?php foreach($games as $day): ?>
<?php
   $theMatch = @$theBetslip[$day['match_id']];
   $odds = $day['correctscore'];
   $scores = explode(',',$odds);
?>

  <div class="jp-matches row" style="margin-right:0;margin-left:0;">

   <div class="teams highlited events-odd" style="">
            <span class="bold"><?php echo $day['pos'].". ".$day['home_team']; ?></span> &nbsp; v  &nbsp; <span class="bold"><?php echo $day['away_team']; ?></span><br>
            <span class="sml">Date: <?php echo date('d/m', strtotime($day['start_time'])); ?> 
            <?php echo date('H:i', strtotime($day['start_time'])); ?></span>
    </div>

  <?php foreach($scores as $score): ?>
  <button class="col-sm-1 <?php echo $day['match_id']; ?> <?php echo clean($day['match_id'].$day['sub_type_id'].@$day['odd_key'].$day['special_bet_value'].$score); 
                    if($theMatch['bet_pick']==$score && $theMatch['sub_type_id']=='332'){
                        echo ' picked';
                     }
                  ?>" pos="<?= $day['pos']; ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="Bingwa four Jackpot" bettype='bingwafour' awayteam="<?php echo $day['away_team']; ?>" parentmatchid="<?php echo $day['parent_match_id']; ?>" oddvalue="1" value="332" custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['odd_key'].@$day['special_bet_value'].$score); ?>" bet-type="bingwafour" odd-key="<?= $score; ?>" target="javascript:;" id="<?php echo $day['match_id']; ?>" onclick="addBet(this.id,this.value,this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('pos'))"><span class="theteam col-sm-12"> <?php echo $score; ?> </span></button>
  <?php endforeach; ?>

</div>

<?php endforeach; ?>

</div>
<img src="{{url('img/loader.gif')}}" class="loader" />

<div class="header-holder mobi">
 <span class="col-sm-6">Total Stake: </span><span class="col-sm-4">Ksh. 50</span><button type="button" id="place_bet_button" class="col-sm-6 place-bet-btn " onclick="jackpotBet()">PLACE A BET</button>
</div>

<div class="betj web-element">
  <div class="col-sm-12">
    <div class="col-sm-6">
      <div class="header-holder">
       <span class="col-sm-4">Total Stake </span><span class="col-sm-4 header-icon">Ksh. 50</span>
      </div>
    </div>
    <div class="col-sm-6">
      <button type="button" id="place_bet_button" class="place-bet-btn" onclick="jackpotBet()">PLACE A BET</button>
    </div>
  </div>
</div>

<div class='alert alert-danger alert-dismissible betslip-error' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>You have insufficient balance please top up</div>
            
</section>