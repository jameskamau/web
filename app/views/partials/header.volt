<?php

$id = $this->session->get('auth')['id'];

  $phql = "SELECT ProfileBalance.balance,ProfileBalance.bonus_balance from ProfileSettings left join ProfileBalance on ProfileSettings.profile_id=ProfileBalance.profile_id where ProfileSettings.profile_id='$id' limit 1";
  $checkUser = $this->modelsManager->executeQuery($phql);

  $checkUser=$checkUser->toArray();
  
 ?>

<div id="shrink-header" class="">
    <nav class="ck pc app-navbar top-nav">
        <div class="by">
            <div class="or">
                <div class="pull-right">
                    <a href="{{url('betmobile')}}" class="mobi betslip-mobi" title="ScorePesa Betslip">Betslip (<span class="slip-counter"><?= $slipCount ?></span>)</a>
                    {% if session.get('auth') != null %} 
                    
                    <span class="balnc" href="">
                        Bal: 

          <?php 
              echo $checkUser['0']['balance'];
          ?>
                        | Bon: <?= @$checkUser['0']['bonus_balance']; ?></span>
                    {% endif %}
                </div>
                <a class="e mobi" href="{{url('')}}" title="ScorePesa">
                    <img src="{{url('img/logo.jpg')}}" alt="ScorePesa" title="ScorePesa">
                </a>

            </div>
            <div class="f collapse" id="navbar-collapse-main">
                <div class="qv rc alu paybill col-sm-6">
                    <img src="{{url('img/mpesa.png')}}" />
                    <span class="main">Paybill No:
                        290080</span>
                </div>
                {% if session.get('auth') != null %} {# variable is not set #}
                <div class="og ale ss profile">
                    <div>{{session.get('auth')['mobile']}} - Balance: KSH.

          <?php 
              echo @$checkUser['0']['balance'];
          ?> &nbsp; | &nbsp; Bonus: <?= @$checkUser['0']['bonus_balance']; ?></div>
                    <div>
                        <a href="{{url('withdraw')}}"> Withdraw </a>
                        <a href="{{url('logout')}}"> Logout </a>
                    </div>
                </div>
                {% else %} {# variable is set #}

          <?php echo $this->tag->form(["login/authenticate","class"=>"ow og i web-element"]); ?>
                <div class="et leftpad">

                    <input type="text" name="mobile" class="form-control" data-action="grow" placeholder="Mobile Number"><br/> <span class="sticky-hidden"><input type="checkbox" name="remember" value="1"> Remember me</input></span>
                </div>
                <div class="et leftpad">
                    <input type="password" name="password" class="form-control" data-action="grow" placeholder="Password"><br/>
                    <a href="{{ url('reset') }}" title="ScorePesa reset password"><span class="sticky-hidden">Forgotten password?</span></a>
                </div>
                <div class="et">
                    <button class="cg fp ">Log In</button>
                    <a class="cg fm leftpad" href="{{ url('signup') }}" title="ScorePesa Join now">Join Now</a>
                </div>
                </form>

                {% endif %}

            </div>

        </div>
    </nav>

    <nav class="second-nav ck pc app-navbar ">
        <div class="by f collapse" id="navbar-collapse-main">
            <div class="thedate">
                <span><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                <span id="date-part" class="nav navbar-nav ss">
                </span>
                <a href="https://twitter.com/ScorePesa" target="_blank"  href="#" class="tw" title="ScorePesa twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="https://www.facebook.com/ScorePesa" target="_blank" class="fb" title="ScorePesa facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            </div>


            <ul class="nav navbar-nav og ale ss">
                <li>
                    <a class="cg fm ox anl" href="{{url('')}}">Home</a>
                </li>

                <!-- <li>
                    <a class="cg fm ox anl" href="{{url('jackpot')}}" data-original-title="" title="ScorePesa weekly Jackpot">
                        Jackpot
                    </a>
                </li> -->

                <li>
                    <a class="cg fm ox anl" href="{{url('how-to-play')}}" title="ScorePesa How to play">
                        How to Play
                    </a>
                </li>

                <li>
                    <a class="cg fm ox anl" href="{{url('')}}" data-original-title="" title="ScorePesa Todays Highlights">
                        Contact us
                    </a>
                </li>
                <!-- <li>
                  <a class="cg fm ox anl" href="{{url('#')}}" data-original-title="" title="">
                    Live Betting
                  </a>
                </li> -->
                {% if session.get('auth') != null %} {# variable is not set #}
                <li>
                    <a href="{{url('mybets')}}" title="ScorePesa My bets"> My Bets </a>
                </li>
                {% endif %}

            </ul>

            <ul class="nav navbar-nav st su sv">

            </ul>

            <ul class="nav navbar-nav hidden">

            </ul>

        </div>

        <div class="mobi">

            {% if session.get('auth') != null %} {# variable is not set #}
            <ul class="nav in">
                <li>
                    <a href="{{url('mybets')}}" title="ScorePesa Jackpot"> My Bets </a>
                </li>
                <!-- <li><a href="{{url('deposit')}}" title="ScorePesa Deposit"> Deposit </a></li> -->
                <li><a href="{{url('withdraw')}}" title="ScorePesa Withdraw"> Withdraw </a></li> 
                <li>
                    <a href="{{url('logout')}}" title="ScorePesa Logout"> Logout </a>
                </li>
            </ul>
            {% else %} {# variable is set #}
            <ul class="nav men">
                <li>
                    <a class="active" href="{{url('signup')}}" title="ScorePesa register">Register!</a>
                </li>
                <li>
                    <a data-toggle="modal" href="{{url('login')}}" title="ScorePesa login">Login</a>
                </li>
            </ul>
            {% endif %}


            <ul class="main-nav">          
                <li>
                    <a href="{{url('')}}">Highlights</a>
                </li>
                <li>
                    <a href="{{url('mobile/football')}}">Football</a>
                </li>
                <li>
                    <a href="{{url('mobile/sports')}}" title="ScorePesa sports">All Sports</a>
                </li>
                <!-- <li>
                  <a href="{{url('jackpot')}}" title="ScorePesa Jackpot">Jackpot</a>
                </li> -->
                <!-- 
                <li>
                  <a href="{{url('bingwa4')}}" title="ScorePesa Bingwa4">Bingwa 4 <!-- <span class="new">new!</span> </a>
                </li>-->
            </ul>
            <div class="paybill-mobile">Paybill No. 290080</div>

        </div>

    </nav>

</div>
