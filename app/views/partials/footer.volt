<footer class="">
<div class="container footer-in" id="navbar-collapse-main">
 	<div class="footer-bottom">
 	<div class="col-sm-12">
 	<div class="col-sm-7 footer-info">
		SOLAMI Limited is licensed by BCLB (Betting Control and Licensing Board of Kenya) under the Betting, Lotteries and Gaming Act, Cap 131, Laws of Kenya under License number: 816.
	</div>
	<div class="col-sm-5">
	<a href="{{ url('terms') }}">Terms and Conditions</a>
	</div>
	</div>
	</div> 
</div>
</footer>