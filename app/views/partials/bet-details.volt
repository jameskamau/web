<div class="">
    <div class="panel-header">
        <h2><a href="{{url('mybets')}}"><i class="fa fa-chevron-left" aria-hidden="true"></i></a> Bet Details</h2>
    </div>
    
        <style>
        body {
            font-family: "Open Sans", sans-serif;
            line-height: 1.25;
        }
        table {
            border: 1px solid #ccc;
            border-collapse: collapse;
            margin: 0;
            padding: 0;
            width: 100%;
            table-layout: fixed;
        }
        table caption {
            font-size: 1.5em;
            margin: .5em 0 .75em;
        }
        table tr {
            padding: .35em;
        }
        table th,
        table td {
            padding: .625em;
            text-align: center;
        }
        table th {
            letter-spacing: .1em;
            text-transform: uppercase;
        }
        @media screen and (max-width: 600px) {
            table {
                border: 0;
            }
            table caption {
                font-size: 1.3em;
            }
            table thead {
                border: none;
                clip: rect(0 0 0 0);
                height: 1px;
                margin: -1px;
                overflow: hidden;
                padding: 0;
                position: absolute;
                width: 1px;
            }
            table tr {
                border-bottom: 3px solid #ddd;
                display: block;
                margin-bottom: .625em;
            }
            table td {
                border-bottom: 1px solid #ddd;
                display: block;
                font-size: .8em;
                text-align: right;
            }
            table td:before {
                /*
                * aria-label has no advantage, it won't be read inside a table
                content: attr(aria-label);
                */
                content: attr(data-label);
                float: left;
                font-weight: bold;
                text-transform: uppercase;
            }
            table td:last-child {
                border-bottom: 0;
            }
        }
    </style>

    
    
    <!--<div class="table-responsive"> <table class="table white-color"> 
            <thead> 
                <tr> <th>ID</th> <th> Bet</th> <th>Date</th> <th>Bet Amount</th> <th>Possible Win</th> <th>Status</th> </tr> 
            </thead> 
            <tbody> 
                <tr> 
            <a href="#"><td><?= $myBet['bet_id'] ?></td></a>
            <td><?php 
                if($myBet['total_matches']>1){
                echo "Multi Bet";
                }else{
                echo "Single Bet";
                }
                ?></td> 
            <td><?= date('d/m H:i', strtotime($myBet['created'])) ?></td> 
            <td><?= $myBet['bet_amount'] ?></td> 
            <td><?= $myBet['possible_win'] ?></td> 
            <td>
                <?php
                if($myBet['status']==1){
                echo 'Not Due';
                }elseif($myBet['status']==5){
                echo 'Won';
                }elseif($myBet['status']==3){
                echo 'Lost';
                }elseif($myBet['status']==4){
                echo 'Cancelled';
                }elseif($myBet['status']==9){
                echo 'Pending Jackpot';
                }else{
                echo 'Lost';
                }
                ?>
            </td> 
            </tr> 
            </tbody> 
        </table> 
    </div>-->
    
        <table>
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Bet</th>
                <th scope="col">Date</th>
                <th scope="col">Bet Amount</th>
                <th scope="col">Possible Win</th>
                <th scope="col">Status</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-label="ID"><?= $myBet['bet_id'] ?></td>
                <td data-label="Bet"><?php 
                if($myBet['total_matches']>1){
                echo "Multi Bet";
                }else{
                echo "Single Bet";
                }
                ?></td>
                <td data-label="Date"><?= date('d/m H:i', strtotime($myBet['created'])) ?></td>
                <td data-label="Bet Amount"><?= $myBet['bet_amount'] ?></td>
                <td data-label="Possible Win"><?= $myBet['possible_win'] ?></td>
                <td data-label="Status">                <?php
                if($myBet['status']==1){
                echo 'Pending results';
                }elseif($myBet['status']==5){
                echo 'Won';
                }elseif($myBet['status']==3){
                echo 'Check Slip';
                }elseif($myBet['status']==4){
                echo 'Cancelled';
                }elseif($myBet['status']==9){
                echo 'Pending Jackpot';
                }else{
                echo 'Check Slip';
                }
                ?></td>
            </tr>
        </tbody>
    </table>
    
    
    
    
    <h3>Event List (<?= sizeof($betDetails) ?>)</h3>
    
      <table>
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Date</th>
                <th scope="col">Name</th>
                <th scope="col">Odds</th>
                <th scope="col">Pick</th>
                <th scope="col">Outcome</th>
                <th scope="col">Results</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($betDetails as $bet): ?>
            <tr>
                <td data-label="ID"><?= $bet['game_id'] ?></td>
                <td data-label="Date">
                    <?= date('d/m H:i', strtotime($bet['start_time'])) ?>
                </td>
                <td data-label="Name">
                    <?= $bet['home_team']." v ".$bet['away_team'] ?>
                </td>
                <td data-label="Odds"><?= $bet['odd_value'] ?></td>
                <td data-label="Pick"><?= $bet['bet_pick'] ?></td>
                <td data-label="Outcome"><?php 
                    if(empty($bet['winning_outcome']))
                        echo "Pending";
                        else echo $bet['winning_outcome'];

                    ?>
                </td>
                <td><?= $bet['ft_score'] ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</div>