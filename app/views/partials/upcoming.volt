
<?php
function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

    return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}
?>
<!-- load upcoming games here -->
                            
<?php foreach ($today as $day): ?>
<?php
    $theMatch = @$theBetslip[$day['match_id']];
?>
<div class="col-md-12 row np hrow">
<div id="team-details" class="col-md-4 col-sm-12 row team-details np">
  <div id="customers" class="col-md-12 col-sm-12 row np c-header">
   <?php echo $day['category']; ?> - 
   <?php echo $day['competition_name']; ?>

  </div>
  <div class="col-md-2 col-sm-4 np mg-column">
    <?php echo date('H:i', strtotime($day['start_time'])); ?> 
  </div>
  <div class="col-md-10 col-sm-8 np mg-column">
    ID <?php echo $day['game_id']; ?>
  </div>
  <div class="col-md-2 col-sm-4 np mg-column">
    <?php echo date('d/m/y', strtotime($day['start_time'])); ?> 
  </div>

  <div class="col-md-10 col-sm-8  np mg-column">
     <?php echo $day['home_team']; ?> VS 
     <?php echo $day['away_team']; ?>
  </div>
    
  <!-- <div class="col-12 np row hidden-xs m1x2h">
    <div class="col-11 np">
        <div class="col-1 np ">1</div>
        <div class="col-1 np">X</div>
        <div class="col-1 np">2</div>
        <div class="col-1 np">1X</div>
        <div class="col-1 np">X2</div>
        <div class="col-2 np">12</div>
        <div class="col-2 np">OV 2.5</div>
        <div class="col-2 np">UN 2.5</div>
    </div>
    <div class="col-1 np">More</div>
    
  </div> -->
 
</div>

<div id="markets" class="col-md-8 col-11 row p-0 mmkts">
  <div class="col-md-4 col-xs-12 row np">
    <div class="col-md-4 col-xs-4 np">
    <div class="top-matches">
        <button class="<?php echo $day['match_id']; ?> 
         <?php 
           echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']);
               if ($theMatch['bet_pick'] == $day['home_team'] 
                   && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                   echo ' picked';
               }
            ?> "
            hometeam="<?php echo $day['home_team']; ?>" 
            oddtype="1x2" 
            bettype='prematch' 
            awayteam="<?php echo $day['away_team']; ?>" 
            oddvalue="<?php echo $day['home_odd']; ?>" 
            target="javascript:;" 
            odd-key="<?php echo $day['home_team']; ?>" 
            parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" 
            custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']); ?>" 
            value="<?php echo $day['sub_type_id']; ?>" 
            special-value-value="0" 
            onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
            <span class="odds-btn-label mobile-view">HOME<br/></span>
            <?php echo $day['home_odd']; ?>
        </button>
    </div> 

    </div>
    <div class="col-md-4 col-xs-4 np">
    
    <div class="top-matches">
        <button class="<?php echo $day['match_id']; ?> <?php
            echo clean($day['match_id'] . $day['sub_type_id'] . 'draw');
            if ($theMatch['bet_pick'] == 'draw' && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                echo ' picked';
            }
            ?> " 
            hometeam="<?php echo $day['home_team']; ?>" 
            oddtype="1x2" bettype='prematch' 
            awayteam="<?php echo $day['away_team']; ?>" 
            oddvalue="<?php echo $day['neutral_odd']; ?>" 
            custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . "draw"); ?>" 
            value="<?php echo $day['sub_type_id']; ?>" 
            odd-key="draw" 
            target="javascript:;" 
            parentmatchid="<?php echo $day['parent_match_id']; ?>" 
            id="<?php echo $day['match_id']; ?>" 
            special-value-value="0" onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
            <span class="odds-btn-label mobile-view">DRAW<br/></span>
            <?php echo $day['neutral_odd']; ?>

       </button>
    </div>
    </div>
    <div class="col-md-4  col-xs-4 np">
    <div class="top-matches">
     <button class="<?php echo $day['match_id']; ?> <?php
            echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']);
            if ($theMatch['bet_pick'] == $day['away_team'] && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                echo ' picked';
            }
            ?> " 
            hometeam="<?php echo $day['home_team']; ?>" 
            oddtype="1x2" bettype='prematch' 
            awayteam="<?php echo $day['away_team']; ?>" 
            oddvalue="<?php echo $day['away_odd']; ?>" 
            value="<?php echo $day['sub_type_id']; ?>" 
            custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']); ?>" 
            odd-key="<?php echo $day['away_team']; ?>" 
            target="javascript:;" 
            parentmatchid="<?php echo $day['parent_match_id']; ?>" 
            id="<?php echo $day['match_id']; ?>" 
            special-value-value="0" 
            onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
            <span class="odds-btn-label mobile-view">AWAY<br/></span>
        <?php echo $day['away_odd']; ?>

    </button>
    </div>
    </div>
  </div>

  <!-- Double chance market remove -->
  <div class="col-md-4 col-xs-12 row np">
    <div class="col-md-4 col-xs-4 np">
    
    <div class="top-matches">
    <button class="<?php echo $day['match_id']; ?> <?php
            echo clean($day['match_id'] . '10' . $day['home_team'] . ' or draw');
            if ($theMatch['bet_pick'] == $day['home_team'] . ' or draw' && $theMatch['sub_type_id'] == 10) {
                echo ' picked';
            }
            ?> " 
            hometeam="<?php echo $day['home_team']; ?>" 
            oddtype="Double Chance" bettype='prematch' 
            awayteam="<?php echo $day['away_team']; ?>" 
            oddvalue="<?php echo $day['double_chance_1x_odd']; ?>" 
            value="10" 
            custom="<?php echo clean($day['match_id'] . '10' . $day['home_team'] . ' or draw'); ?>" 
            odd-key="<?= $day['home_team'] . ' or draw';?>" 
            target="javascript:;" 
            parentmatchid="<?php echo $day['parent_match_id']; ?>" 
            id="<?php echo $day['match_id']; ?>" 
            special-value-value="0" 
            onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
            <span class="odds-btn-label mobile-view">1 OR X<br/></span>
        <?php echo $day['double_chance_1x_odd']; ?>

    </button>
    </div>
    </div>
    <div class="col-md-4 col-xs-4 np">
    
    <div class="top-matches">
    <button class="<?php echo $day['match_id']; ?> <?php
            echo clean($day['match_id'] . '10' . 'draw or '.$day['away_team']);
            if ($theMatch['bet_pick'] == 'draw or '.$day['away_team'] && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                echo ' picked';
            }
            ?> " 
            hometeam="<?php echo $day['home_team']; ?>" 
            oddtype="Double Chance" bettype='prematch' 
            awayteam="<?php echo $day['away_team']; ?>" 
            oddvalue="<?php echo $day['double_chance_x2_odd']; ?>" 
            value="10" 
            custom="<?php echo clean($day['match_id'] . '10' . 'draw or '.$day['away_team']); ?>" 
            odd-key="<?= 'draw or '.$day['away_team']; ?>" 
            target="javascript:;" 
            parentmatchid="<?php echo $day['parent_match_id']; ?>" 
            id="<?php echo $day['match_id']; ?>" 
            special-value-value="0" 
            onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
            <span class="odds-btn-label mobile-view">X OR 2<br/></span>
        <?php echo $day['double_chance_x2_odd']; ?>

    </button>
    </div>
    </div>
    
    <div class="col-md-4 col-xs-4 np">
    
    <div class="top-matches">
    <button class="<?php echo $day['match_id']; ?> <?php
            echo clean($day['match_id'] . '10' . $day['away_team']. ' or '.$day['away_team']);
            if ($theMatch['bet_pick'] == $day['away_team']. ' or '.$day['away_team'] && $theMatch['sub_type_id'] == 10) {
                echo ' picked';
            }
            ?> " 
            hometeam="<?php echo $day['home_team']; ?>" 
            oddtype="Double Chance" bettype='prematch' 
            awayteam="<?php echo $day['away_team']; ?>" 
            oddvalue="<?php echo $day['double_chance_12_odd']; ?>" 
            value="10" 
            custom="<?php echo clean($day['match_id'] . '10' . $day['away_team']. ' or '.$day['away_team']); ?>" 
            odd-key="<?= $day['home_team']. ' or '.$day['away_team']; ?>" 
            target="javascript:;" 
            parentmatchid="<?php echo $day['parent_match_id']; ?>" 
            id="<?php echo $day['match_id']; ?>" 
            special-value-value="0" 
            onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
            <span class="odds-btn-label mobile-view">1 OR 2<br/></span>
            <?php echo $day['double_chance_12_odd']; ?>

    </button>
    </div>

    </div> <!-- md4 double chance -->

    </div><!-- Double chance market -->

    <div class="col-md-3 col-xs-12 row np">
        <div class="col-md-6 col-xs-6 np">
        <div class="top-matches">
        <button class="<?php echo $day['match_id']; ?> <?php
                echo clean($day['match_id'] . '18' . 'under 2.5');

                if ($theMatch['bet_pick'] == 'under 2.5' && $theMatch['sub_type_id'] == '18') {
                    echo ' picked';
                }
                ?> " 
                hometeam="<?php echo $day['home_team']; ?>" 
                oddtype="Total" bettype='prematch' 
                awayteam="<?php echo $day['away_team']; ?>" 
                oddvalue="<?php echo $day['under_25_odd']; ?>" 
                value="18" 
                custom="<?php echo clean($day['match_id'] . 18 . 'under 2.5'); ?>" 
                odd-key="under 2.5" 
                target="javascript:;" 
                parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                id="<?php echo $day['match_id']; ?>" 
                special-value-value="2.5" 
                onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
                <span class="odds-btn-label mobile-view">UNDER 2.5<br/></span>
            <?php echo $day['under_25_odd']; ?>

        </button>
        </div>
        </div>
        <div class="col-md-6 col-xs-6 np">
        <div class="top-matches">
        <button class="<?php echo $day['match_id']; ?> <?php
                echo clean($day['match_id'] . '18' . 'over 2.5');
                if ($theMatch['bet_pick'] == 'over 2.5' && $theMatch['sub_type_id'] == 18) {
                    echo ' picked';
                }
                ?> " 
                hometeam="<?php echo $day['home_team']; ?>" 
                oddtype="Total" bettype='prematch' 
                awayteam="<?php echo $day['away_team']; ?>" 
                oddvalue="<?php echo $day['over_25_odd']; ?>" 
                value="18" 
                custom="<?php echo clean($day['match_id'] . '18' . 'over 2.5'); ?>" 
                odd-key="over 2.5" 
                target="javascript:;" 
                parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                id="<?php echo $day['match_id']; ?>" 
                special-value-value="2.5" 
                onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">
                <span class="odds-btn-label mobile-view">OVER 2.5<br/></span>
            <?php echo $day['over_25_odd']; ?>

        </button>
        </div>
      </div>
      </div>
  
    <div class="col-md-1 col-1 np more-markets">
        <a class="side"  href="<?php echo 'markets?id=' . $day['match_id']; ?>">+<?php echo $day['side_bets']; ?> </a>
    </div>
 </div>
</div>
<?php endforeach; ?>
<!-- End of  games -->                           