<div class="inner-content">
<h2>Welcome to ScorePesa</h2>
<p>
Thanks for your interest in ScorePesa! Once you fill in the registration form below, you will receive the verification code via SMS. To complete the registration process, please submit the verification code.<br/>
</p>

        <?= $this->flashSession->output(); ?>

<?php echo $this->tag->form("signup/join"); ?>

 <p>
    <label>Mobile Number *</label>
    <?php echo $this->tag->numericField(["mobile","placeholder"=>"07XX XXX XXX","class"=>"form-control"]) ?>
 </p>

 <p>
    <label>Password *</label>
    <?php echo $this->tag->passwordField(["password","name"=>"password","class"=>"form-control","placeholder"=>"Password"]) ?>
 </p>

  <p>
  <label>Confirm Password *</label>
    <?php echo $this->tag->passwordField(["password","name"=>"repeatPassword","class"=>"form-control","placeholder"=>"Confirm password"]) ?>
 </p>

 <p>  
<input type="checkbox" name="terms" value="1"> I accept these <a href="{{url('terms')}}" style="padding-left:0;display:inline;">Terms & Conditions</a> * </input>
<br/>
<br/>
<input type="checkbox" name="age" value="1"> I'm over 18 years of age * </input>
 </p>

<script src="https://static.twinpinenetwork.com/twinpine-postback.js">
postback();
</script> 

 <p>
    <?php echo $this->tag->submitButton(["Register","class"=>"cg fm"]) ?>
 </p>

</form>
</div>
