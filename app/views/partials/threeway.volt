
<div class="card-body collapse show">
    <div class="table-responsive font-14">
            <?php

            function clean($string) {
                $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
                $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

                return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
            }
            ?>
            

            <?php foreach ($matches as $day): ?>
                <table id="customers" class="table color-bordered-table" width="">
                    <tbody>

                            <?php
                            $theMatch = @$theBetslip[$day['match_id']];
                            ?>
                            <tr>
                                <td colspan="100%" >
                                    <span>
                                        <?php echo $day['category']; ?>
                                        - <?php echo $day['competition_name']; ?> |
                                        <span class="web-view">Game ID</span> <?php echo $day['game_id']; ?>  
                                    </span>
                                    <span class="web-view">
                                    <?php echo date('Y/d/m H:i', strtotime($day['start_time'])); ?>
                                    </span>
                                    <span class="mobile-view">
                                    <span style="float:right;padding:3px;">
                                        <?php echo date('d/m H:i', strtotime($day['start_time'])); ?>
                                    </span>
                                </td>
                            </tr>

                            <tr class="mobile-view">
                                <td colspan="100%">
                                    <b> <?= $day['home_team'] ?> VS <?= $day['away_team'] ?> </b>
                                </td>
                            </tr>

                            <tr>

                                <td width="42%" data-label="Home Team" >
                                    <div class="col-sm-12 top-matches" style="padding:0">
                                        <button class="<?php echo $day['match_id']; ?> <?php
                                        echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']);
                                        if ($theMatch['bet_pick'] == $day['home_team'] && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                                            echo ' picked';
                                        }
                                        ?> "
                                                hometeam="<?php echo $day['home_team']; ?>" 
                                                oddtype="3 Way" 
                                                bettype='prematch' 
                                                awayteam="<?php echo $day['away_team']; ?>" 
                                                oddvalue="<?php echo $day['home_odd']; ?>" 
                                                target="javascript:;" 
                                                odd-key="<?php echo $day['home_team']; ?>" 
                                                parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" 
                                                custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']); ?>" 
                                                value="<?php echo $day['sub_type_id']; ?>" 
                                                special-value-value="0" 
                                                onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">


                                            <span class="theteam col-sm-9 web-view" style="float:left;">
                                                <?php echo $day['home_team']; ?>
                                            </span> 
                                            <span class="theteam col-sm-9 mobile-view" style="float:left;">1
                                            </span>
                                            <span class="theodds col-sm-3"><?php echo $day['home_odd']; ?></span>
                                        </button>
                                    </div>
                                </td>
                                <?php if($day['neutral_odd'] > 0) { ?>

                                <td width="10%" data-label="Draw">
                                    <div class="top-matches" style="padding:0">
                                        <button class="<?php echo $day['match_id']; ?> <?php
                                        echo clean($day['match_id'] . $day['sub_type_id'] . 'draw');
                                        if ($theMatch['bet_pick'] == 'draw' && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                                            echo ' picked';
                                        }
                                        ?> " 
                                                hometeam="<?php echo $day['home_team']; ?>" 
                                                oddtype="3 Way" bettype='prematch' 
                                                awayteam="<?php echo $day['away_team']; ?>" 
                                                oddvalue="<?php echo $day['neutral_odd']; ?>" 
                                                custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . "draw"); ?>" 
                                                value="<?php echo $day['sub_type_id']; ?>" 
                                                odd-key="draw" 
                                                target="javascript:;" 
                                                parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                                                id="<?php echo $day['match_id']; ?>" 
                                                special-value-value="0" onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">

                                            <span class="theodds">
                                                <?php echo $day['neutral_odd']; ?>
                                            </span>

                                        </button>
                                    </div>
                                </td>
                                <?php } ?>
                                <td width="42%" data-label="Away Team" >
                                    <div class="top-matches" style="padding:0">
                                        <button class="<?php echo $day['match_id']; ?> <?php
                                        echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']);
                                        if ($theMatch['bet_pick'] == $day['away_team'] && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                                            echo ' picked';
                                        }
                                        ?> " 
                                                hometeam="<?php echo $day['home_team']; ?>" 
                                                oddtype="3 Way" bettype='prematch' 
                                                awayteam="<?php echo $day['away_team']; ?>" 
                                                oddvalue="<?php echo $day['away_odd']; ?>" 
                                                value="<?php echo $day['sub_type_id']; ?>" 
                                                custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']); ?>" 
                                                odd-key="<?php echo $day['away_team']; ?>" 
                                                target="javascript:;" 
                                                parentmatchid="<?php echo $day['parent_match_id']; ?>" 
                                                id="<?php echo $day['match_id']; ?>" 
                                                special-value-value="0" 
                                                onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'))">

                                            <span class="theteam col-sm-9 web-view" style="float:left;">
                                                <?php echo $day['away_team']; ?> 
                                            </span> 
                                             <span class="theteam col-sm-9 mobile-view" style="float:left;">2
                                                    </span>
                                            <span class="theodds col-sm-3"><?php echo $day['away_odd']; ?></span>

                                        </button>
                                    </div>
                                </td>
                                <td width="6%" class="markets">
                                    <?php
                                    if ($theMatch && $theMatch['sub_type_id'] != 10) {
                                        //echo ' picked';
                                    }
                                    ?>
                                    <a class="side"  href="<?php echo 'markets?id=' . $day['match_id']; ?>">+<?php echo $day['side_bets']; ?> </a>
                                </td>
                            </tr>
                            
                         </tbody>
                </table>
            <?php endforeach; ?>
               
    </div> <!-- end responsive table -->
       
</div><!-- end card body -->
