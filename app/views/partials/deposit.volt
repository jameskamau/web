<!--
<div class="panel-header">
	<h2>Online Deposit (MPESA)</h2>
</div>
<div class="inner-content">
<p>Enter amount below, use your service pin to authorize the transaction. If you do not have a service pin, please follow the instructions to set.</p>

        {{ this.flashSession.output() }}

<?php echo $this->tag->form("deposit/topup"); ?>

 <p>
    <label>Amount (Ksh.) *</label>
    <?php echo $this->tag->numericField(["amount","placeholder"=>"Ksh. ","class"=>"form-control","required"=>"required"]) ?>
 </p>

  <p>
    <?php echo $this->tag->submitButton(["Top up now","class"=>"cg fm"]) ?> 
 </p>

</form>
</div>
-->

{% if session.get('auth') != null %}
<div class="">
<div class="panel-header">
        <h2>Make Instant Deposit</h2>
</div>
<div class="table-responsive inner-content whitebg ">
<p>Enter amount below:</p>

        {{ this.flashSession.output() }}

<?php echo $this->tag->form("/deposit"); ?>

 <p>
    <label>Amount (KSh.) *</label>
    <?php echo $this->tag->numericField(["amount","placeholder"=>"KSh. ","class"=>"form-control","required"=>"required"]) ?>
 </p>

<p>
    <?php echo $this->tag->submitButton(["Deposit","class"=>"cg fm"]) ?>
 </p>

</form>
</div>
</div>
{% else %}

<div class="panel-header"><h2>DEPOSIT</h2></div>
<div class="odd-wrapper">

    <div class="nav-side">

      <button class="deposit" style="background-color: #EF4323;width: 100%;">How to Top Up Your Account</button>
        <div id="definitions">
            <div class="bigger-list">

                <p> 1. Go to MPESA on your phone and select "Lipa Na MPESA"</p>

                <p> 2. Select "Pay Bill" </p>

                <p> 3. Select "Enter Business No."</p>

                <p> 4. Key in 290080 as the Business Number and Click "OK"</p>

                <p> 5. Select "Account no."</p>

                <p> 6. Key in SCOREPESA as the Account no. and Click "OK"</p>

                <p> 7. Key in the amount you wish to topup e.g. 20 and Click "OK"</p>

                <p> 8. Proceed to specify your MPESA PIN and Click "OK"</p>

                <p> 9. Confirm your transaction </p>

                <p> 10. Your SCOREPESA Account will be successfully credited with the specified amount </p>

            </div>
        </div>
    </div>
</div>
{%endif%}