<?php 
$totalOdds=1; 
?>

<div class="col-md-6 col-lg-12 inslip">

    <style>
    .single-slip{
        
    }
        .ttt
        {
            display: block;
            height: 50px;
            line-height: 25px;
            padding: 4px 6px 6px;
            font-size: 12px;
            margin-bottom: 1px;
           
            /*background-color: #FFFACD;*/
            clear:both;
        }
        .ttt .date { float:right }
        .ttt .name { float:left}

        .tit
        {
            display: block;
            line-height: 25px;
            padding: 4px 6px 6px;
            font-size: 12px;
            
            margin-bottom: 1px;
            clear:both;
            color:#fff;
        }
        .tit .date { float:right }
        .tit .name { float:left;  }

        .tit_win {
            padding: 0 5px;
            float: left;
            width: 100%;
            margin-bottom: 10px;
            margin-top: 5px;
            background: green;
        }
        
        .tit_slip .date { float:right }
        .tit_slip .name { float:left }

        .tit_slip {
            display: block;
            height: 70px;
            padding: 0px 1px 2px;
            font-size: 8px;
            background-color: #fff;
            line-height: 3em;
            color: #101721;
            clear: both;
            margin-bottom: 4px;
            border-bottom: 1px solid #1e1e1e;
            opacity: 0.7;
        }

      .tit_slip_match {
            display: flex;
            line-height: 17px;
            padding: 1px 6px 1px;
            font-size: 12px;
            color: #fff;
            /* background-color: #FFF; */
            background-color: #0f0f0f;
            text-transform: capitalize;
            clear: both;
            padding-bottom: 5px;
        }
        .tit_slip_match .teams { float:left; width: 100%;}
        .tit_slip_match .canc { 
            float:right;
            color: red;
            cursor: pointer;
        }

        .tit_stake
        {
            display: block;
            line-height: 20px;
            padding: 0px;
            font-size: 12px;
            color: #000;
            padding-right: 5px;
        }
        .tit_stake .label
        {
            padding-top: 0px;
            font-size: 12px;
            float: left;
            width: 60%;
            padding: 5px; 
            /**color: #f6d900; **/
            color: #fff;  
        }
        .tit_input {
            width: 40%;
            float: left;
            border: 1px solid #1e1e1e;
            padding: 5px 3px;
        }
        .odd-btn, .pos_win{
            width: 100%;
            border-radius: 3px;
            margin-bottom: 5px;
            line-height: 24px;
            font-weight: bold;

            border-radius: 0 !important;
            opacity: 0.8;
        }
        .dummy{padding: 5px;}
    </style>
    <?php 
    $betslip = $this->session->get('betslip'); 
    ?>  
    <?php foreach((array)$betslip as $bet): ?>
    <?php 
    $odd = $bet['odd_value'];
    if ($bet['bet_type'] == 'prematch') {
        $jackpotSlip = 0; 
    }else{
        $jackpotSlip = 1; 
    }
    $totalOdds = round($totalOdds*$odd,2);
    ?>
    <?php endforeach; ?>

    <?php 
    //$betslip = $this->session->get('betslip'); 
    $matchCount = 0;
    $bonus = 0;
    $bonusOdds = 1;


    if(!$betslip){
    echo '<p class="dummy">You have not selected any matches.
         Please select desired matches to place bet</p>';
    }
    ?>  

    <?php foreach((array)$betslip as $bet): ?>

    <?php 
    $odd = $bet['odd_value'];
    if($bet['bet_pick']=='x'){
    $pick = 'DRAW';
    }else if($bet['bet_pick']=='2'){
    $pick = $bet['away_team'];
    }else if($bet['bet_pick']=='1'){
    $pick = $bet['home_team'];
    } else{  $pick = $bet['bet_pick']; }

    if($bet['odd_value']>=1.6){
    $bonusOdds*=$odd;
    $matchCount++;
    }
    //$totalOdds = round($totalOdds*$odd,2);
    ?>
    <div style="height:1px;width: 100%;"></div>
    <div class="tit_slip_match">
        <span class="teams slip-games">
            <?php echo $bet['home_team']; ?><b> VS </b>  <?php echo $bet['away_team']; ?>
        </span>
        <span class="canc">
            <span id="<?php echo $bet['match_id']; ?>" type="submit" class="slip-games" 
                onclick="removeMatch(this.id)">X</span>
        </span>
    </div>

    <div class="tit_slip">
        <span class="name slip-games">
            <b>Bet type: </b> <?php echo $bet['odd_type']; ?> <br/>
            <b>Odds: </b><?php echo $odd ?>
            <br/>
            <b>Pick: </b><?= $pick; ?>
        </span>
        
    </div>

    <?php endforeach; ?>
     <?php if($jackpotSlip != 1){ ?>
     <div class="tit tit_win">
        <span class="name">
            Stake after Tax <br/>
            Total Odds <br/><button class="btn btn-light btn-sm btn-cart-tools odd-btn"> <?php echo $totalOdds; ?></button>
        </span>
        <span class="date">
            <span id="stake-after-tax"><?php echo round(50/1.2,2); ?></span><br/>
            Possible Winnings<br/>
            <span id="pos_win" class="btn btn-light btn-sm btn-cart-tools pos_win">

                <?php $winnings = $totalOdds*50/1.2; ?>
                <b>Ksh. <?php ($winnings<50000)?$winnings = $winnings:$winnings=50000; $taxable = $winnings-(50/1.2);  echo round($winnings,2); ?></b>
            </span>
        </span>
    </div>
    <div class="tit tit_win" style="background: #0F0F0F;">
        <span class="name">
            Withholding Tax (20%)<br/>
            <button id="tax" class="btn btn-light btn-sm btn-cart-tools odd-btn"> 
                <?php  $tax = (20 * $taxable) / 100;  echo round($tax,2); ?> 
            </button>
        </span>
        <span class="date">
            NET POSSIBLE WIN<br/>
            <span id="net-amount" class="btn btn-light btn-sm btn-cart-tools pos_win">

                <b>Ksh. <?php echo round($winnings - $tax,2); ?></b>
            </span>
        </span>
    </div>
    
    <?php } ?>
    <form>
        <div class="tit_stake">
            <div class="label">BET AMOUNT(KSH)</div>
             <?php if($jackpotSlip != 1){ ?>
            <div class="input">
                <input class="bet-select tit_input" type="number" id="bet_amount" onkeyup="winnings()" minimum="3"   value="50" min="50" max="10000" />
            </div>
        <?php } else {?>
            <div class="input">
                <input class="bet-select tit_input" type="number" id="bet_amount" readonly="readonly" disabled="disabled" minimum="3"   value="50" min="50" max="10000" />
            </div>
        <?php } ?>
        </div>

        <input type="hidden" id="max_win" value="20000"> 
        <input type="hidden" id="minimum_bet" value="50"> 
        <input type="hidden" id="user_id" value="{{session.get('auth')['id']}}">
        <input type="hidden" id="total_odd" value="<?php echo $totalOdds; ?>">
        <input type="hidden" id="max_multi_games" value="20">

    </form>
    
</div>
