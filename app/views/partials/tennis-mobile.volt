<div class="">
<div class="panel-header">
  <h2>Top Leagues</h2>
</div>

   <ul class="f-list">
          {% for league in tennis %}
            <?php $name = str_replace(', New York, USA','',$league["competition_name"]); ?>
            {% set id = league["competition_id"] %}
            {% set category = league["category"] %}
            <li><a class="b fa fa-chevron-right" href="{{url('tennis?id=')}}{{id}}">
            <span class="f-league"> {{ name }}, {{ category }} </span>
            </a></li>
            {% endfor  %}
  </ul>

</div>