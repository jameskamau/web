<?php 
  $slipCount = $this->session->get('betslip');
  $slipCount = sizeof($slipCount);
 ?>
<div class="gn rs">

<div class='alert alert-success alert-dismissible betslip-success' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>Bet successfully placed!</div>
    <ul class="bet-option-list">
<div id='' class="bet alu">
<header><div class="betslip-header">Betslip (<span class="slip-counter"><?= $slipCount ?></span>)</span></div></header>
<img src="{{url('img/ring.gif')}}" class="loader" />
<button id="slip-button-close" type="button" class="close mobi" aria-hidden="true">×</button>
<div id="betslip" class="betslip ">

</div>

<div id="quick-login">

<p>Please login to place a bet</p>

<?php echo $this->tag->form("login/authenticate"); ?>

 <p>
    <label>Mobile Number *</label>
    <?php echo $this->tag->numericField(["mobile","placeholder"=>"07XX XXX XXX","class"=>"form-control"]) ?>
 </p>

 <p>
    <label>Password *</label>
    <?php echo $this->tag->passwordField(["password","name"=>"password","class"=>"form-control","placeholder"=>"Password"]) ?>
 </p>

  <div class="col-sm-12 zero-padding">
    <div class="col-sm-4 zero-padding"><?php echo $this->tag->submitButton(["Login","class"=>"cg fm"]) ?></div>
    <div class="col-sm-8 zero-padding"><a href="{{ url('signup') }}">Join now</a></div>
 </div>

</form>
</div>

</div>

<div class="qv rc alu">

	  <div class="support">
    <div class="helpline">
      <span class="col-sm-4"><img src="{{url('img/phone-call.svg')}}"/></span>
      <span class="csc">Customer Care</span>
	<span class="col-sm-8" style="color:#fff;">+254 101 290 080</span>
	<span class="col-sm-4"><img src="{{url('img/phone-call.svg')}}"/></span>
	<span class="col-sm-8" style="color:#fff;">0705 290 080</span>
    </div>
    <div class="mail">
      <i class="fa fa-envelope-o" aria-hidden="true"></i> &nbsp; support@scorepesa.co.ke
    </div>
		</div>
  </div>

</div>
