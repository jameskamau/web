<section class="panel">            
            <div class="panel-body">
<p><strong>BETTING WITH ScorePesa</strong></p>
<p><strong>Age</strong>: Anyone above 18years of age can play as many times as they desire but within the daily limits on bet values and payouts set aside in our Terms and Conditions</p>
<p><strong>Registration</strong>: SMS &ldquo;ScorePesa&rdquo; to 29008. You will receive a confirmation message from 29008 confirming that you are now registered and ready to bet.</p>
<p><strong>Account Credit/ Account top up</strong>: To top up your ScorePesa Account, go to the Mpesa Menu, Lipa na Mpesa, go to pay bill number enter business number 290028, account no. ScorePesa and enter the amount you wish to deposit. You can also deposit online at www.scorepesa.com. Once you&rsquo;ve logged in, click on &lsquo;Deposit&rsquo;, enter amount and then click on &lsquo;Top Up Now&rsquo;.</p>
<p><strong>Balance inquiry</strong> : To check your balance, SMS the word BALANCE to 29008. To check your bonus balance, SMS the word BONUS to 29008.</p>
<p><strong>How to place a Bet</strong>: The minimum bet amount is Ksh 50 and the maximum limit is set in the terms and conditions.</p>
<p>There are three possible results in any football match:</p>
<p>1 - Home Team wins</p>
<p>X - Draw result</p>
<p>2 - Away Team wins</p>
<p>To bet on your team, SMS ScorePesa to 29008 and you will receive current football match fixtures and their ODDS. To receive more games simply SMS ScorePesa to 29008. The more you SMS the more games you receive.</p>
<p>You can also visit the ScorePesa website www.scorepesa.com and identify the game you wish to bet on.</p>
<p>Types of bets:</p>
<p>Single Bet: Place bet in this format; GAMEID#PICK#AMOUNT</p>
<p>e.g. GAMEID 123 Arsenal vs Liverpool ODDS (1=2.54 X=3.47 2=3.10)</p>
<p>to bet on this match, SMS 123#1#200 to 29008</p>
<p>Multi Bet: Place bet in this format; GAMEID#PICK#GAMEID#PICK#AMOUNT</p>
<p>e.g. GAMEID 123 Arsenal vs Liverpool ODDS (1=2.54 X=3.47 2=3.10)</p>
<p>GAMEID 456 Manchester United vs Chelsea ODDS (1=2.87 X=4.13 2=3.90)</p>
<p>to bet on this match, SMS 123#1#456#X#200 to 29008</p>
<p>Double Chance (DC): Possible outcomes &ndash; 12, 1X, X2 Pick Format: DC12 DC1X DCX2</p>
<p>GAMEID 123 Arsenal vs Liverpool ODDS (1=2.54 X=3.47 2=3.10)</p>
<p>Single Bet - Place bet in this format: GAMEID#PICK#AMOUNT</p>
<p>E.g. 123#DC1X#200</p>
<p>GAMEID 123 Arsenal vs Liverpool ODDS (1=2.54 X=3.47 2=3.10)</p>
<p>GAMEID 456 Manchester United vs Chelsea ODDS (1=2.87 X=4.13 2=3.90)</p>
<p>Multi Bet - Place bet in this format: GAMEID#PICK#AMOUNT</p>
<p>E.g. 123#DC1X#456#DC12#200</p>
<p>Goal Goal (GG) Both teams score</p>
<p>No Goal (NG) No team scores:</p>
<p>GAMEID 123 Arsenal vs Liverpool ODDS (1=2.54 X=3.47 2=3.10)</p>
<p>Single Bet - Place bet in this format: GAMEID#PICK#AMOUNT</p>
<p>E.g. 123#GG#200</p>
<p>123#NG#200</p>
<p>GAMEID 123 Arsenal vs Liverpool ODDS (1=2.54 X=3.47 2=3.10)</p>
<p>GAMEID 456 Manchester United vs Chelsea ODDS (1=2.87 X=4.13 2=3.90)</p>
<p>Multi Bet - Place bet in this format: GAMEID#PICK#AMOUNT</p>
<p>E.g. 123#GG#456#NG#200</p>
<p>Over &amp; Under 1.5/2.5/:</p>
<p>Over 2.5 (ov25) over 2 goals scored</p>
<p>Under 2.5 (un25) under 2 goals scored</p>
<p>GAMEID 123 Arsenal vs Liverpool ODDS (1=2.54 X=3.47 2=3.10)</p>
<p>Single Bet - Place bet in this format: GAMEID#PICK#AMOUNT</p>
<p>E.g. 123#ov25#200 123#un25#200</p>
<p>GAMEID 123 Arsenal vs Liverpool ODDS (1=2.54 X=3.47 2=3.10)</p>
<p>GAMEID 456 Manchester United vs Chelsea ODDS (1=2.87 X=4.13 2=3.90)</p>
<p>Multi Bet - Place bet in this format: GAMEID#PICK#AMOUNT</p>
<p>E.g. 123#ov25#456#un25#200</p>
<p>Halftime/Fulltime</p>
<p>GAMEID#HtFt(Halftime Pick Fulltime Pick)#AMOUNT</p>
<p>GAMEID 123 Arsenal vs Liverpool ODDS (1=2.54 X=3.47 2=3.10)</p>
<p>123#HtFtX1#100</p>
<p>Jackpot</p>
<p>JP#PICK1PICK2PICK3PICK4&hellip;&hellip;&hellip;PICK13</p>
<p>e.g. JP#1XX2211X21X21</p>
<p>Send all bets placed to 29008.</p>
<p><strong>Winners&rsquo; Notification</strong>: Winners will receive an SMS notification of their winnings and have it deposited in their ScorePesa account a few minutes after the game ends. The extra minutes allows us to validate the results and post them on our website www.scorepesa.co.ke</p>
<p>Withdraw: To withdraw, send WITHDRAW#AMOUNT to 29008. You can also withdraw on www.scorepesa.com. Once you are logged in, click on withdraw, enter amount and click on &lsquo;withdraw now&rsquo;. Minimum withdrawal amount is Kshs. 50. Kindly note you are not able to withdraw your bonus. Withdrawal charges are as follows:</p>
<p>50 &ndash; 1,000 (15) 1,001 &ndash; 2,500 (25) 2,501 &ndash; 70,000 (33)</p>
</div> 
        </section>