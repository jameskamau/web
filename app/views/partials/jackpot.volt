<style type="text/css">
    
    .jackpot-odds {

        text-align: right !important;
        padding-right: 0px;
        font-size: 14px;
    }

    .jackpot-key{

        text-align: left;
        padding-left: 0px;
        font-size: 14px;
    }


</style>
<?php
function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

    return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}
?>
<!-- load upcoming games here -->
           
<?php $count = 1; ?>                 
<?php foreach ($games as $day): ?>
<?php
   $theMatch = @$theBetslip[$day['match_id']];
   $odds = $day['threeway'];
   $odds = explode(',',$odds);
   $home_odd = $odds['0'];
   $neutral_odd = $odds['2'];
   $away_odd = $odds['1'];
?>
<div class="col-md-12 row np hrow">
<div id="team-details" class="col-md-6 col-sm-12 row team-details np">
  <div id="customers" class="col-md-12 col-sm-12 row np c-header">
   <?php echo "<span style='font-weight:bold;color:#FFF;font-size: 12px;'>#".$count++."&nbsp;</span>".$day['category']; ?> - 
   <?php echo $day['competition_name']; ?>

  </div>
  
  <div class="col-md-10 col-sm-8 np mg-column">
     <span style="color:#009B40;">ID <?php echo $day['game_id']; ?> - <?php echo $day['start_time']; ?></span>
  </div>

  <div class="col-md-10 col-sm-8  np mg-column">
     <?php echo $day['home_team']; ?> VS 
     <?php echo $day['away_team']; ?>
  </div>
 
</div>

<div id="markets" class="col-md-6 col-11 row p-0 mmkts">
  <div class="col-md-12 col-xs-12 row np">
    <div class="col-md-4 col-xs-4 np">
    <div class="top-matches">
        <button class="<?php echo $day['match_id']; ?> 
         <?php 
           echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']);
               if ($theMatch['bet_pick'] == $day['home_team'] 
                   && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                   echo ' picked';
               }
            ?> "
            pos="<?= $day['pos']; ?>"
            hometeam="<?php echo $day['home_team']; ?>" 
            oddtype="1x2" 
            bettype='jackpot' 
            awayteam="<?php echo $day['away_team']; ?>" 
            oddvalue="<?php echo $home_odd; ?>" 
            target="javascript:;" 
            odd-key="<?php echo $day['home_team']; ?>" 
            parentmatchid="<?php echo $day['parent_match_id']; ?>" 
            id="<?php echo $day['match_id']; ?>" 
            custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['home_team']); ?>" 
            value="<?php echo $day['sub_type_id']; ?>" 
            special-value-value="0" 
            onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'),this.getAttribute('pos'))">
            <span class="odds-btn-label mobile-view">  <?php echo $home_odd; ?> </span>
            
            <div class="row">
                <div class="col-sm-6 jackpot-key">1</div>
                <div class="col-sm-6 jackpot-odds"> 
                    <?php echo $home_odd; ?>
                </div>
            </div>
        </button>
    </div> 

    </div>
    <div class="col-md-4 col-xs-4 np">
    
    <div class="top-matches">
        <button class="<?php echo $day['match_id']; ?> <?php
            echo clean($day['match_id'] . $day['sub_type_id'] . 'draw');
            if ($theMatch['bet_pick'] == 'draw' && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                echo ' picked';
            }
            ?> " 
            pos="<?= $day['pos']; ?>"
            hometeam="<?php echo $day['home_team']; ?>" 
            oddtype="1x2" bettype='jackpot' 
            awayteam="<?php echo $day['away_team']; ?>" 
            oddvalue="<?php echo $neutral_odd; ?>" 
            custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . "draw"); ?>" 
            value="<?php echo $day['sub_type_id']; ?>" 
            odd-key="draw" 
            target="javascript:;" 
            parentmatchid="<?php echo $day['parent_match_id']; ?>" 
            id="<?php echo $day['match_id']; ?>" 
            special-value-value="0" onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'),this.getAttribute('pos'))">
            <span class="odds-btn-label mobile-view">  <?php echo $neutral_odd; ?> </span>
            
            <div class="row">
                <div class="col-sm-6 jackpot-key">X</div>
                <div class="col-sm-6 jackpot-odds"> 
                    <?php echo $neutral_odd; ?> 
                </div>
            </div>

       </button>
    </div>
    </div>
    <div class="col-md-4  col-xs-4 np">
    <div class="top-matches">
     <button class="<?php echo $day['match_id']; ?> <?php
            echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']);
            if ($theMatch['bet_pick'] == $day['away_team'] && $theMatch['sub_type_id'] == $day['sub_type_id']) {
                echo ' picked';
            }
            ?> " 
            pos="<?= $day['pos']; ?>"
            hometeam="<?php echo $day['home_team']; ?>" 
            oddtype="1x2" bettype='jackpot' 
            awayteam="<?php echo $day['away_team']; ?>" 
            oddvalue="<?php echo $away_odd; ?>" 
            value="<?php echo $day['sub_type_id']; ?>" 
            custom="<?php echo clean($day['match_id'] . $day['sub_type_id'] . $day['away_team']); ?>" 
            odd-key="<?php echo $day['away_team']; ?>" 
            target="javascript:;" 
            parentmatchid="<?php echo $day['parent_match_id']; ?>" 
            id="<?php echo $day['match_id']; ?>" 
            special-value-value="0" 
            onClick="addBet(this.id, this.value, this.getAttribute('odd-key'), this.getAttribute('custom'), this.getAttribute('special-value-value'), this.getAttribute('bettype'), this.getAttribute('hometeam'), this.getAttribute('awayteam'), this.getAttribute('oddvalue'), this.getAttribute('oddtype'), this.getAttribute('parentmatchid'),this.getAttribute('pos'))">
            <span class="odds-btn-label mobile-view">  <?php echo $away_odd; ?> </span>

            <div class="row">
                <div class="col-sm-6 jackpot-key">2</div>
                <div class="col-sm-6 jackpot-odds"> 
                    <?php echo $away_odd; ?> 
                </div>
            </div>

    </button>
    </div>
    </div>
  </div>
 </div>
</div>
<?php endforeach; ?>
<!-- End of  games -->        
<input type="hidden" id="jackpot_id" name="jackpot_id" value="<?php echo $jackpotID; ?>">  
<input type="hidden" id="jackpot_type" name="jackpot_type" value="<?php echo $jackpotType; ?>"> 
<input type="hidden" id="total_matches" name="total_matches" value="<?php echo $totalMatches; ?>"> 