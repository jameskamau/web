{{ partial("sections/header") }}

<div class="page-wrapper">
<!-- Navigation Bar -->
{{ partial("sections/left") }}


    <!-- Main Container -->
    <div class="container-fluid" style="min-height:800px">

        <div class="row">
            <!-- Game listing -->
            <div class ="col-md-8 np">
                <div class="match-list">

                    <div class="card-header row">
                        <div class="col-md-6">
                            <h4 class="card-title m-b-0"> 
                                GAMES
                            </h4>
                        </div>
                       <div class="ss col-sm-6 nopadding web-element">
                         <form name="search" class="row" method="post" action="/?sp={{sportId}}">
                            <div class="col-sm-9 nopadding submit-input">
                                <input type="text" name="keyword" class="form-control" data-action="grow"
                                                         placeholder="Your search criteria" value="{{keyword}}">
                            </div>
                            <div class="col-sm-3 nopadding search-button">
                                <button type="submit" class="search-btn">Search</button>
                            </div>
                            </form>
                        </div>
                    </div>
                    
                    <!-- start tab content header -->
                    <div class="tab-content-header">
                        <div class="tab-content-header-text col-md-12 row p-0">
                            <div class="col-md-6 p-0 ">
                                ScorePesa All Matches
                            </div>
                            <div class="col-md-3 p-0 m1x2h web-view">
                                1x2
                            </div>
                            <div class="col-md-3 p-0 movu web-view">
                                Over/Under 2.5
                            </div>
                            <!-- 
                            <div class="col-md-3 p-0 mdc">
                                Double Chance
                            </div>
                        -->
                        </div>
                    </div>
                    <div class=" col-md-12 row p-0 markets-desc web-view">
                        <div class="col-md-4 p-0">
                            &nbsp;
                        </div>
                        <div class="col-md-3 row np oddk m1x2h ">
                            <div class="col-md-4 np">1</div>
                            <div class="col-md-4 np">X</div>
                            <div class="col-md-4 np">2</div>
                        </div>
                        <div class="col-md-3 row p-0 mdc">
                            <div class="col-md-4">1X</div>
                            <div class="col-md-4">X2</div>
                            <div class="col-md-4">12</div>
                        </div>
                        <div class="col-md-2 row np movu">
                            <div class="col-md-6 np">Under 2.5</div>
                            <div class="col-md-6 np">Over 2.5</div>
                            
                        </div>
                         <div class="col-md-1 row np movu">
                            More
                         </div>
                        <!--  No double chance
                        <div class="col-md-3 row p-0 mdc">
                            <div class="col-md-4">1X</div>
                            <div class="col-md-4">X2</div>
                            <div class="col-md-4">12</div>
                        </div>
                    -->
                    </div>

                    <!-- End ta content header -->


                    <!-- End ta content header -->
                    <!-- Tab panels -->
                    <div class="tab-content">
                        <!--Panel 1-->
                        <div class="tab-pane fade in show active" id="panel0" role="tabpanel">

					       {{ partial("partials/upcoming") }}
                        </div>
                    </div>
 				
             	</div> <!-- match list -->
            </div > <!-- end col md-8 for middle content -->
            <!-- Start right bar -->
            <div class="col-md-4 np bs">
                {{ partial("sections/right") }}
            </div>
            <!-- end right side bar -->
        </div> <!-- end row -->
    </div>
    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
</div>



