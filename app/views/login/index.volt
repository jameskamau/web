{{ partial("sections/header") }}

<div class="page-wrapper">

<!-- Navigation Bar -->
{{ partial("sections/left") }}


    <!-- Main Container -->
    <div class="container-fluid" style="min-height:800px">

        <div class="row">
            <!-- Game listing -->
            <div class ="col-md-8 np">
                <div class="match-list">

                <div class="card-header">
                    <h4 class="card-title m-b-0">Login</h4>
                </div>

                <div class="login-form p-3">
                    {{ this.flashSession.output() }}
                    <?php echo $this->tag->form("login/authenticate"); ?>
                    <div class="form-group mb-1">
                        <label class="mb-0" for="inputCard">Mobile Number</label>
                        <?php echo $this->tag->numericField(["mobile","placeholder"=>"07XX XXX XXX","class"=>"form-control form-control-sm"]) ?>
                    </div>
                    <div class="form-group mb-1">
                        <label class="mb-0" for="inputFullname">Password</label>
                        <?php echo $this->tag->passwordField(["password","name"=>"password","class"=>"form-control form-control-sm","placeholder"=>"Password"]) ?>
                    </div>
                    <p>
                        <?php echo $this->tag->submitButton(["Login","class"=>"btn btn-theme"]) ?> <a class="forgot" href="{{ url('reset') }}"><span class="sticky-hidden">Forgotten password?</span></a>
                    </p>
                    </form>
               </div> <!-- rounded -->
            </div> <!-- match list -->
            </div> <!- middle section -->
            <!-- Start right bar -->
            <div class="col-md-4 np bs">
                {{ partial("sections/right") }}
            </div>
            <!-- end right side bar -->
        </div> <!-- end row -->
      </div>
    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
</div>

