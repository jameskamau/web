<!DOCTYPE html>
<html lang="en">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="description" content="ScorePesa - Kenya's leading betting website ">
        <meta name="keywords" content="ScorePesa, Kenya, Sports Betting, SMS Betting, Upcoming games, best odds ">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:site_name" content="ScorePesa"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" type="image/png" href="favicon.png" />
        {{ get_title() }} 
        {{ partial("sections/header-section-scripts") }}
    </head>
    <body class="fix-header fix-sidebar card-no-border">
        {{ content() }}

        {{ partial("sections/footer-section-scripts") }}
    </body>
</html>
