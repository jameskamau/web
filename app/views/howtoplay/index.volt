{{ partial("sections/header") }}

<div class="page-wrapper">
<!-- Navigation Bar -->
{{ partial("sections/left") }}



    <!-- Main Container -->
    <div class="container-fluid" style="min-height:800px">

        <div class="row">
            <!-- Game listing -->
            <div class ="col-md-8 np">
                <div class="match-list">

                <div class="card-header">
                    <h4 class="card-title m-b-0">How to Play </h4>
                </div>

                <div class="terms p-3">
                    <section class="panel">            
            <div class="panel-body">

<p><strong>How to bet on ScorePesa</strong></p>
<p><strong>Age</strong>: You must be above 18 years of age to play on ScorePesa. All the terms and conditions provides in the terms page apply</p>

<p><strong>Registration</strong>: Via SMS, Send SMS 'GAMES' to 29008 or via web log on to www.scorepesa.co.ke and create acccount via Register Now link</p>

<p><strong>Deposit </strong>: From PAYBILL 290080, Account SCOREPESA you may deposit any amount upto KSH 70,000. Once you have an account with scorepesa.co.ke. You may deposit via deposit link on scorepesa.co.ke</p>

<p><strong>Balance</strong> :Via web; While logged in balance will diaplay in top right corner, Via SMS send word BALANCE to 29008.</p>

<p><strong>Bet</strong>: You may bet via any channel provided for by ScorePesa including WEB, USSD,SMS or ScorePesa App</p>

<p>You may subscribe to receive SMS with upcoming games by sending word FOOTBALL to 29008. Once you receive the games follow the example proviced to place bet via SMS. General format is ID#PICK#STAKE</p>
<p>The Games will also be listed on scorepesa.com from where you can visit and bet online</p>

<p>SMS Betting shortcuts</p>
<p>3 Way, Single match; ID#PICK#AMOUNT</p>
<p>3 Way, Multiple matches; ID1#PICK1#ID2#PICK2#ID3#PICK3#....#STAKE</p>
<p>Double Chance (DC);  Prefix DC e.g DC12, DC1X, DCX2</p>
<p>Both teams to Score; Prefix GG (True), NG (False) </p>
<p>Totals Over &amp; Under; Prexix OV25 - For over 2.5, OV35 -For over 3.5 and UN25 for under 2.5</p>
<p><b>Note:</b> Where playing Prefix like in case above create your SMS as ID#Prefix#STAKE e.g 123#GG#50 or 123#NG#50 to place bet on Both teams to score True and False respectively

<!--
<p>Jackpot</p>
<p>To bet via SMS all match picks in order of the games starting with keyword JP e.g JP#1X21X21X21X21</p>
-->

<p><strong>Winners</strong>: All winning will be deposited in scorepesa.co.ke account and use will be notified of their winnings. Users are free to access their winnings via all channels provided by scorepesa including SMS, Web, USSD</p>

<p>Withdrawal; SMS W#AMOUNT to 29008. Withdrawal link is also available on scorepesa.co.ke for online withdrawals. Note; Respective withdrawal charges apply</p>

</div> 
        </section>
                </div> <!-- rounded -->
            </div> <!-- match list -->
            </div> <!- middle section -->
            <!-- Start right bar -->
            <div class="col-md-4 np bs">
                {{ partial("sections/right") }}
            </div>
            <!-- end right side bar -->
        </div> <!-- end row -->
      </div>
    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
</div>