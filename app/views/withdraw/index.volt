{{ partial("sections/header") }}



<div class="page-wrapper">
    <!-- Navigation Bar -->
    {{ partial("sections/left") }}

    <!-- Main Container -->
    <div class="container-fluid" style="min-height:800px">

        <div class="row">
            <!-- Game listing -->
            <div class ="col-md-8 np">
                <div class="match-list">

                <div class="card-header">
                    <h4 class="card-title m-b-0">Withdraw - Direct to your Phone</h4>
                </div>

                <div class="login-form p-3">
                    {{ this.flashSession.output() }}
                    <?php echo $this->tag->form("withdraw/withdrawal"); ?>                 
                    <div class="form-group mb-1">
                        <label class="mb-0" for="inputCard">Amount</label>
                        <?php echo $this->tag->numericField(["amount","placeholder"=>"Kenya Shillings ","class"=>"form-control","required"=>"required"]) ?>
                    </div>
                    <p>
                        <?php echo $this->tag->submitButton(["Withdraw","class"=>"btn btn-theme"]) ?>
                    </p>
                    </form>
             </div> <!-- rounded -->
            </div> <!-- match list -->
            </div> <!- middle section -->
            <!-- Start right bar -->
            <div class="col-md-4 np bs">
                {{ partial("sections/right") }}
            </div>
            <!-- end right side bar -->
        </div> <!-- end row -->
      </div>
    <!-- Footer -->
    {{ partial("sections/footer") }}
    <a href="#top" class="back-top text-center" onclick="$('body,html').animate({scrollTop: 0}, 500);
            return false">
        <i class="fa fa-angle-double-up"></i>
    </a>
</div>




