<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="ScorePesa - Kenya's leading name in sports betting. Exciting Odds. Online and SMS.">
        <meta name="keywords" content="ScorePesa, Sports betting, Kenya,Todays games, SMS betting, bet, multibet,best odds">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:site_name" content="ScorePesa"/>
        <link rel="icon" type="image/png" sizes="16x16" href="{{url('img/favicon.ico')}}">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        {{ get_title() }} 

        {{ partial("partials/header-scripts") }}

        <!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s)
            {
                if (f.fbq)
                    return;
                n = f.fbq = function () {
                    n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)
                    f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                    'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '290531698092998');
            fbq('track', 'PageView');
        </script>
        <noscript>
    <img height="1" width="1" 
         src="https://www.facebook.com/tr?id=290531698092998&ev=PageView
         &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->




    <script>
        (function (t, w, i, n, p, ine, ne, tw, or, k) {
            p = w.getElementsByTagName('head')[0];
            ine = w.createElement('script');
            ine.async = 1;
            ine.src = i.replace(',', ne) + ne + n;
            p.appendChild(ine);
        })(window, document, 'https://static.,network.com/', '-postback.js', '', '', 'twinpine');
    </script>


</head>
<body>        
    
    {{ content() }}

    {{ partial("partials/footer-scripts") }}
</body>
</html>
